namespace :demo do

  desc 'Re init database with demo adverts'
  task :reinit => :environment do
    Rake::Task['db:drop'].execute
    Rake::Task['db:create'].execute
    Rake::Task['db:migrate'].execute
    Rake::Task['demo:seed_users'].execute
    #Rake::Task['demo:seed_order_statuses'].execute
    Rake::Task['demo:seed_company'].execute
    Rake::Task['demo:seed_products'].execute
    Rake::Task['demo:seed_dictionaries'].execute
    Rake::Task['demo:seed_offers'].execute
    Rake::Task['demo:seed_contracts'].execute
    Rake::Task['demo:seed_demo_orders'].execute
    Rake::Task['demo:seed_employees'].execute
  end

  desc 'seed_employees'
  task :seed_employees => :environment do
    # corehook
    saygon = User.find_by_email('saygon91@gmail.com')
    corehook = User.find_by_email('corehook@gmail.com')

    corehook.companies.first.employees.create(user: saygon)
    # corehook.companies.first.employees.create(user: corehook)
    # corehook.add_role :advert, corehook.companies.first
    saygon.add_role :advert, corehook.companies.first


    saygon.companies.first.employees.create(user: corehook)
    # saygon.companies.first.employees.create(user: saygon)
    # saygon.add_role :advert, saygon.companies.first
    corehook.add_role :advert, saygon.companies.first
  end

  desc 'Seed demo orders'
  task :seed_demo_orders => :environment do
    Contract.all.each do |contract|
      company = contract.company

      puts "Contract #{contract.as_json} -> #{company.as_json}"
      (20).times do
        customer = Customer.create(company: company,first_name: Faker::Name.first_name, last_name: Faker::Name.last_name, phone: Faker::PhoneNumber.cell_phone)

        order = contract.orders.create(customer: customer, offer: contract.offer, contract: contract, company_id: contract.company_id, delivery_type_id: rand(0..1),status_id: company.dictionaries.where(object_type: DICTIONARY_TYPES.index('статус заказа')).sample.id, logistic_status_id: company.dictionaries.where(object_type: DICTIONARY_TYPES.index('статус посылки')).sample.id)
        order.order_products.create(company_id: contract.company_id, product: contract.offer.product, count: rand(1..3), discount: 0, price: contract.offer.product.price)
        order.recalc_total_amount_after_create()

        puts "New order in offer #{contract.offer.name} with customer #{customer.name} with status #{order.status_id} and logistic_status #{order.logistic_status_id}"

      end

    end
  end

  desc 'seed dictionaries'
  task :seed_dictionaries => :environment do
    Company.all.map { |c|
      puts "Company #{c.name}"+'-'*20
      DICTIONARIES.map {|d|
        d[:words].map { |w|
          dict = c.dictionaries.where(object_type: d[:object_type], name: w).first_or_create
          puts "Dictionary [#{dict.id}] -> #{dict.name} created at company #{dict.company_id}"
        }
      }
    }
  end

  desc 'seed demo contracts at all offers'
  task :seed_contracts => :environment do
    User.webmasters.each do |webmaster|
      Offer.where(private: false).each do |offer|
        contract = webmaster.contracts.create(offer: offer, accepted: true, company_id: offer.company_id)
        contract.save
        contract.update_attributes(company_id: offer.company_id)

        puts "seed_contracts -> Offer[#{offer.company_id}] - [#{offer.as_json}] Contract [#{contract.as_json}]"
      end
    end
  end

  desc 'seed products'
  task :seed_products => :environment do
    products = ['Foxford — Подготовка к ЕГЭ']#, 'Beats — Bluetooth колонка', 'ASAXI — Набор для приготовления роллов', 'Steam Brush - Отпариватель','Bridge of love — Онлайн знакомства','Kings Bounty: Legions — Клиентская игра']
    Company.all.map { |company| products.map { |p| company.products.create(name: p, price: rand(1000..2000))}}
  end

  desc 'seed offers'
  task :seed_offers => :environment do
    Company.all.each do |company|
      company.products.each do |product|
        offer = company.offers.create(name: product.name, description: 'demo offer', product: product, private: false, price: Faker::Number.decimal(5))
        offer.landing_pages.create(url: Faker::Internet.url) if offer
        puts "seed_offers -> #{offer.as_json}"
      end
    end
  end

  desc 'seed companies'
  task :seed_company => :environment do
    User.all.each do |user|
      company = user.companies.create(name: "#{user.email.split('@')[0]}_#{Faker::Internet.domain_word}")
      if company
        user.add_role :advert_admin, company
        user.add_role :advert, company
        warehouse = company.warehouses.create(name: Faker::Internet.domain_word)
        puts "Компания #{company.to_json} создана со складом #{warehouse.name} для пользователя #{user.name}"
        # rand(20..55).times do
        #   puts "Склад [#{warehouse.name}] создан в компании #{company.name}"
        #   product = Product.create(company: company, name: Faker::Lorem.word, weight: rand(100..1000), count: rand(1..10))
        #   if product
        #     wp = warehouse.warehouse_products.create(product: product)
        #     puts "Товар #{wp.product.name} добавлен на склад #{wp.warehouse.name}" if wp
        #   end
        # end if warehouse
      end

    end
  end

  desc 'Seed users'
  task :seed_users => :environment do
    users_list  = [
      {email: 'mg71645@gmail.com',      roles: [:advert, :webmaster, :advert_admin, :generate_barcodes], password: 'mg71645mg71645', first_name: 'Марат', last_name: 'Гильванов'},
      {email: 'corehook@gmail.com',     roles: [:advert, :advert_admin, :generate_barcodes], password: 'corehook', first_name: 'Тимур', last_name: 'Талипов'},
      {email: 'saygon91@gmail.com',     roles: [:advert, :advert_admin, :generate_barcodes], password: 'saygon91', first_name: 'Саят', last_name: 'Мухамадиев'},
      {email: 'storekeeper@gmail.com',  roles: [:advert, :storekeeper, :generate_barcodes], password: 'storekeeper', first_name: 'Тимур', last_name: 'Шуашев'},
      {email: 'webmaster@gmail.com',    roles: [:webmaster, :generate_barcodes], password: 'webmaster', first_name: 'Сэмуэль', last_name: 'Кабанов'},
    ]




    users_list.each do |user_data|
      user = User.create(user_data)
      if user
        puts "Создан пользователь #{user_data[:email]}."
        user.add_role :advert if user.email.eql? 'mg71645@gmail.com'
        user.add_role :webmaster if user_data[:email].eql? 'webmaster@gmail.com'
        # user_data[:roles].each do |role|
        #   puts "Пользователю #{user_data[:email]} добавили роль #{role}"
        #   user.add_role role
        #   user.save
        # end
      end
    end

  end

end