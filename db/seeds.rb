# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#

# User has Company [name]
# Company has Offer
# Company has Warehouse
# WarehouseProduct belongs to product, warehouse
# Warehouse has WarehouseProduct
# Product has [name, description, weight]
#
#
#
#
# rails g model company user:references name
# rails g model warehouse company:references name
# rails g model product name weight:integer count:integer
# rails g model warehouse_product product:references warehouse:references sold:boolean price:double
#
# rails g model offer name description company:references product:references
# rails g model offer_type name per_action:float percent_price:integer offer:references postclick:integer
# rails g model geo name offer:references
# rails g model news offer:references content:text
# rails g model transit_page url:text name cr:integer offer:references
# rails g model landing_page url:text name cr:integer offer:references
# rails g model customer first_name last_name phone city country zip address comment
# rails g model order offer:references customer:references