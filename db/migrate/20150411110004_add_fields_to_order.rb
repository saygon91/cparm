class AddFieldsToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :total_amount_discount, :float, null: true, default: 0.0
    add_column :orders, :total_amount_percent, :float, null: true, default: 0.0
  end
end
