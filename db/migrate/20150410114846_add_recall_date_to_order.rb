class AddRecallDateToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :recall_date, :datetime, null: true
  end
end
