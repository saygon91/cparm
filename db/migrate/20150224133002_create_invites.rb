class CreateInvites < ActiveRecord::Migration
  def change
    create_table :invites do |t|
      t.references :company, index: true
      t.references :user, index: true, null: true
      t.boolean :joined
      t.string :email
      t.string :key

      t.timestamps null: false
    end
    add_foreign_key :invites, :companies
    add_foreign_key :invites, :users
  end
end
