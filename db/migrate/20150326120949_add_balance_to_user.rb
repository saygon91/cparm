class AddBalanceToUser < ActiveRecord::Migration
  def change
    add_column :users, :balance, :float, default: 0, null: false
  end
end
