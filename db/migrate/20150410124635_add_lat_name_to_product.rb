class AddLatNameToProduct < ActiveRecord::Migration
  def change
    add_column :products, :lat_name, :string, null: true
  end
end
