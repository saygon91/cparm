class AddDeliveryAmountToProduct < ActiveRecord::Migration
  def change
    add_column :products, :delivery_amount, :float, default: 0.0
  end
end
