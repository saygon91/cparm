class AddCompanyFieldToBalance < ActiveRecord::Migration
  def change
    add_reference :balances, :company, index: true
    add_foreign_key :balances, :companies
  end
end
