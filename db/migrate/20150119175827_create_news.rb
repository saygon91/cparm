class CreateNews < ActiveRecord::Migration
  def change
    create_table :news do |t|
      t.references :offer, index: true
      t.text :content

      t.timestamps null: false
    end
    add_foreign_key :news, :offers
  end
end
