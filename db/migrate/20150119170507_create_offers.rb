class CreateOffers < ActiveRecord::Migration
  def change
    create_table :offers do |t|
      t.string :name
      t.text :description
      t.references :company, index: true, null: false
      t.references :product, index: true, null: false

      # Оффер приватный ?
      t.boolean :private, default: false

      # Отчисление в процентах от стоимости товара
      t.float :percent, null: true, default: 0

      # Фиксированное отчисление
      t.float :price, null: true, default: 0
      t.integer :currency, limit: 1, default: 0

      t.timestamps null: false
    end
    add_foreign_key :offers, :companies
    add_foreign_key :offers, :products
  end
end
