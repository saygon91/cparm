class AddFieldsToCheck < ActiveRecord::Migration
  def change
    add_column :checks, :date_from, :datetime
    add_column :checks, :date_to, :datetime
  end
end
