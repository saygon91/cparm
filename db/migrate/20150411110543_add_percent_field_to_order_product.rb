class AddPercentFieldToOrderProduct < ActiveRecord::Migration
  def change
    add_column :order_products, :percent, :integer, default: 0
  end
end
