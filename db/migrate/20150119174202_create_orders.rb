class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.references :offer, index: true
      t.references :customer, index: true
      t.references :company, index: true
      t.integer :contract_id, null: true, default: nil
      t.integer :status_id, null: false, default: 1
      t.integer :logistic_status_id, null: false, default: -1
      t.integer :delivery_type_id, null: false, default: -1
      t.float   :total_amount, null: false, default: 0
      t.integer :delivery_amount, null: false, default: 0
      t.string :barcode, null: true
      t.integer :delivery, null: false, default: 1
      t.integer :courier, null: false, default: 1
      t.integer :zip, null: true
      t.text :landing_page, null: true
      t.string :lead_key, null: true

      t.timestamps null: false

    end


    add_foreign_key :orders, :companies
    add_foreign_key :orders, :offers
    add_foreign_key :orders, :customers
  end
end
