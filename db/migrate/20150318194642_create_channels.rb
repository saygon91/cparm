class CreateChannels < ActiveRecord::Migration
  def change
    create_table :channels do |t|
      t.references :company, index: true
      t.string :name, limit: 20

      t.timestamps null: false
    end
    add_foreign_key :channels, :companies
  end
end
