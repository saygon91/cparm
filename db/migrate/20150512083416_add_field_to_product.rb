class AddFieldToProduct < ActiveRecord::Migration
  def change
    add_column :products, :sale_two, :integer, default: 0
    add_column :products, :sale_three, :integer, default: 0
  end
end
