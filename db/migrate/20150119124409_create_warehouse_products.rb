class CreateWarehouseProducts < ActiveRecord::Migration
  def change
    create_table :warehouse_products do |t|
      t.references :product, index: true
      t.references :warehouse, index: true
      t.integer :count, null: false, default: 0
      t.boolean :sold
      t.float :price

      t.timestamps null: false
    end
    add_foreign_key :warehouse_products, :products
    add_foreign_key :warehouse_products, :warehouses
  end
end
