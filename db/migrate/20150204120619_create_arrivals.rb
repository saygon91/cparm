class CreateArrivals < ActiveRecord::Migration
  def change
    create_table :arrivals do |t|
      t.references :warehouse, index: true
      t.references :product,   index: true
      t.references :user, index: true

      # Количество & Комментарий
      t.integer :count
      t.text :comment

      t.timestamps null: false
    end
    add_foreign_key :arrivals, :products
    add_foreign_key :arrivals, :warehouses
    add_foreign_key :arrivals, :users
  end
end
