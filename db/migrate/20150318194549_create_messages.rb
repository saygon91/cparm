class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.integer :message_type, limit: 1
      t.text :message, null: false
      t.integer :src, null: false
      t.integer :dst, null: false
      t.boolean :deleted, default: false
      t.integer :company_id, null: false
      t.boolean :read, default: false
      t.datetime :readed_at, default: nil, null: true

      t.timestamps null: false
    end
  end
end
