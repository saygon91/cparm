class CreateDictionaries < ActiveRecord::Migration
  def change
    create_table :dictionaries do |t|
      t.string :name
      t.integer :object_type, limit: 1
      t.string :additional
      t.references :company, index: true

      t.timestamps null: false
    end
    add_foreign_key :dictionaries, :companies
  end
end
