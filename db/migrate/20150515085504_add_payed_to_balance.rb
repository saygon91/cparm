class AddPayedToBalance < ActiveRecord::Migration
  def change
    add_column :balances, :payed, :boolean, default: false
  end
end
