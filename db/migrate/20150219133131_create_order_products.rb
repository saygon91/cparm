class CreateOrderProducts < ActiveRecord::Migration
  def change
    create_table :order_products do |t|
      t.references :product, index: true
      t.references :order, index: true
      t.references :company, index: true
      t.integer :price, default: 0, null: false
      t.integer :count
      t.integer :discount, default: 0

      t.timestamps null: false
    end
    add_foreign_key :order_products, :products
    add_foreign_key :order_products, :orders
    add_foreign_key :order_products, :companies
  end
end
