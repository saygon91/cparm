class CreateOutgos < ActiveRecord::Migration
  def change
    create_table :outgos do |t|
      t.references  :warehouse, index: true
      t.references  :product, index: true


      t.integer     :count
      t.references  :user, index: true
      t.text        :comment, null: true

      t.timestamps null: false
    end

    add_foreign_key :outgos, :warehouses
    add_foreign_key :outgos, :products
    add_foreign_key :outgos, :users
  end
end
