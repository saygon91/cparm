class CreateWarehouses < ActiveRecord::Migration
  def change
    create_table :warehouses do |t|
      t.references  :company, index: true
      t.string      :name
      t.text        :location                 # Местонахождение

      t.timestamps null: false
    end
    add_foreign_key :warehouses, :companies
  end
end
