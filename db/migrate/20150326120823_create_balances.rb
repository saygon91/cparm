class CreateBalances < ActiveRecord::Migration
  def change
    create_table :balances do |t|
      t.references :user, index: true
      t.references :order, index: true
      t.float :amount

      t.timestamps null: false
    end
    add_foreign_key :balances, :users
    add_foreign_key :balances, :orders
  end
end
