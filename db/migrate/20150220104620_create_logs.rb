class CreateLogs < ActiveRecord::Migration
  def change
    create_table :logs do |t|
      t.references  :user, index: true, null: true              # Кто
      t.references  :company, index: true                       # В какой компании
      t.references  :order, index: true, null: false            # Какой заказ
      t.integer     :action_id, null: false, default: 0         # Что сделал (изменил, добавил, создал)
      t.integer     :field_id, null: false, default: 0          # С каким полем (статус доставки, имя клиента и тд)
      t.integer     :order_product_id, null: true, default: nil # ID order product при доп продаже
      t.string     :old_value, null: true, default: 0
      t.string     :new_value, null: true, default: 0
      t.string     :field_str, null: true

      t.timestamps null: false
    end

    add_foreign_key :logs, :users
    add_foreign_key :logs, :companies
    add_foreign_key :logs, :orders
  end
end
