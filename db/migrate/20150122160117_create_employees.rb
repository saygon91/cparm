class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.references  :user, index: true,    null: false
      t.references  :company, index: true, null: false
      t.boolean     :blocked, default: false
      t.text        :block_reason, null: true
      t.text        :unblock_reason, null: true
      t.datetime    :blocked_at, null: true
      t.datetime    :unblocked_at, null: true

      t.timestamps null: false
    end
    add_foreign_key :employees, :users
    add_foreign_key :employees, :companies
  end
end
