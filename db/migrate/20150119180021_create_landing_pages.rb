class CreateLandingPages < ActiveRecord::Migration
  def change
    create_table :landing_pages do |t|
      t.text :url
      t.string :name
      t.integer :cr
      t.references :offer, index: true

      t.timestamps null: false
    end
    add_foreign_key :landing_pages, :offers
  end
end
