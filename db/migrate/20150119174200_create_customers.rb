class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :role
      t.string :first_name
      t.string :last_name
      t.string :phone
      t.string :city
      t.string :country
      t.string :zip
      t.string :address
      t.string :comment
      t.string :email
      t.string :province
      t.references :company, index: true

      t.timestamps null: false
    end
    add_foreign_key :customers, :companies
  end
end
