class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name, null: false
      t.integer :weight
      t.integer :count
      t.integer :price
      t.text :description, null: true
      t.timestamps null: false
      t.references :company, index: true
    end
    add_foreign_key :products, :companies
  end
end
