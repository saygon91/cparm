class ChangeOrderColumnType < ActiveRecord::Migration
  def change
    remove_column :customers, :country
    add_column :customers, :country, :integer, default: 0
  end
end
