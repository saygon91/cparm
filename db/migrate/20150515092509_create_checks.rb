class CreateChecks < ActiveRecord::Migration
  def change
    create_table :checks do |t|
      t.references :user, index: true
      t.float :amount
      t.text :comment
      t.integer :webmaster_id
      t.references :company, index: true

      t.timestamps null: false
    end
    add_foreign_key :checks, :users
    add_foreign_key :checks, :companies
  end
end
