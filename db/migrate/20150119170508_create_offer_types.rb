class CreateOfferTypes < ActiveRecord::Migration
  def change
    create_table :offer_types do |t|
      t.string :name
      t.float :per_action
      t.integer :percent_price
      t.references :offer, index: true
      t.integer :postclick

      t.timestamps null: false
    end
    add_foreign_key :offer_types, :offers
  end
end
