class CreateGeos < ActiveRecord::Migration
  def change
    create_table :geos do |t|
      t.string :name
      t.references :offer, index: true

      t.timestamps null: false
    end
    add_foreign_key :geos, :offers
  end
end
