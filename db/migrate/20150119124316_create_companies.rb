class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.references :user, index: true
      t.string :name, null: false
      t.text :description, null: true
      t.integer :last_barcode, null: false, default: 1
      t.string :barcode_prefix, null: false, default: 'RD010'


      t.timestamps null: false
    end
    add_foreign_key :companies, :users
  end
end
