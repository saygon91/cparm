class CreateContracts < ActiveRecord::Migration
  def change
    create_table :contracts do |t|
      t.integer :webmaster_id
      t.references :offer, index: true
      t.boolean :accepted, default: true, null: false
      t.boolean :denied, default: false, null: false
      t.references :company, index: true

      t.string :utm_source , null: true
      t.string :utm_medium, null: true
      t.string :utm_campaign, null: true
      t.string :utm_term, null: true
      t.string :utm_content, null: true

      t.integer :old_price, null: true, default: 0
      t.integer :new_price, null: true, default: 0
      t.timestamps null: false
    end
    add_foreign_key :contracts, :offers
    add_foreign_key :contracts, :companies
  end
end
