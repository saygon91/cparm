class CreateTokens < ActiveRecord::Migration
  def change
    create_table :tokens do |t|
      t.string :name
      t.string :key
      t.references :company, index: true

      t.timestamps null: false
    end
    add_foreign_key :tokens, :companies
  end
end
