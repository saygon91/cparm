class AddDeliveryDateToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :delivery_date, :datetime, null: true
  end
end
