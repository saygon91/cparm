# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150515104357) do

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true

  create_table "arrivals", force: :cascade do |t|
    t.integer  "warehouse_id"
    t.integer  "product_id"
    t.integer  "user_id"
    t.integer  "count"
    t.text     "comment"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "arrivals", ["product_id"], name: "index_arrivals_on_product_id"
  add_index "arrivals", ["user_id"], name: "index_arrivals_on_user_id"
  add_index "arrivals", ["warehouse_id"], name: "index_arrivals_on_warehouse_id"

  create_table "balances", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "order_id"
    t.float    "amount"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.boolean  "payed",      default: false
    t.integer  "company_id"
  end

  add_index "balances", ["company_id"], name: "index_balances_on_company_id"
  add_index "balances", ["order_id"], name: "index_balances_on_order_id"
  add_index "balances", ["user_id"], name: "index_balances_on_user_id"

  create_table "channels", force: :cascade do |t|
    t.integer  "company_id"
    t.string   "name",       limit: 20
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "channels", ["company_id"], name: "index_channels_on_company_id"

  create_table "checks", force: :cascade do |t|
    t.integer  "user_id"
    t.float    "amount"
    t.text     "comment"
    t.integer  "webmaster_id"
    t.integer  "company_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.datetime "date_from"
    t.datetime "date_to"
  end

  add_index "checks", ["company_id"], name: "index_checks_on_company_id"
  add_index "checks", ["user_id"], name: "index_checks_on_user_id"

  create_table "companies", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "name",                             null: false
    t.text     "description"
    t.integer  "last_barcode",   default: 1,       null: false
    t.string   "barcode_prefix", default: "RD010", null: false
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "companies", ["user_id"], name: "index_companies_on_user_id"

  create_table "contracts", force: :cascade do |t|
    t.integer  "webmaster_id"
    t.integer  "offer_id"
    t.boolean  "accepted",     default: true,  null: false
    t.boolean  "denied",       default: false, null: false
    t.integer  "company_id"
    t.string   "utm_source"
    t.string   "utm_medium"
    t.string   "utm_campaign"
    t.string   "utm_term"
    t.string   "utm_content"
    t.integer  "old_price",    default: 0
    t.integer  "new_price",    default: 0
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "contracts", ["company_id"], name: "index_contracts_on_company_id"
  add_index "contracts", ["offer_id"], name: "index_contracts_on_offer_id"

  create_table "customers", force: :cascade do |t|
    t.string   "role"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "phone"
    t.string   "city"
    t.string   "zip"
    t.string   "address"
    t.string   "comment"
    t.string   "email"
    t.string   "province"
    t.integer  "company_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "country",    default: 0
  end

  add_index "customers", ["company_id"], name: "index_customers_on_company_id"

  create_table "dictionaries", force: :cascade do |t|
    t.string   "name"
    t.integer  "object_type", limit: 1
    t.string   "additional"
    t.integer  "company_id"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "dictionaries", ["company_id"], name: "index_dictionaries_on_company_id"

  create_table "employees", force: :cascade do |t|
    t.integer  "user_id",                        null: false
    t.integer  "company_id",                     null: false
    t.boolean  "blocked",        default: false
    t.text     "block_reason"
    t.text     "unblock_reason"
    t.datetime "blocked_at"
    t.datetime "unblocked_at"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  add_index "employees", ["company_id"], name: "index_employees_on_company_id"
  add_index "employees", ["user_id"], name: "index_employees_on_user_id"

  create_table "geos", force: :cascade do |t|
    t.string   "name"
    t.integer  "offer_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "geos", ["offer_id"], name: "index_geos_on_offer_id"

  create_table "invites", force: :cascade do |t|
    t.integer  "company_id"
    t.integer  "user_id"
    t.boolean  "joined"
    t.string   "email"
    t.string   "key"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "invites", ["company_id"], name: "index_invites_on_company_id"
  add_index "invites", ["user_id"], name: "index_invites_on_user_id"

  create_table "landing_pages", force: :cascade do |t|
    t.text     "url"
    t.string   "name"
    t.integer  "cr"
    t.integer  "offer_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "landing_pages", ["offer_id"], name: "index_landing_pages_on_offer_id"

  create_table "logs", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "company_id"
    t.integer  "order_id",                       null: false
    t.integer  "action_id",        default: 0,   null: false
    t.integer  "field_id",         default: 0,   null: false
    t.integer  "order_product_id"
    t.string   "old_value",        default: "0"
    t.string   "new_value",        default: "0"
    t.string   "field_str"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  add_index "logs", ["company_id"], name: "index_logs_on_company_id"
  add_index "logs", ["order_id"], name: "index_logs_on_order_id"
  add_index "logs", ["user_id"], name: "index_logs_on_user_id"

  create_table "messages", force: :cascade do |t|
    t.integer  "message_type", limit: 1
    t.text     "message",                                null: false
    t.integer  "src",                                    null: false
    t.integer  "dst",                                    null: false
    t.boolean  "deleted",                default: false
    t.integer  "company_id",                             null: false
    t.boolean  "read",                   default: false
    t.datetime "readed_at"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  create_table "news", force: :cascade do |t|
    t.integer  "offer_id"
    t.text     "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "news", ["offer_id"], name: "index_news_on_offer_id"

  create_table "offer_types", force: :cascade do |t|
    t.string   "name"
    t.float    "per_action"
    t.integer  "percent_price"
    t.integer  "offer_id"
    t.integer  "postclick"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "offer_types", ["offer_id"], name: "index_offer_types_on_offer_id"

  create_table "offers", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "company_id",                            null: false
    t.integer  "product_id",                            null: false
    t.boolean  "private",               default: false
    t.float    "percent",               default: 0.0
    t.float    "price",                 default: 0.0
    t.integer  "currency",    limit: 1, default: 0
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  add_index "offers", ["company_id"], name: "index_offers_on_company_id"
  add_index "offers", ["product_id"], name: "index_offers_on_product_id"

  create_table "order_products", force: :cascade do |t|
    t.integer  "product_id"
    t.integer  "order_id"
    t.integer  "company_id"
    t.integer  "price",      default: 0, null: false
    t.integer  "count"
    t.integer  "discount",   default: 0
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "percent",    default: 0
  end

  add_index "order_products", ["company_id"], name: "index_order_products_on_company_id"
  add_index "order_products", ["order_id"], name: "index_order_products_on_order_id"
  add_index "order_products", ["product_id"], name: "index_order_products_on_product_id"

  create_table "order_statuses", force: :cascade do |t|
    t.string   "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "orders", force: :cascade do |t|
    t.integer  "offer_id"
    t.integer  "customer_id"
    t.integer  "company_id"
    t.integer  "contract_id"
    t.integer  "status_id",             default: 1,   null: false
    t.integer  "logistic_status_id",    default: -1,  null: false
    t.integer  "delivery_type_id",      default: -1,  null: false
    t.float    "total_amount",          default: 0.0, null: false
    t.integer  "delivery_amount",       default: 0,   null: false
    t.string   "barcode"
    t.integer  "delivery",              default: 1,   null: false
    t.integer  "courier",               default: 1,   null: false
    t.integer  "zip"
    t.text     "landing_page"
    t.string   "lead_key"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.datetime "delivery_date"
    t.datetime "recall_date"
    t.float    "total_amount_discount", default: 0.0
    t.float    "total_amount_percent",  default: 0.0
  end

  add_index "orders", ["company_id"], name: "index_orders_on_company_id"
  add_index "orders", ["customer_id"], name: "index_orders_on_customer_id"
  add_index "orders", ["offer_id"], name: "index_orders_on_offer_id"

  create_table "outgos", force: :cascade do |t|
    t.integer  "warehouse_id"
    t.integer  "product_id"
    t.integer  "count"
    t.integer  "user_id"
    t.text     "comment"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "outgos", ["product_id"], name: "index_outgos_on_product_id"
  add_index "outgos", ["user_id"], name: "index_outgos_on_user_id"
  add_index "outgos", ["warehouse_id"], name: "index_outgos_on_warehouse_id"

  create_table "products", force: :cascade do |t|
    t.string   "name",                          null: false
    t.integer  "weight"
    t.integer  "count"
    t.integer  "price"
    t.text     "description"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "company_id"
    t.string   "lat_name"
    t.float    "delivery_amount", default: 0.0
    t.integer  "sale_two",        default: 0
    t.integer  "sale_three",      default: 0
  end

  add_index "products", ["company_id"], name: "index_products_on_company_id"

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id"
  add_index "roles", ["name"], name: "index_roles_on_name"

  create_table "tokens", force: :cascade do |t|
    t.string   "name"
    t.string   "key"
    t.integer  "company_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "tokens", ["company_id"], name: "index_tokens_on_company_id"

  create_table "transit_pages", force: :cascade do |t|
    t.text     "url"
    t.string   "name"
    t.integer  "cr"
    t.integer  "offer_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "transit_pages", ["offer_id"], name: "index_transit_pages_on_offer_id"

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",  null: false
    t.string   "encrypted_password",     default: "",  null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,   null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "avatar"
    t.string   "phone_number"
    t.string   "country"
    t.string   "icq"
    t.string   "skype"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "balance",                default: 0.0, null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id"

  create_table "versions", force: :cascade do |t|
    t.string   "item_type",  null: false
    t.integer  "item_id",    null: false
    t.string   "event",      null: false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
  end

  add_index "versions", ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"

  create_table "warehouse_products", force: :cascade do |t|
    t.integer  "product_id"
    t.integer  "warehouse_id"
    t.integer  "count",        default: 0, null: false
    t.boolean  "sold"
    t.float    "price"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "warehouse_products", ["product_id"], name: "index_warehouse_products_on_product_id"
  add_index "warehouse_products", ["warehouse_id"], name: "index_warehouse_products_on_warehouse_id"

  create_table "warehouses", force: :cascade do |t|
    t.integer  "company_id"
    t.string   "name"
    t.text     "location"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "warehouses", ["company_id"], name: "index_warehouses_on_company_id"

end
