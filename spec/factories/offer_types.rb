# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :offer_type do
    name "MyString"
    per_action 1.5
    percent_price 1
    offer nil
    postclick 1
  end
end
