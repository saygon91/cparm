# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :warehouse do
    company nil
    name "MyString"
  end
end
