# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :landing_page do
    url "MyText"
    name "MyString"
    cr 1
    offer nil
  end
end
