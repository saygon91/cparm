# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :customer do
    first_name "MyString"
    last_name "MyString"
    phone "MyString"
    city "MyString"
    country "MyString"
    zip "MyString"
    address "MyString"
    comment "MyString"
  end
end
