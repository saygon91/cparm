# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :warehouse_product do
    product nil
    warehouse nil
    sold false
    price ""
  end
end
