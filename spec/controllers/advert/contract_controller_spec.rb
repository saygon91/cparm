require 'rails_helper'

RSpec.describe Advert::ContractController, :type => :controller do

  describe "GET accept" do
    it "returns http success" do
      get :accept
      expect(response).to be_success
    end
  end

  describe "GET denied" do
    it "returns http success" do
      get :denied
      expect(response).to be_success
    end
  end

end
