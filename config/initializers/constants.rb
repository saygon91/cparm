LOG_ACTIONS = ['создал','изменил','добавил','удалил', 'открыл', 'закрыл']
LOG_ORDER_FIELDS = ["заказ","статус заказа","статус посылки","фио","телефон","индекс","адрес","район","город","способ доставки","продукт","комментарий","изменил количество продукта", "страна", "курьер", "имя", "фамилия", "сумма доставки", "штрихкод", "дата доставки", "дата перезвона", "скидка в процентах","скидка в сумме"]
SYSTEM_ROLES = [:advert, :webmaster, :storekeeper, :advert_admin, :call_operator, :call_manager, :log_operator, :log_manager]
ADVERT = 1
WEBMASTER = 2
ROLES = {
    :advert_admin => 'Администратор',
    :call_manager => 'call менеджер',
    :webmaster => 'Веб мастер',
    :call_operator => 'call operator',
    :log_manager => 'log менеджер',
    :log_operator => 'log оператор',
}
MESSAGE_TYPE = ['channel', 'private']
# В каком диапазоне искать журналы логов, если не задана дата поиска
# /app/controllers/advert/order_controller.rb : filter_logs
FILTER_SEARCH_START_DATE = DateTime.new(1945,1,1)
FILTER_SEARCH_END_DATE = DateTime.new(3000,1,1)

ORDER_STATUSES = ['Новый', 'Отменен', 'Перезвонить', 'Недозвонился', 'Брак', 'Другой Регион', 'Подтвержден', 'Оплачен', 'Отправлен', 'Возврат', 'Обработан', 'У конкурентов']

#ORDER_LOGISTIC_STATUSES = ['Упакован', 'Отправлен', 'Срочно отправить', 'Обработка', 'Хранение', 'Проблемный']
ORDER_LOGISTIC_STATUSES = ['Упакован', 'Отправлен', 'Срочно отправить', 'Обработка', 'Хранение', 'Проблемный', 'Отложенная доставка', 'На отправку', 'В дороге', 'Отправлен.Обработка', 'Отправлен.Отложен', 'Отправлен.Проблемный', 'Отправлен.На доставку']
SECRET_KEY = '363348060553a451'
UID = '61699544'
ARRIVAL = 1
OUTGO = 2
CURRENCY = ['WMZ', 'WMR']
DICTIONARY_TYPES = ['страна', 'город', 'индекс', 'статус посылки', 'статус курьера', 'статус заказа', 'способ доставки', 'курьер']
DELIVERY_TYPES = ['Курьером', 'Почтой']
DICTIONARIES = [
    {object_type: DICTIONARY_TYPES.index('страна'),         words: ['Казахстан', 'Россия', 'Киргизия', 'Узбекистан']},
    {object_type: DICTIONARY_TYPES.index('статус посылки'), words: ORDER_LOGISTIC_STATUSES},
    {object_type: DICTIONARY_TYPES.index('статус заказа'),  words: ORDER_STATUSES},
    {object_type: DICTIONARY_TYPES.index('способ доставки'),words: ['Курьером', 'Почтой']},
    {object_type: DICTIONARY_TYPES.index('индекс'),         words: ['Астана', 'Алматы', 'Ускаман', 'Актау', 'Шымкент', 'Караганда', 'Павлодар', 'Тараз', 'Атырау', 'Актобе', 'Костанай', 'Петропавловск', 'Кокшетау', 'Кызылорда', 'Жанаозен', 'Жесказган', 'Экибастуз', 'Семей', 'Уральск', 'Талдыкорган', 'Темиртау']},
    {object_type: DICTIONARY_TYPES.index('курьер'),         words: ['Семенов', 'Петров']}
]
def static_by_name(list, name)
  result = -1
  list.map {|dw|
    result = list.index(dw) if dw.mb_chars.downcase.eql? name.mb_chars.downcase
  }
  #logger.info "From static list #{list.to_s} try to get #{name} field. Result #{result}"
  result
end

def static_by_id(list, id)
  return list[id]
end
def get_dict_by_name(name)
  result = -1
  DICTIONARY_TYPES.map {|dw| result = DICTIONARY_TYPES.index(dw) if dw.mb_chars.downcase == name.mb_chars.downcase }
  result
end

def get_dict_by_id(id)
  return DICTIONARY_TYPES[id]
end

#
#
# def dev_objects
#   {   saygon: User.find_by_email('saygon91@gmail.com'),
#       corehook: User.find_by_email('corehook@gmail.com')
#   }
# end

# ids = 0
# menu = [id: ids, name: 'создание заказа', value: 'new_order']
# ['статус заказа','статус посылки', 'курьер', 'способ доставки'].map {|dict_type|
#   Dictionary.get_dict_list(dict_type, 2).map { |dict|
#     ids += 1
#     menu << {id: ids, name: "Изменил #{dict_type} на #{dict.name}", value: "#{dict.field_str}"}
#   }
# }

