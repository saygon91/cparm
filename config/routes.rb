Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  root 'index#index'
  devise_for :users, path: 'access', controllers: { :registrations => "registrations" }

  namespace :api do
    scope :v1 do
      match 'lead' => 'v1#lead', via: [:get, :post]
      match 'update_order' => 'v1#update_order', via: [:post]
    end
    #/api/v1/lead
  end

  namespace :advert do
    get '/' => 'index#index'

    scope :company do
      scope :billing do
        post 'calculate'  => 'company#calculate', format: [:json]
      end
      # Generate token for integrations
      scope :api do
        post 'new_token' => 'company#new_token', as: :new_token
      end

      get 'all'     => 'company#all', format: [:json]
      post 'new'    => 'company#new'
      post 'update' => 'company#update'
      post 'new_order_status' => 'order#new_order_status'
      post 'logs'   => 'company#logs'



      scope :customer do
        post 'update' => 'customer#update', as: :customer_update
      end

      scope :dictionary do
        post 'new' => 'dictionary#new'
        post 'update' => 'dictionary#update'
      end

      scope :invite do
        post 'new'    =>  'invite#new'
        post 'delete' =>  'invite#delete'
      end

      scope :contract, :format => [:json] do
        get 'all.json' => 'contract#all'
        post 'accept' => 'contract#accept'
        post 'denied' => 'contract#denied'
      end

      scope :employee, :format => [:json] do
        get 'all.json'  => 'employee#all'
        post 'salary' => 'company#salary', format: [:json], as: :salary
        post 'new'      => 'employee#new'
        post 'update'   => 'employee#update'
        post 'block'    => 'employee#block'
        post 'unblock'  => 'employee#unblock'
        post 'invite'   => 'employee#invite'
      end

      scope :order, :format => [:json] do
        post 'generate_statistic' => 'order#generate_statistic', as: :generate_statistic
        post 'accepted_statistic' => 'order#accepted_statistic', as: :accepted_statistic
        post 'generate_barcodes' => 'order#generate_barcodes', as: :generate_barcodes
        match 'print_orders_form' => 'order#print_orders_form', as: :print_orders_form, via: [:get, :post]
        post 'status_change' => 'order#status_change', as: :status_change
        post 'search_orders' => 'order#search_orders', as: :search
        post 'filter_logs' => 'order#filter_logs', as: :filter_logs
        post 'close' => 'order#close', as: :close
        post 'logs' => 'order#logs'
        post 'new' => 'order#new', as: :new_order
        get 'all.json' => 'order#all'
      	post 'update' => 'order#update'
      	post 'xls' => 'order#export_xls', format: [:xls]
        post 'export_to_registry' => 'order#export_to_registry', format: [:xls]
        post 'change_delivery_amount' => 'order#change_delivery_amount'

        post 'add_product' => 'order#add_product', as: :add_order_product
      end

      scope :offer do
        get 'all.json' => 'offer#all'
        get 'price' => 'offer#price'
        post 'new' => 'offer#new'
        post 'update' => 'offer#update'
        post 'delete' => 'offer#delete'
      end

      scope :product do
        get 'all.json' => 'product#all'
        post 'new' => 'product#new'
        post 'update' => 'product#update'
        post 'delete' => 'product#delete'
      end

      scope :warehouse do
        get 'all.json'  => 'warehouse#all'

        post 'new'      => 'warehouse#new'
        post 'update'   => 'warehouse#update'
        post 'delete'   => 'warehouse#delete'

        scope :arrival do
          post 'new'      => 'arrival#new'
          post 'update'   => 'arrival#update'
          post 'delete'   => 'arrival#delete'
        end

        scope :outgo do
          post 'new'      => 'outgo#new'
          post 'update'   => 'outgo#update'
          post 'delete'   => 'outgo#delete'
        end
        
      end


    end

    # Angular patch
    match "/*path" => redirect("/advert/?goto=%{path}"), via: [:get]
  end

  namespace :webmaster do
    get '/' => 'index#index'
    get 'all' => 'index#serialization', format: [:json]
    get 'contract/statistic'

    get 'invite/:key' => 'index#invite'

    scope :contract do
      post 'statistic' => 'contract#statistic', format: [:json]
      post 'generate_balances' => 'contract#generate_balances', format: [:json]
    end

    scope :offer do
      post 'connect' => 'offer#connect', format: [:json]
    end
    
    # Angular patch
    match "/*path" => redirect("/webmaster/?goto=%{path}"), via: [:get]
  end

  scope 'sandbox' do
    get 's.json' => 'sandbox#s'
  end

  scope :landing do
    match 'new_offer' => 'landing#new_offer', via: [:get, :post]
  end

end
