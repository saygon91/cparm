require File.expand_path('../boot', __FILE__)
require 'csv'
require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Cparm
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    config.time_zone = 'Astana'
    config.active_record.default_timezone = :utc
    #rake time:zones:all



    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    # Do not swallow errors in after_commit/after_rollback callbacks.
    config.system_user_email = 'system.cparm@0day.kz'
    config.active_record.raise_in_transactional_callbacks = true
    config.xls_export_files_path = Rails.root.join('public/export/').to_path

    # API Tokens
    config.monsters_lead_token = 'd196e5f6fa1'
    config.monsters_lead_api_key = 'fce609c5f3c25b1f5e7dce53ba73b501'

    config.action_mailer.delivery_method = :smtp
    config.action_mailer.default_url_options = {:host => 'cparm.0day.kz', :from => 'cparm.notifications@yandex.ru'}
    config.action_mailer.raise_delivery_errors = true
    config.action_mailer.perform_deliveries = true
    config.action_mailer.smtp_settings = {
        # SLAVETKSII
        address: 'smtp.yandex.ru',
        port: 587,
        domain: 'cparm.0day.kz',
        user_name: 'cparm.notifications@yandex.ru',
        password: 's9n3m19f13dglz1',
        authentication: :plain,
        enable_starttls_auto: true
    }

  end
end
# #
# puts "Activerecord Default timezone #{ActiveRecord::Base.default_timezone}"
# ActiveRecord::Base.default_timezone = :local
# puts "Activerecord Default timezone #{ActiveRecord::Base.default_timezone}"

class String
  def numeric?
    !(self =~ /^-?\d+(\.\d*)?$/).nil?
  end
end