class LogSerializer < ActiveModel::Serializer
  attributes :user, :action_id, :company_id, :user_id, :order_id, :field_id, :order_product_id, :old_value, :new_value

  def user
    return "#{object.user.name}" if object.user
    "неизвестно"
  end

end
