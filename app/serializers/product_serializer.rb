class ProductSerializer < ActiveModel::Serializer
  attributes :id, :name, :count, :weight, :price, :lat_name, :delivery_amount, :description, :sale_two, :sale_three

end
