class UserSerializer < ActiveModel::Serializer
  attributes :id, :email, :first_name, :last_name, :skype, :icq, :country, :phone_number, :avatar, :balance
  has_many :balances
  #has_many :companies

  #except :created_at, :updated_at
end
