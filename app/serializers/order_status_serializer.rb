class OrderStatusSerializer < ActiveModel::Serializer
  attributes :id, :status
end
