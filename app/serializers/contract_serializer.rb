class ContractSerializer < ActiveModel::Serializer
  attributes :id, :webmaster_id, :offer, :accepted, :denied, :company_id, :orders_count, :offer, :urls,:utm_source ,:utm_medium,:utm_campaign,:utm_term,:utm_content, :count


  def orders_count
    @orders_count = object.orders.count
  end

  def count
    # НОВЫЙ	ПОДТВЕРЖДЕН	ОТПРАВЛЕН	ОПЛАЧЕН	ОТМЕНЕН
    status_accepted  = object.company.dictionaries.find_by_name('Подтвержден')
    status_sended  = object.company.dictionaries.find_by_name('Отправлен')
    status_payed  = object.company.dictionaries.find_by_name('Оплачен')
    status_canceled  = object.company.dictionaries.find_by_name('Отменен')
    status_new = object.company.dictionaries.find_by_name('Новый')

    count = {new: 0, accepted: 0, sended: 0, payed: 0, canceled: 0}

    object.orders.map { |order|
      case order.status_id
        when status_new.id        then count[:new] += 1
        when status_accepted.id   then count[:accepted] += 1
        when status_sended.id     then count[:sended] += 1
        when status_payed.id      then count[:payed] += 1
        when status_canceled.id   then count[:canceled] += 1
      end
    }
    count
  end

  def urls
    @urls = []
    object.offer.landing_pages.map {|lp|
      @urls << lp.url
    } if object.offer
    @urls
  end

end
