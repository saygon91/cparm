class OrderProductSerializer < ActiveModel::Serializer
  attributes :id, :order_id, :company_id, :product_id, :count, :discount, :price, :percent
end
