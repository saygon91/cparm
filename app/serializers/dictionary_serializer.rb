class DictionarySerializer < ActiveModel::Serializer
  attributes :id, :name, :object_type, :additional, :company_id
end
