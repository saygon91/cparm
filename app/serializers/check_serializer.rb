class CheckSerializer < ActiveModel::Serializer
  attributes :id, :user, :user_id,  :webmaster_id, :amount, :comment, :webmaster, :created_at, :date_from, :date_to

  def webmaster
    u = User.find_by_id(object.webmaster_id)
    UserSerializer.new(u, root: false) if not u.nil?
  end
end
