class EmployeeSerializer < ActiveModel::Serializer
  attributes :id, :company_id, :user_id, :roles, :user, :blocked, :blocked_at, :unblocked_at, :block_reason

  def roles
    all_roles = {}
    object.user.roles.where(resource_id: object.company_id).map {|role| all_roles[role.name] = true}
    all_roles
  end
end
