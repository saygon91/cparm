class CompanySerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :webmasters, :filter_actions, :filter_fields, :customers, :barcode_prefix, :last_barcode
  has_many :warehouses
  has_many :offers
  has_many :products
  has_many :employees
  has_many :contracts
  has_many :roles
  has_many :orders
  has_many :logs
  has_many :dictionaries
  has_many :checks

  def orders
    status_recall  = object.dictionaries.find_by(name: 'Перезвонить', object_type: DICTIONARY_TYPES.index('статус заказа'))
    status_send  = object.dictionaries.find_by(name: 'Недозвонился', object_type: DICTIONARY_TYPES.index('статус заказа'))
    status_new = object.dictionaries.find_by(name: 'Новый', object_type: DICTIONARY_TYPES.index('статус заказа'))

    @orders = []
    if scope and not scope.has_role? :advert_admin, object
      object.orders.map { |order|

        if scope.has_role? :call_manager, object or scope.has_role? :call_operator, object
          # Call operator && Call manager
          case order.status_id
            # Перезвонить
            when status_recall.id
            when status_send.id
            when status_new.id
              @orders << order
            else
              @orders << order if scope.has_role? :log_manager, object or scope.has_role? :log_operator
          end
        elsif scope.has_role? :log_manager, object or scope.has_role? :log_operator, object
          # Log operator && Log manager
          case order.status_id
            # Перезвонить
            when proc { |s| status_recall.id != s }
            when proc { |s| status_send.id != s }
            when proc { |s| status_new.id != s }
              @orders << order
          end

        end

      }
      # if scope.has_role? :log_manager, object or scope.has_role? :log_operator, object
      #   if scope.has_role? :call_manager, object or scope.has_role? :call_operator, object
      #     @orders = object.orders.all.limit(100)
      #   else
      #     @orders = object.orders.where(status_id: object.dictionaries.find_by_name('Подтвержден').id).limit(100)
      #   end
      # elsif scope.has_role? :call_manager, object or scope.has_role? :call_operator, object
      #   @orders = object.orders.where.not(status_id: object.dictionaries.find_by_name('Подтвержден').id).limit(100)
      # else
      #   puts "#{scope.email} -> #{scope.roles_name} "
      # end
    else
      @orders = object.orders.all.limit(100)
      puts "ADMIN ORDERS"
    end


    @orders
  end

  def customers
    @customers = []
    object.orders.map {|order| @customers << order.customer if order.customer}
    @customers
  end

  def filter_actions
    @filter_actions = LOG_ACTIONS
  end

  def filter_fields
    @filter_fields = LOG_ORDER_FIELDS
  end

  def roles
    return scope.roles.where(resource_id: object.id) if scope and object
    return []
  end


  def webmasters
    @webmasters ||= []

    object.contracts.map {|c|
      @webmasters <<  c.webmaster if c.webmaster and not @webmasters.include? c.webmaster
    } if object
    result = []
    @webmasters.map { |wm|
      result << UserSerializer.new(wm, root: false)
    }
    result
  end

end
