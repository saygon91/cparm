class MessageSerializer < ActiveModel::Serializer
  attributes :id, :message_type, :message, :src, :dst, :deleted, :company_id, :read, :readed_at, :created_at
end
