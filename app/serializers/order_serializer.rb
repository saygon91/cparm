class OrderSerializer < ActiveModel::Serializer
  attributes :id, :offer_id, :company_id, :contract_id, :customer, :status_id,
      :total_amount, :delivery_type_id, :logistic_status_id, :delivery,
      :courier, :delivery_amount, :landing_page, :barcode, :delivery_date, :recall_date, :total_amount_discount, :total_amount_percent, :lead_key

  has_many :order_products

  def delivery_date
     object.delivery_date.strftime("%FT%T.%LZ") if not object.delivery_date.nil?
  end

  def recall_date
    object.recall_date.strftime("%FT%T.%LZ") if not object.recall_date.nil?
  end

end

# Order.where(status_id: 17).map { |order|
#   if not order.lead_key.nil?
#     puts "Send accepted for #{order.id}"
#     order.monsters_leads_accepted
#   end
# }