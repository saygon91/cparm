class CustomerSerializer < ActiveModel::Serializer
  attributes :id, :name, :first_name, :last_name, :phone, :address, :zip, :comment, :city,  :email, :country, :province, :address, :role


  def name
    "#{first_name} #{last_name}"
  end
end
