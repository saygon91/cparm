class WarehouseSerializer < ActiveModel::Serializer
  attributes :id, :name, :company_id
  has_many :warehouse_products
  has_many :arrivals
  has_many :outgos
end
