class BalanceSerializer < ActiveModel::Serializer
  attributes :id, :order_id, :amount, :user_id, :payed, :company_id
end
