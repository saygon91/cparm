class WarehouseProductSerializer < ActiveModel::Serializer
  attributes :id, :product_id, :warehouse_id, :count
end
