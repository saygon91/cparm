class OfferSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :product_id, :connected, :geo, :private, :currency, :price

  has_many :geos
  has_many :news
  has_many :landing_pages

  def geo
    return geos.first.name if geos.count > 0
    return "неизвестно"
  end

  def connected

    if object and not scope.nil? and scope.is_webmaster?
      return scope.contracts.where(offer: object).count > 0 ? true : false
    end

    false

  end
end
