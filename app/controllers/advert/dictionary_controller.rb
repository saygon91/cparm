class Advert::DictionaryController < AdvertBaseController
  # /advert/company/dictionary/new
  # New dictionarie with params[object_type, name]
  # Название пусть будет dictionarie
  def new
    logger.info "#{__FILE__} : #{__method__} : New dictionary word #{params}"

    if @company and current_user.has_role? :advert_admin, @company

      @response[:dictionarie] = @company.dictionaries.new(params[:word])

      if not @response[:dictionarie].save
        @response[:dictionarie].errors.full_messages.map { |em| @response[:errors] << em}
        @response[:success] = false
      end
    else

      @response[:success] = false
      @response[:errors] << 'У вас недостаточно прав' if not current_user.has_role? :advert_admin, @company

    end
    render json: @response
  end

  # /advert/company/dictionary/update
  # company_id, dictionarie
  def update
    if @company and params.has_key? :dictionary and current_user.has_role? :advert_admin, @company
      dictionary = @company.dictionaries.find_by_id(params.require(:dictionary).require(:id))
      if dictionary
        if dictionary.update_attributes(name: params.require(:dictionary).require(:name))
          @response[:dictionarie] = DictionarySerializer.new(dictionary, root: false)
        end
      end
    else
      @response[:success] = false
      @response[:errors] << 'У вас недостаточно прав!' if not current_user.has_role? :advert_admin, @company
    end

    render json: @response
  end

  def element
    :dictionary
  end

end