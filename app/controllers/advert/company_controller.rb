class Advert::CompanyController < AdvertBaseController

  # /advert/company/all.json
  def all
    @response[:companies] = current_user.all_companies
    @response[:roles] = ROLES
    @response[:dictionary] = DICTIONARY_TYPES
    @response[:delivery_types] = DELIVERY_TYPES
    @response[:currency] = CURRENCY
    @response[:user] = UserSerializer.new(current_user, root: false)

    #render json: @response
    render json: @response
  end

  # POST /advert/company/billing/calculate
  # company_id, webmaster_id, pay:boolean, date_from, date_to, comment
  def calculate
    logger.info "PAY IS FALSE" if params[:pay] == false
    logger.info "PAY IS TRUE" if params[:pay] == true

    @webmasters = []
    @response[:checks] = []
    if @company and not params.has_key? :webmaster_id or params[:webmaster_id].nil?
      logger.info "Load all WM checks"
      @webmasters = @company.webmasters
    elsif params.has_key? :webmaster_id and not params[:webmaster_id].nil?
      @company.webmasters.each do |wm|
        logger.info "Load #{wm.email} checks"
        @webmasters << wm if wm.id == params[:webmaster_id]
      end
    end


    if @company and current_user.owner(@company) and not @webmasters.count < 0
      total_amount = 0

      @webmasters.each do |webmaster|
        logger.info "Find #{webmaster.email} checks"
        @company.balances.where("created_at > ? and created_at < ? and payed = 'f' and user_id = '?'", params[:date_from], params[:date_to], webmaster.id).map { |b|
          total_amount += b.amount
          logger.info "Amount #{total_amount}"
        }

        check = @company.checks.new(user: current_user, webmaster_id: webmaster.id, amount: total_amount, date_from: params[:date_from], date_to: params[:date_to])
        @response[:checks] << check
      end

    else
      @response[:errors] << 'Не найден webmaster' if @webmasters.count < 0
    end

    render json: @response
  end

  # POST /advert/company/salary
  # company_id, date_from, date_to
  def salary
    if @company and current_user.owner(@company)
      payed_status = @company.dictionaries.find_by_name('Подтвержден')
      salary = {}

      @company.logs.where("created_at > ? and created_at < ? and new_value = '?'", params[:date_from], params[:date_to], params[:status_id]).map {|log|
        prcnt = 1.0
        salary[log.user.email] = {} if salary[log.user.email].nil?
        salary[log.user.email][:amount] = 0       if salary[log.user.email][:amount].nil?
        salary[log.user.email][:orders_count] = 1 if salary[log.user.email][:orders_count].nil?
        salary[log.user.email][:salary] = 0       if salary[log.user.email][:salary].nil?
        salary[log.user.email][:orders_count] += 1


        salary[log.user.email][:amount] += log.order.total_amount

        log.order.order_products.count > 1 ? prcnt = 1.5 : prcnt = 1.0

        salary[log.user.email][:salary] += (log.order.total_amount / 100) * 1.5

      }

      @response[:salary] = salary
    end

    render json: @response
  end

  # POST /advert/company/logs
  # company_id, limit
  def logs
    if @company
      log_limit = params[:limit].nil? ? 100 : params[:limit]

      @company.logs.limit(log_limit).map {|l|
        @response[:logs] << LogSerializer.new(l, root: false)
      }
    end

    render json: @response
  end

  # POST /advert/company/update
  # company_id, company{name, description}
  def update
    if @company and params.has_key? :company and current_user.companies.find_by_id(@company.id)
      @response[:company] = @company if @company.update_attributes(params[:company])
    else
      @response[:success] = false
      @response[:errors] << 'Не найдена компания' if @company.nil?
      @response[:errors] << 'Вы указали неверные данные' if not params.has_key? :company
    end

    render json: @response
  end

  # POST /advert/company/new
  # company{name, description}
  def new
    if current_user.has_role? :advert and params.has_key? :company
      company_params = params.require(:company)
      company = current_user.create_company(company_params)
      if company
        @response[:company] = CompanySerializer.new(company, root: false,scope: current_user)
      else
        @response[:success] = false
        @response[:errors] += company.errors.full_messages
      end
    else
      @response[:success] = false
      @response[:errors] << 'У Вас нет прав для создания компании'
    end
    # if params.has_key? :company
    #
    #   company_params = params.require(:company)
    #
    #   @response[:company] = current_user.companies.find_or_create_by(name: company_params[:name])
    #   if @response[:company]
    #     @response[:company].update_attributes(description: company_params[:description])
    #     @response[:company] = CompanySerializer.new(@response[:company], root: false)
    #   else
    #     @response[:success] = false
    #   end
    # else
    #   @response[:errors] << 'Отсутствуют данные о новой компании'
    #   @response[:success] = false
    # end

    render json: @response
  end

  def element
    :company
  end

end