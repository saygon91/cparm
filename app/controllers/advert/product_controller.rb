class Advert::ProductController < AdvertBaseController

  # /advert/company/product/all.json
  # All company products
  def all
    if @company and current_user.has_access(__method__, element)
      @response[:products] = []
      @company.products.each do |product|
        @response[:products] << ProductSerializer.new(product, root: false)
      end
    end

    render json: @response
  end

  # /advert/company/product/update
  # Update Product by product_id with params[:product]
  def update
    if @company and @product and current_user.has_access(__method__, element)
      if @product.update_attributes(params[:product])
        @response[:product] = @product
      else
        @response[:success] = false
      end
    end

    render json: @response
  end

  # /advert/company/product/new
  # New Product with params[:product]
  def new
    logger.info "#{__FILE__} : #{__method__} : New product #{params}"

    if @company and current_user.has_access(__method__, element)

      @response[:product] = @company.products.new(params[:product])

      if not @response[:product].save
        @response[:product].errors.full_messages.map { |em| @response[:errors] << em}
        @response[:success] = false
      end
    else

      @response[:success] = false
      @response[:errors] << 'У вас недостаточно прав' if not current_user.is_advert?
      @response[:errors] << 'Компания не найдена'     if @company.nil?

    end
    render json: @response
  end

  # POST /advert/company/product/delete
  # Delete Product by product_id
  def delete
    if @company and @product and current_user.has_access(__method__, element)
      @response[:success] = false if not @product.destroy
    else
      @response[:errors] << 'Компания не найдена' if !@company
      @response[:errors] << 'Товар не найден'     if !@product
    end
    render json: @response
  end

  def element
    :product
  end
end
