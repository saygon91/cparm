class Advert::OutgoController < AdvertBaseController

  # POST /advert/company/warehouse/outgo/new
  # company_id, warehouse_product_id, count, comment
  # Create new product outgo from warehouse
  def new
    if @company and @warehouse and @product and current_user.has_access(__method__, element)
      @wp = @warehouse.warehouse_products.find_by(product: @product)

      if @wp
        logger.info "WP #{@wp.to_json}"
        if @wp.count < params[:count].to_i
          @response[:success] = false
          @response[:errors] << "На складе не хватает товара епта! На складу осталось (#{@wp.count})."
        else
          if @wp.update_attributes(count: @wp.count - params[:count].to_i)
            @response[:outgo] = @warehouse.outgos.create(product: @product, count: params[:count], user: current_user)
            @response[:warehouse_product] = WarehouseProductSerializer.new(@wp, root: false)
          else
            @response[:success] = false
            @response[:errors] << 'Не удалось вычесть товар со склада'
          end
        end
      else
        @response[:success] = false
        @response[:errors] << 'На складе такого товара нет! Добавьте товар на склад'
      end
    else
      @response[:success] = false
    end

    render json: @response
  end

  # POST
  def update
  end

  def delete
  end

  def element
    :outgo
  end
end
