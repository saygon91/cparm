class Advert::CustomerController < AdvertBaseController

  def update
    if @company and @customer and params.has_key? :customer
      if @customer.update_customer(params.require(:customer), current_user, @company)
        @response[:customer] = CustomerSerializer.new(@customer, root: false)
      else
        @response[:errors] << 'Не найден клиент' if @customer.nil?
        @response[:success] = false
      end
    end

    render json: @response
  end

end
