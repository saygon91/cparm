class Advert::ArrivalController < AdvertBaseController

  # POST /advert/company/warehouse/arrival
  # New product Arrival to warehouse
  # company_id, warehouse_id, product_id, count, comment
  def new
    if @company and @warehouse and @product and current_user.has_access(__method__, element)
      @response[:arrival] = @warehouse.arrivals.create(product: @product,warehouse: @warehouse,count: params[:count],comment: params[:comment], user: current_user)

      if @response[:arrival]
        @wp = @warehouse.warehouse_products.where(product: @product).first_or_create



        if @wp
          new_count = params[:count].to_i + @wp.count

          @response[:warehouse_product] = WarehouseProductSerializer.new(@wp, root: false) if @wp.update_attributes(count: new_count)

        end
      end

    else
      @response[:success] = false
    end

    render json: @response
  end

  def update
  end

  def delete
  end

  def element
    :arrival
  end
end
