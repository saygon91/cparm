class Advert::OrderController < AdvertBaseController

  layout 'blank', only: :print_orders_form

  # /advert/company/order/print_orders_form
  # company_id, order_ids
  def print_orders_form
  end

  # /advert/company/order/accepted_statistic
  def accepted_statistic
    @response[:statistic] ||= {accepted: 0, sended: 0, payed: 0, canceled: 0, new: 0, returned: 0}

    if @company
      status_accepted  = @company.dictionaries.find_by(name: 'Подтвержден', object_type: DICTIONARY_TYPES.index('статус заказа'))
      status_send  = @company.dictionaries.find_by(name: 'Отправлен', object_type: DICTIONARY_TYPES.index('статус заказа'))
      status_payed  = @company.dictionaries.find_by(name: 'Оплачен', object_type: DICTIONARY_TYPES.index('статус заказа'))
      status_canceled  = @company.dictionaries.find_by(name: 'Отменен', object_type: DICTIONARY_TYPES.index('статус заказа'))
      status_new = @company.dictionaries.find_by(name: 'Новый', object_type: DICTIONARY_TYPES.index('статус заказа'))
      status_returned = @company.dictionaries.find_by(name: 'Возврат', object_type: DICTIONARY_TYPES.index('статус заказа'))
      status_defect = @company.dictionaries.find_by(name: 'Брак', object_type: DICTIONARY_TYPES.index('статус заказа'))
      status_other_region = @company.dictionaries.find_by(name: 'Другой Регион', object_type: DICTIONARY_TYPES.index('статус заказа'))
      status_recall = @company.dictionaries.find_by(name: 'Недозвонился', object_type: DICTIONARY_TYPES.index('статус заказа'))

      logger.info "accepted = #{status_accepted.id}, sended = #{status_send.id}, payed = #{status_payed.id}, canceled = #{status_canceled.id}, new : #{status_new.id}, returned: #{status_returned.id}"


      result_logs = @company.logs.where("created_at > ? and created_at < ? and new_value = ?", params[:date_from], params[:date_to], status_accepted.id)


      @response[:statistic][:all] = result_logs.count
      result_logs.map { |log|
        logger.info "Parsing order with status_id #{log.order.status_id}"
        case log.order.status_id
          when status_new.id        then @response[:statistic][:new] += 1
          when status_accepted.id   then @response[:statistic][:accepted] += 1
          when status_send.id       then @response[:statistic][:sended] += 1
          when status_payed.id      then @response[:statistic][:payed] += 1
          when status_canceled.id   then @response[:statistic][:canceled] += 1
          when status_returned.id   then @response[:statistic][:returned] += 1
          when status_defect.id     then @response[:statistic][:defect] += 1
          when status_other_region.id then @response[:statistic][:other_region] += 1
          when status_recall.id     then @response[:statistic][:recall] += 1
          else
            logger.info "Else #{log.order.status_id} with class #{log.order.status_id.class} | #{log.order.status}"

        end
      }
    end

    render json: @response
  end

  # /advert/company/order/generate_statistic
  def generate_statistic
    @response[:statistic] ||= {accepted: 0, sended: 0, payed: 0, canceled: 0, new: 0, returned: 0}

    if @company
      status_accepted  = @company.dictionaries.find_by(name: 'Подтвержден', object_type: DICTIONARY_TYPES.index('статус заказа'))
      status_send  = @company.dictionaries.find_by(name: 'Отправлен', object_type: DICTIONARY_TYPES.index('статус заказа'))
      status_payed  = @company.dictionaries.find_by(name: 'Оплачен', object_type: DICTIONARY_TYPES.index('статус заказа'))
      status_canceled  = @company.dictionaries.find_by(name: 'Отменен', object_type: DICTIONARY_TYPES.index('статус заказа'))
      status_new = @company.dictionaries.find_by(name: 'Новый', object_type: DICTIONARY_TYPES.index('статус заказа'))
      status_returned = @company.dictionaries.find_by(name: 'Возврат', object_type: DICTIONARY_TYPES.index('статус заказа'))
      status_defect = @company.dictionaries.find_by(name: 'Брак', object_type: DICTIONARY_TYPES.index('статус заказа'))
      status_other_region = @company.dictionaries.find_by(name: 'Другой Регион', object_type: DICTIONARY_TYPES.index('статус заказа'))

      logger.info "accepted = #{status_accepted.id}, sended = #{status_send.id}, payed = #{status_payed.id}, canceled = #{status_canceled.id}, new : #{status_new.id}, returned: #{status_returned.id}"


      result_logs = @company.logs.where("created_at > ? and created_at < ? and old_value = '?' and new_value = '?'", params[:date_from], params[:date_to], params[:status_from], params[:status_to])

      # logger.info "Result by time #{result_logs.as_json}"
      # if not params[:status_from].nil? and params[:status_from].to_s.length > 0
      #   result_logs = result_logs.where(old_value: params[:status_from])
      #   logger.info "Filter for statistic by status_from #{params[:status_from]} \n Result : #{result_logs.as_json}"
      # end
      #
      # if not params[:status_to].nil? and params[:status_to].to_s.length > 0
      #   result_logs = result_logs.where(new_value: params[:status_to])
      #   logger.info "Filter for statistic by status_to #{params[:status_from]} \n Result  : #{result_logs.as_json}"
      # end

      @response[:statistic][:all] = result_logs.count
      result_logs.map { |log|
        logger.info "Parsing order with status_id #{log.order.status_id}"
        case log.order.status_id
          when status_new.id        then @response[:statistic][:new] += 1
          when status_accepted.id   then @response[:statistic][:accepted] += 1
          when status_send.id       then @response[:statistic][:sended] += 1
          when status_payed.id      then @response[:statistic][:payed] += 1
          when status_canceled.id   then @response[:statistic][:canceled] += 1
          when status_returned.id   then @response[:statistic][:returned] += 1
          # when status_defect.id     then @response[:statistic][:defect] += 1
          # when status_other_region.id then @response[:statistic][:other_region] += 1
          else
            logger.info "Else #{log.order.status_id} with class #{log.order.status_id.class} | #{log.order.status}"

        end
      }
    end

    render json: @response
  end

  # /advert/company/order/export_to_registry
  def export_to_registry
    if @company and @orders
      registry_file = Order.registry_to_xls(params[:order_ids])
      registry_file_name = registry_file.split('/')[-1]

      download_url = "#{request.protocol}#{request.host}:#{request.port}/export/#{registry_file_name}"

      logger.info "EXPORT TO REGISTRY : #{registry_file} to #{download_url}"
      @response[:registry_file] = download_url
    else
      @response[:success] = false
      @response[:registry_file] = ''
    end

    render json: @response
    # @response[:success] = false
  end

  # /advert/company/order/status_change
  # {company_id, ORDER_STATUS_ID, LOGISTIC_STATUS_ID, order_ids [1,2,3]}
  def status_change
    @response[:orders] = []

    # 1. if params has key status_id and logistic_status_id find orders by params[order_ids] and change order status
    if @company and @orders

      @orders.map {|order|
        if params.has_key? :status_id
          if order.status_id != @status_accepted.id and params[:status_id] == @status_accepted.id
            params[:logistic_status_id] = @status_need_send.id
          end

          order.change_status(:status_id, params[:status_id], current_user)
        end

        if params.has_key? :logistic_status_id
          order.change_status(:logistic_status_id, params[:logistic_status_id], current_user)
        end


        @response[:orders] << OrderSerializer.new(order, root: false)
      }
    end

    render json: @response
  end

  # /advert/company/order/search_orders
  # company_id, search_text, fields[id":true,"last_name":true,"first_name":true,"phone":true,"comment":true,"address":true}}
  def search_orders
    @response[:orders] ||= []
    @result_orders = []
    result = []
    if @company and params.has_key? :search_text and params.has_key? :fields
      if params.require(:fields).has_key? :global and params.require(:fields)[:global]
        logger.info "Search globally"
        params[:search_text].split("\n").map { |search_text|
          logger.info "Filter globally by [#{search_text}]"
          params[:fields].keys.map {|search_field|
            if Customer.attribute_names.include? search_field and params[:fields].has_key? search_field and params[:fields][search_field] == true
              logger.info "Customer search key #{search_field} - Numeric ? #{search_field.eql? 'id'}, value : #{search_text} numeric? #{search_text.numeric?}"
              logger.info "#{params[:fields][search_field]}"
              if search_field.eql? "id"
                if search_text.numeric?
                  @company.customers.where(id: search_text).map {|customer| customer.orders.map {|order| result << order} }
                end
              else
                @company.customers.where("lower(#{search_field}) LIKE ?", "%#{search_text}%").map {|customer| customer.orders.map {|order| result << order} }
              end
            end


            if Order.attribute_names.include? search_field and params[:fields].has_key? search_field and params[:fields][search_field] == true
              logger.info "Order search key #{search_field} - Numeric ? #{search_field.numeric?}, value : #{search_text} numeric? #{search_text.numeric?}"
              if search_field.eql? "id"
                if search_text.numeric?
                  @company.orders.where(id: search_text).map {|order| result << order}
                end
              else
                @company.orders.where("lower(#{search_field}) LIKE ?","%#{search_text}%").map {|order| result << order}
              end
            end
          }
        }
      else
        logger.info "Search once"
        params[:fields].keys.map {|search_field|
          if Customer.attribute_names.include? search_field and params[:fields].has_key? search_field  and params[:fields][search_field]
            logger.info "Customer search key #{search_field} - Numeric ? #{search_field.eql? 'id'}"
            if search_field.eql? "id" and params[:search_text].numeric?
              @company.customers.where(id: params[:search_text]).map {|customer| customer.orders.map {|order| result << order} }
            else
              @company.customers.where("lower(#{search_field}) LIKE ?", "%#{params[:search_text]}%").map {|customer| customer.orders.map {|order| result << order} }
            end

          end


          if Order.attribute_names.include? search_field and params[:fields].has_key? search_field  and params[:fields][search_field]
            logger.info "order search key #{search_field}"
            if search_field.eql? "id" and params[:search_text].numeric?
              @company.orders.where(id: params[:search_text]).map {|order| result << order}
            else
              @company.orders.where("lower(#{search_field}) LIKE ?","%#{params[:search_text]}%").map {|order| result << order}
            end
          end
        }

      end

      logger.info "Result orders #{result.to_json}"
      result.map {|order| @result_orders << order if not @result_orders.include? order}
      @result_orders.map { |order|
        @response[:orders] << OrderSerializer.new(order, root: false)
      }
    end


    render json: @response
  end

  # /advert/company/order/filter_logs
  # { company_id, action_id, field_id, dictionary_id }
  def filter_logs
    # TODO Рефакторить код на безопасность
    result = []
    result_orders = []
    log_result = []
    @response[:orders] = []
    if @company

      # Парсим дату
      if params[:date_from].nil? or params[:date_from].length <= 0
        date_from = FILTER_SEARCH_START_DATE
      else
        date_from = params[:date_from].to_datetime.to_s
      end
      if params[:date_to].nil? or params[:date_to].length <= 0
        date_to = FILTER_SEARCH_END_DATE
      else
        date_to = params[:date_to].to_datetime.to_s
      end

      logger.info "Start filtering orders from #{date_from}"
      logger.info "Start filtering orders to #{date_to}"




      log_result += @company.logs.where('created_at > ? and created_at < ?', date_from, date_to)


      if params.has_key? :filters and not params[:filters].nil? and params[:filters].length > 0
        # Выбраны фильтры по журналу
        # Filter param {"dictionary_filter_type_id"=>5, "action_id"=>"1", "field_id"=>"1", "dictionary_id"=>24}

        params[:filters].map { |filter_param|
          logger.info "Filter param #{filter_param}"
          log_result.map { |log|
            if log.action_id.to_s == filter_param[:action_id] and log.field_id.to_s == filter_param[:field_id].to_s and log.new_value.to_s.eql? filter_param[:dictionary_id].to_s
              logger.info "Log param #{log.as_json}"
              result << log.order if not result.include? log.order
            else
              logger.info "Log action_id \n #{log.action_id.to_s} == #{filter_param[:action_id].class} #{log.new_value.class} != #{filter_param[:dictionary_id].class} "
            end
          }
        }
        # params[:filters].map {|filter_param|
        #   logger.info "Filter param #{filter_param}"
        #   if filter_param[:field_id] and DICTIONARY_TYPES.include? LOG_ORDER_FIELDS[filter_param[:field_id].to_i]
        #     # Если поиск со словаря
        #     log_result = log_result.where('delivery_date > ?', filter_param[:delivery_date]).all if filter_param[:delivery_date]
        #     log_result = log_result.where('delivery_date > ?', filter_param[:recall_date]).all if filter_param[:recall_date]
        #     .where(action_id: filter_param[:action_id], field_id: filter_param[:field_id], new_value: filter_param[:dictionary_id])
        #   else
        #     # Поиск не по словарю
        #     log_result += @company.logs.where('created_at > ? and created_at < ?', date_from, date_to)
        #     .where(action_id: filter_param[:action_id], field_id: filter_param[:field_id])
        #   end
        #
        #   log_result.map {|log| result << log.order if not result.include? log.order}
        #   logger.info "#{result.count} results for #{filter_param}"
        # }

      else
        # Не выбраны фильтры по журналу
        @orders = @company.orders.where('created_at > ? and created_at < ?', date_from, date_to)


        #@orders = @orders.limit(params[:search][:limit]) if params[:search].has_key? :limit

        @orders.map {|order|
          result << order if not result.include? order
        }
      end


      search_params = params.require(:search)
      status_id_result = []
      logistic_status_id_result = []
      courier_result = []
      finish_result = []


      #
      # Фильтрация по status_id
      if search_params.has_key? :status_id and not search_params[:status_id].nil? and search_params[:status_id].length > 0
        logger.info 'Order status_id '+'-'*30
        result.map {|order|
          search_params[:status_id].map {|status_id|
            #logger.info "Status Reverse #{search_params[:status_reverse]}"
            #logger.info "#{search_params[:status_reverse]}"
            if search_params[:status_reverse] == "true"
              # НЕ ИМЕЕТ СТАТУС
              logger.info "Order #{order.id} status #{order.status_id} != #{search_params[:status_reverse]} #{status_id}"
              if order.status_id != status_id and not status_id_result.include? order
                status_id_result << order
              end
            else
              # ИМЕЕТ СТАТУС
              logger.info "Order #{order.id} status #{order.status_id} == #{search_params[:status_reverse]} #{status_id}"
              if order.status_id == status_id and not status_id_result.include? order
                status_id_result << order
              end
            end
          }
        }
        logger.info 'Order status_id '+'-'*30
      else
        logger.info "No status_id filter #{result}"
        status_id_result = result
      end



      #
      # Фильтрация по logistic_status_id
      if search_params.has_key? :logistic_status_id and not search_params[:logistic_status_id].nil? and search_params[:logistic_status_id].length > 0
        logger.info 'Order logistic_status_id '+'-'*30
        status_id_result.map {|order|
          search_params[:logistic_status_id].map { |logistic_status_id|
            if search_params[:logistic_status_reverse] == "true"
              # НЕ ИМЕЕТ СТАТУС
              logger.info "Order #{order.id} logistic_status_id #{order.logistic_status_id} ne imeet #{search_params[:logistic_status_reverse]} #{logistic_status_id}"
              if order.logistic_status_id != logistic_status_id and not logistic_status_id_result.include? order
                logistic_status_id_result << order
              end
            else
              # ИМЕЕТ СТАТУС
              logger.info "Order #{order.id} logistic_status_id #{order.logistic_status_id} imeet #{search_params[:logistic_status_reverse]} #{logistic_status_id}"
              if order.logistic_status_id == logistic_status_id and not logistic_status_id_result.include? order
                logistic_status_id_result << order
              end
            end
          }
        }
        logger.info 'Order logistic_status_id '+'-'*30
      else
        logger.info "No logistic_status_Id filter"
        logistic_status_id_result = status_id_result
      end



      #
      # Фильтрация по courier
      if search_params.has_key? :courier and search_params[:courier].length > 0
        logger.info 'Order courier '+'-'*30
        logistic_status_id_result.map {|order|
          search_params[:courier].map {|courier|

            if order.courier == courier and not courier_result.include? order
              logger.info "Compare courier order [#{order.courier}] == #{courier}"
              courier_result << order
            else
              logger.info "Compare courier order [#{order.courier}] != #{courier}"
            end
          }
        }
        logger.info 'Order end courier '+'-'*30
      else
        logger.info "No courier filter"
        courier_result = logistic_status_id_result
      end



      #
      # Фильтрация по zip
      if search_params.has_key? :zip and search_params[:zip].length > 0
        courier_result.map {|order|
          search_params[:zip].map {|zip|
            if order.zip == zip and not finish_result.include? order
              finish_result << order
            else

            end
          }
        }

      else
        logger.info "No zip filter"
        finish_result = courier_result
      end


      webmasters_filter = finish_result
      #
      # Фильтр по webmaster
      if search_params.has_key? :webmasters and search_params[:webmasters].length > 0
        finish_result = []
        webmasters_filter.map { |order|
          search_params[:webmasters].map { |webmaster_id|
            finish_result << order if order.contract and order.contract.webmaster_id == webmaster_id and not finish_result.include? order
          }
        }

      end


      #
      # Фильтр по продуктам
      new_result = []
      if search_params.has_key? :products and search_params[:products].length > 0
        search_params[:products].map { |order_product_id|
          Product.where(id: order_product_id).map { |product|
            logger.info "Filter by product #{product.name}"
            finish_result.map { |order|
              order.order_products.map { |op| new_result << order if op.product == product }
              # if not order.order_products.include? product
              #   finish_result.delete(order)
              #   logger.info "Order not include product #{order_product.product.name}"
              # else
              #   logger.info "Order included product #{order_product.product.name}"
              # end
            }
          }
        }

        finish_result = new_result
      end

      #
      # Фильтр по дате доставки и дате перезвона
      [:delivery_date_from, :delivery_date_to, :recall_date_from, :recall_date_to].map { |key|
        if search_params.has_key? key and search_params[key].length <= 0
          search_params.delete(key)
          logger.info "Remove search key #{key}"
        end
      }


      finish_result = filter_order_by_key(:delivery_date_from, finish_result, search_params)
      finish_result = filter_order_by_key(:delivery_date_to, finish_result, search_params)
      finish_result = filter_order_by_key(:recall_date_from, finish_result, search_params)
      finish_result = filter_order_by_key(:recall_date_to, finish_result, search_params)

      # delivery_recall_result = finish_result
      # finish_result = []
      # delivery_recall_result.map { |order|
      #   # Recall
      #
      #   [:delivery_date_from, :delivery_date_to, :recall_date_from, :recall_date_to].map { |key|
      #
      #     if search_params.has_key? key and search_params[key].length > 0
      #       key_attr = key.to_s.split('_')[0..1].join('_').to_sym
      #       cond = key.to_s.split('_')[-1]
      #
      #
      #       if search_params.has_key? key and order[key_attr].class.eql? ActiveSupport::TimeWithZone and order.attribute_present? key_attr
      #
      #         logger.info "Filtering order with id #{order.id} for key #{key_attr} with param '#{search_params[key]}'"
      #
      #         # Удалить заказ и результата если дата доставки заказа меньше
      #         if cond.eql? 'from'
      #           logger.info "Filtering condition #{cond} with param #{search_params[key]}"
      #
      #           if order[key_attr] < search_params[key]
      #             logger.info "Filter by #{key} > Order[#{key_attr}] #{order[key_attr]} > #{search_params[key]}"
      #             #finish_result.delete(order)
      #           else
      #             logger.info "Order #{order.id} stay in result "
      #             finish_result << order if not finish_result.include? order
      #           end
      #         elsif cond.eql? 'to'
      #           logger.info "Filtering condition #{cond}"
      #
      #           if order[key_attr] > search_params[key]
      #             logger.info "Filter by #{key} < Order[#{key_attr}] #{order[key_attr]} < #{search_params[key]}"
      #             #finish_result.delete(order)
      #           else
      #             logger.info "Order #{order.id} stay in result "
      #             finish_result << order if not finish_result.include? order
      #           end
      #         end
      #
      #       else
      #         if order[key_attr].nil?
      #           logger.info "Order[#{key_attr}] is #{order[key_attr].nil?} so ##{order.id} deleted from result"
      #           #finish_result.delete(order)
      #           #finish_result << order if not finish_result.include? order
      #         end
      #
      #       end
      #     end
      #
      #   }
      #
      # }

      logger.info "Finish result #{finish_result}"
      finish_result.map { |order|
        @response[:orders] << OrderSerializer.new(order, root: false)
      }


    end
    render json: @response

  end

  def filter_order_by_key(key, finish_result, search_params)
    if search_params.has_key? key and search_params[key].length > 0
      temp_result = finish_result
      finish_result = []
      temp_result.map { |order|
        # Recall
        key_attr = key.to_s.split('_')[0..1].join('_').to_sym
        cond = key.to_s.split('_')[-1]

        if order[key_attr].class.eql? ActiveSupport::TimeWithZone and order.attribute_present? key_attr

          logger.info "Filtering order with id #{order.id} for key #{key_attr} with param '#{search_params[key]}'"

          # Удалить заказ и результата если дата доставки заказа меньше
          if cond.eql? 'from'
            logger.info "Filtering condition #{cond} with param #{search_params[key]}"

            if order[key_attr] < search_params[key]
              logger.info "Filter by #{key} > Order[#{key_attr}] #{order[key_attr]} > #{search_params[key]}"
              #finish_result.delete(order)
            else
              logger.info "Order #{order.id} stay in result "
              finish_result << order if not finish_result.include? order
            end
          elsif cond.eql? 'to'
            logger.info "Filtering condition #{cond}"

            if order[key_attr] > search_params[key]
              logger.info "Filter by #{key} < Order[#{key_attr}] #{order[key_attr]} < #{search_params[key]}"
              #finish_result.delete(order)
            else
              logger.info "Order #{order.id} stay in result "
              finish_result << order if not finish_result.include? order
            end
          end

        else
          if order[key_attr].nil?
            logger.info "Order[#{key_attr}] is #{order[key_attr].nil?} so ##{order.id} deleted from result"
            #finish_result.delete(order)
            #finish_result << order if not finish_result.include? order
          end

        end


      }
    else
      return finish_result
    end

    finish_result
  end

  # /advert/company/order/all.json
  # All company orders in json array
  def all

    if @company and current_user.has_role? :call_manager
      @response[:orders] = []
      @company.orders.each do |order|
        @response[:orders] << OrderSerializer.new(order, root: false)
      end
    end

    render json: @response
  end

  # /advert/company/order/close
  # c_id, o_id
  def close
    @order.closed(current_user.id) if @order
    render json: @response
  end

  # /advert/company/order/generate_barcodes
  def generate_barcodes
    logger.info "Barcode params #{params}"

    @response[:orders] ||= []

    if @company and current_user.has_role? :advert_admin, @company
      @company.orders.where(id: params.require(:order_ids)).map {|order|
        @response[:orders] << order.generate_barcode(params.require(:barcode_prefix), current_user)
      }
    else
      logger.info "Current user has :generate_barcode? #{current_user.has_role? :generate_barcode, @company}"
    end

    render json: @response
  end

  # /advert/company/order/logs
  # company_id, order_id
  def logs
    @order.opened(current_user.id) if @order and params.has_key? :open and params[:open]
    @response[:logs] = @order.logs_string if @company and @order
    render json: @response
  end

  # /advert/company/new_order_status
  # status
  def new_order_status
    new_status = OrderStatus.where(status: params[:status]).first_or_create
    if new_status
      @response[:status] = OrderStatusSerializer.new(new_status, root: false)
      @response[:alerts] << 'Добавлен новый статус'
    else
      @response[:success] = false
      @response[:errors] << 'Не удалось создать новый статус заказа! Обратитесь к разработчикам'
    end

    render json: @response
  end

  # /advert/company/order/update
  # Update company order by order_id with params[:order]
  def update
    if @company and @order and @order.update_order(params[:order], current_user)
      @response[:order] = OrderSerializer.new(@order, root: false)
      @response[:alerts] << 'Заказ успешно сохранен'
      @response[:alerts] += @order.errors.full_messages
    else
      @response[:errors] += @order.errors.full_messages
    end

    render json: @response
  end

  # /advert/company/order/new
  # company_id, order
  def new
    if @company
      @order = @company.orders.create(params[:order])

      if @order
        params[:order][:order_products].map { |op| @order_product = @order.order_products.create(op) }

        @response[:order] = OrderSerializer.new(@order, root: false)

        @response[:alerts] << 'Новый заказ создан'
      else
        @response[:errors] += @order.errors.full_messages
        @response[:success] = false
      end
    end

    render json: @company
  end

  # /advert/company/order/xls
  # Export orders to XLS file
  # company_id, order_ids[1,2,3], columns[name, fio, etc]
  def export_xls
    if @company and @orders and params.has_key? :columns
      export_file = Order.export_to_xls(params[:order_ids], params[:columns])
      export_file_name = export_file.split('/')[-1]

      download_url = "#{request.protocol}#{request.host}:#{request.port}/export/#{export_file_name}"

      logger.info "EXPORT TO XLS : #{export_file} to #{download_url}"
      @response[:export_file] = download_url
    else
      @response[:success] = false
      @response[:export_file] = ''
    end

    render json: @response
    # @response[:success] = false
  end

  # POST /advert/company/order/change_delivery_amount
  # company_id, order_ids, delivery_amount
  # Массовое распределение цен за доставку товара
  def change_delivery_amount
    @response[:orders] ||= []
    if @company and @orders and params.has_key? :delivery_amount
      @orders.map {|order|
        order.change_delivery_amount(params[:delivery_amount], current_user)

        @response[:orders] << OrderSerializer.new(order, root: false)
      }

    else
      @response[:errors] << 'Укажите цену доставки' if not params.has_key? :delivery_amount
      @response[:errors] << 'Выделите заказы' if not @orders or @orders.length <= 0
    end

    render json: @response

  end

  def add_product
    if @company and @order
      params[:order_products].map { |op|
        if not op.include? :id
          order_product = OrderProduct.where(offer_id: op[:offer_id], company_id: op[:company_id]).first_or_create
          @response[:order_products] << order_product if OrderProductSerializer.new(order_product, root: false)
        end

      }
    end

    render json: @response
  end

end
