class Advert::WarehouseController < AdvertBaseController

  # /advert/company/warehouse/all.json
  # All company warehouses
  def all

    if @company and current_user.has_access(__method__, element)
      @response[:warehouses] = []
      @company.warehouses.each do |warehouse|
        @response[:warehouses] << WarehouseSerializer.new(warehouse, root: false)
      end
    end

    render json: @response
  end

  # /advert/company/warehouse/update
  # Update warehouse by warehouse_id with params[:warehouse]
  def update
    if @company and @warehouse and current_user.has_access(__method__, element)
      if @warehouse.update_attributes(params[:warehouse])
        @response[:warehouse] = @warehouse
      else
        @response[:success] = false
      end
    end

    render json: @response
  end

  # /advert/company/warehouse/new
  # New warehouse with params[:warehouse]
  def new
    if @company and current_user.has_access(__method__, element)

      @response[:warehouse] = @company.warehouses.new(params[:warehouse])

      if not @response[:warehouse].save
        @response[:warehouse].errors.full_messages.map { |em| @response[:errors] << em}
        @response[:success] = false
      else
        @response[:warehouse] = WarehouseSerializer.new(@response[:warehouse], root: false)
      end
    else

      @response[:success] = false
      @response[:errors] << 'У вас недостаточно прав' if not current_user.is_advert?
      @response[:errors] << 'Компания не найдена'     if @company.nil?

    end
    render json: @response
  end

  # POST /advert/company/warehouse/delete
  # Delete warehouse by warehouse_id
  def delete
    if @company and @warehouse and current_user.has_access(__method__, element)
      @response[:success] = false if not @warehouse.destroy
    else
      @response[:errors] << 'Компания не найдена' if !@company
      @response[:errors] << 'Товар не найден'     if !@warehouse
    end
    render json: @response
  end


  # GET|POST /advert/company/warehouse/arrival
  def arrival

    if @company and @warehouse and request.post? and params.has_key? :arrivals
      params[:arrivals].each do |arriv_param|
        @response[:arrivals] = @warehouse.warehouse_products.create()
      end


    elsif request.get?


    end

  end

  # GET|POST /advert/company/warehouse/outgo
  def outgo

    if request.post?


    elsif request.get?


    end

  end

  def element
    :warehouse
  end
end
