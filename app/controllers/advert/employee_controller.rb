class Advert::EmployeeController < AdvertBaseController


  # /advert/company/employee/all
  def all
    @response[:employees] ||= []
    if @company
      @company.employees.each do |employee|
        @response[:employees] << EmployeeSerializer.new(employee, root: false)
      end
    else
      @response[:success] = false
      @response[:errors] << 'Компания не найдена' if @company.nil?
    end

    render json: @response
  end

  # /advert/company/employee/invite
  # company_id, employee[]
  def invite
    @employee_data = params[:employee] if params.has_key? :employee

    if @company and @employee_data

      # Find or create user
      @employee_user = User.find_by_email(@employee_data[:email])
      if @employee_user
        # User exists
      else
        # User not exists
        @employee_user = User.new(email: @employee_data[:email], first_name: '', last_name: '')
        @employee_password = SecureRandom.hex.to_s[0..10]
        @employee_user.password = @employee_password
        if @employee_user.save
          email = UserMailer.invite_email(@employee_user, @company, @employee_password)
          if email.deliver_now
            @response[:success] = true
          else
            @response[:success] = false
            @response[:errors] += @employee_user.errors.full_messages
          end
        end
      end

      @employee = @company.employees.find_or_create_by(user: @employee_user)
      if not @employee
        @response[:success] = false
        @response[:errors] += @employee.errors.full_messages
      else

        @employee.user.add_role :advert, @company

        if @employee_data.has_key? :roles
          @employee_data[:roles].keys.map {|k|
            # wtf ne pomnu shto za kod nizhe )
            #employee.user.add_role "advert_#{k.split('_')[1]}" if k.split('_').length > 1 and params[:employee][:roles][k]
            if SYSTEM_ROLES.include? k.to_sym and @employee_data[:roles][k]
              @employee.user.add_role k.to_sym, @company if not @employee.user.has_role? k.to_sym, @company
            else
              logger.info "Invalid role #{k} in #{SYSTEM_ROLES}"
            end
          }
        end

        @response[:employee] = EmployeeSerializer.new(@employee, root: false) if employee
      end



    end

    render json: @response
  end


  # /advert/company/employee/new
  # Метод для создания нового сотрудника в компании.
  # Если пользователь с таким email существует, то он добавляется
  # в список сотрудников
  def new
    params.has_key? :employee ? employee_data = params[:employee] : employee_data = nil

    # User password
    user_pwd = SecureRandom.hex.to_s[0..10]

    if @company and current_user.has_role? :advert_admin, @company and employee_data

      # 1. Если пользователь не существует, то надо его создать, иначе надо просто "найти" его
      # 2. Если пользователь создан то надо присвоить ему пароль и отправить письмо
      # 2. Создать модель Employee с найденным или созданным пользователем

      user = User.find_by_email(employee_data[:email])
      if not user
        # Пользователя надо создать

        user = User.new(email: employee_data[:email], password: user_pwd)
        user.first_name = employee_data[:first_name]  if employee_data.has_key? :first_name
        user.last_name = employee_data[:last_name]    if employee_data.has_key? :last_name

        if not user.save
          # Не удалось сохранить пользователя
          @response[:success] = false
          @response[:errors] += user.errors.full_messages
        end

      end


      if user
        # 1. Создаем модель Employee
        # 2. Отправляем invite
        employee = @company.employees.find_by_user_id(user.id)
        if not employee
          employee = @company.employees.create(user: user)

          if employee
            UserMailer.invite_email(user, @company, user_pwd).deliver_now

            employee.user.add_role :advert, @company

            employee_data[:roles].map {|k, state| employee.add_role(k) if state } if employee_data.has_key? :roles

            @response[:employee] = EmployeeSerializer.new(employee, root: false) if employee

          else
            @response[:success] = false
            @response[:errors] << 'Не удалось добавить сотрудника'
          end
        else
          @response[:success] = true
          @response[:messages] << 'Данный пользователь уже числится в сотрудниках компании'

        end


      else
        @response[:success] = false
        @response[:errors] << 'Не удалось найти или создать пользователя'

      end


    end


    render json: @response
  end


  # POST /advert/company/employee/update
  # Update employee roles
  def update
    if @company and @employee
      roles = params[:employee][:roles]

      # Delete all roles
      @employee.user.roles.where(resource_type: 'Company', resource_id: @company.id).map { |role| role.destroy } if @employee.user

      # Add advert role
      @employee.user.add_role :advert, @company

      roles.map { |role, state| @employee.add_role role if state }

      @response[:employee] = EmployeeSerializer.new(@employee, root: false)
    else
      @response[:success] = false
    end

    render json: @response
  end

  # POST /advert/company/employee/unblock
  # company_id, employee_id, unblock_reason
  def unblock
    # Block employee in company
    if @company and @employee
      if not @employee.unblock(params)
        @response[:success] = false
        @response.errors.full_messages.map { |e| @response[:errors] << e}
      else
        @response[:employee] = @employee
      end
    else
      @response[:errors] << 'Сотрудник не найден' if @employee.nil?
      @response[:errors] << 'Компания не найдена' if @company.nil?
    end

    render json: @response
  end


  # POST /advert/company/employee/block
  # company_id, employee_id, block_reason
  def block
    # Block employee in company
    if @company and @employee and current_user.owner(@company)
      if not @employee.block(params)
        @response[:success] = false
        @employee.errors.full_messages.map { |e| @response[:errors] << e}
      else
        @response[:employee] = @employee
      end
    else
      @response[:success] = false
      @response[:errors] << 'Вы не владелец компании' if not current_user.is_advert_admin?
      @response[:errors] << 'Сотрудник не найден' if @employee.nil?
      @response[:errors] << 'Компания не найдена' if @company.nil?
    end

    render json: @response
  end

  def element
    :employee
  end
end
