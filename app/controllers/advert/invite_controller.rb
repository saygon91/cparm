class Advert::InviteController < AdvertBaseController


  # POST /advert/company/invite/new
  # company_id email
  def new

    invite = Invite.find_by_email(params[:email])

    if @company and params.has_key? :email and invite.nil?
      @response[:invite] = current_user.invite_webmaster(@company, params[:email])
      if not @response[:invite].nil?
        @response[:alerts] << 'Вебмастеру выслано приглашение'
      else
        @response[:errors] << 'Не удалось выслать приглашение. Внутренняя ошибка сервера'
        @response[:success] = false
      end
    else
      if not Invite.find_by_email(params[:email]).nil?
        @response[:alerts] << 'Пользователю уже выслано приглашение'
        @response[:invite] = invite
      end
    end

    render json: @response
  end

  # POST /advert/company/invite/delete
  # company_id, invite_id
  def delete
  end

end
