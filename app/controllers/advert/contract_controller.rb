class Advert::ContractController < AdvertBaseController

  def all
    if @company
      @response[:contracts] = []
      @company.contracts.where(accepted: true, denied: false).each do |contract|
        @response[:contracts] << ContractSerializer.new(contract, root: false)
      end
    end

    render json: @response
  end
  # POST /advert/company/contract/accept
  def accept
    if @company and @contract and @contract.update_attributes(denied: false, accepted: true)
      @response[:success] = true
      @response[:contract] = ContractSerializer.new(@contract, root: false)
      @response[:alerts] << 'Контракт успешно подключен'
    else
      logger.info "#{@company}, #{@contract}"
      @response[:errors] << 'Не удалось принять контракт. Внутренняя ошибка сервера'
      @response[:success] = false
    end

    render json: @response
  end


  # POST /advert/company/contract/denied
  def denied
    if @company and @contract and @contract.update_attributes(denied: true, accepted: true)
      @response[:success] = true
      @response[:alerts] << 'Контракт успешно отключен'
      @response[:contract] = ContractSerializer.new(@contract, root: false)
    else
      @response[:errors] << 'Не удалось отклонить контракт. Внутренняя ошибка сервера'
      @response[:success] = false
    end

    render json: @response
  end

end
