class Advert::ApiController < AdvertBaseController

  # /advert/company/api/new_token
  # company_id, token[key, name]
  def new_token
    if @company
      @response[:token] = @company.tokens.create(params[:token])
    end

    render json: @response
  end

end
