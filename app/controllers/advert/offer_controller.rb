class Advert::OfferController < AdvertBaseController

  # /advert/company/offer/price
  # Offer price
  def price
    @offer = Offer.find_by_id(params[:id])
    if @offer
      @response = {}
      @response[:price] = @offer.product.price if @offer.product
    end

    render json: @response, callback: params['callback'],  content_type: Mime::JS
  end

  # /advert/company/offer/all.json
  # All company offers in json array
  def all
    if @company
      @response[:offers] = []
      @company.offers.each do |offer|
        @response[:offers] << OfferSerializer.new(offer, root: false)
      end
    end

    render json: @response
  end

  # /advert/company/offer/delete
  # Delete offer by offer_id
  def delete
    if @company
      @offer = @company.offers.find_by_id(params[:offer_id])
      @response[:success] = false #if @offer and not @offer.destroy
      @response[:errors] << 'Удаление оффера запрещено!'
    end

    render json: @response
  end

  # /advert/company/offer/update
  # update offer offer_id with params[:offer]
  def update
    if @company and @offer
      @response[:offer] = @offer if @offer and @offer.update_offer(params[:offer])
    end

    render json: @response
  end

  # /advert/company/offer/new
  # Create new offer with params[:offer]
  def new
    # Params : {"company_id"=>1,
    # "offer"=>
    # {"name"=>"qegqeg",
    # "description"=>"eqggeq",
    # "product_id"=>1,
    # "lp_add"=>["https://land1.ru", "https://land12.ru"], "tp"=>["https://transit1.ru"], "cities"=>["Moscow"]}}
    if @company and request.post?
      @offer = @company.offers.create(params[:offer])
      if @offer
        params[:offer][:landing_pages].map {|lp|   @offer.landing_pages.create(url: lp)} if params[:offer][:landing_pages]
        params[:offer][:geos].map   {|geos| @offer.geos.create(name: geos) }      if params[:offer][:geos]
        @response[:offer] = OfferSerializer.new(@offer, root: false)

      else
        @offer.errors.full_messages.each do |message|
          @response[:errors] << message
        end

      end
    end

    render json: @response
  end
end
