
class RegistrationsController < Devise::RegistrationsController

  def user_url(resource)
    if resource
      resource.is_advert? ?     '/advert/'    : root_path
      resource.is_webmaster? ?  '/webmaster/' : root_path
    else
      "/"
    end
  end

  def create
    super do |resource|
      user = params[:user]

      resource.update_attributes(first_name: user[:first_name])
      resource.update_attributes(last_name: user[:last_name])
      resource.update_attributes(skype: user[:skype])
      resource.update_attributes(icq: user[:icq])

      resource.save



      user_type = WEBMASTER if not [ADVERT,WEBMASTER].include? user[:user_type]

      if user[:user_type].eql? '1'
        logger.info "ADVERT ----- #{resource.as_json} #{user[:user_type].class}"
        resource.add_role :advert
        # company = resource.companies.create(name: params[:user][:company_name])
        #
        # #employee = company.employees.find_or_create_by(user: resource)
        #
        # resource.add_role :advert, company              # роль Advert
        # resource.add_role :advert_admin, company        # Админ компании
        # resource.add_role :generate_barcodes, company   # Доступ на генерацию barcode
      end


      if user[:user_type].eql? '2'
        logger.info "webmaster ----- #{resource.as_json} #{user[:user_type].class}"
        resource.add_role :webmaster
      end

      #logger.info "\n\n\n#{resource.to_json} || #{resource.errors.full_messages}\n\n\n"
    end
  end

  def after_sign_up_path_for(resource)
    case resource
      when :user, User
        resource.is_advert? ?     '/advert/'    : root_path
        resource.is_webmaster? ?  '/webmaster/' : root_path
      else
        super
    end
  end

  before_filter :update_sanitized_params, if: :devise_controller?

  def update_sanitized_params
    devise_parameter_sanitizer.for(:sign_up) {|u| u.permit(:first_name, :last_name, :user_type, :skype, :icq, :email, :password, :password_confirmation)}
  end
end
