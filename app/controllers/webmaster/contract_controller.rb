class Webmaster::ContractController < WebmasterBaseController

  # POST /webmaster/contract/generate_balances

  def generate_balances
    @contracts = []
    @response[:checks] = []

    if params.has_key? :contract_id and not params[:contract_id].nil?
      contract = current_user.contracts.find_by_id(params[:contract_id])
      @contracts << contract if not contract.nil?
    else
      @contracts = current_user.contracts
    end


    @contracts.map {|contract|
      logger.info "Parse contract #{contract.id}"
      total_amount = 0
      contract.orders.map { |order|
        logger.info "Parse order #{order.id}"

        order.balances.where(user: current_user, payed: false, company: order.company).map { |balance|
          logger.info "Parse balance #{balance.id} -> #{balance.to_json}"
          total_amount += balance.amount
        }
      }

      check = current_user.checks.new(user: current_user, webmaster_id: current_user.id,
          amount: total_amount, date_from: params[:date_from],
          date_to: params[:date_to], company: contract.company)
      @response[:checks] << check

    } if @contracts.length > 0


    render json: @response
  end

  def statistic
    contracts = []
    if not params.has_key? :contract_id or params[:contract_id].nil?
      logger.info "All contracts"
      contracts = current_user.contracts
    else
      logger.info "One contract"
      contracts << @contract
    end

    @response[:statistic] = {process: 0, canceled: 0, accepted: 0}

    logger.info "#{contracts}"
    if contracts.length > 0
      contracts.map {|contract|
        @company = contract.company
        if @company
          status_accepted  = @company.dictionaries.find_by(name: 'Подтвержден', object_type: DICTIONARY_TYPES.index('статус заказа'))
          status_send  = @company.dictionaries.find_by(name: 'Отправлен', object_type: DICTIONARY_TYPES.index('статус заказа'))
          status_payed  = @company.dictionaries.find_by(name: 'Оплачен', object_type: DICTIONARY_TYPES.index('статус заказа'))
          status_canceled  = @company.dictionaries.find_by(name: 'Отменен', object_type: DICTIONARY_TYPES.index('статус заказа'))
          status_new = @company.dictionaries.find_by(name: 'Новый', object_type: DICTIONARY_TYPES.index('статус заказа'))
          status_returned = @company.dictionaries.find_by(name: 'Возврат', object_type: DICTIONARY_TYPES.index('статус заказа'))
          status_defect = @company.dictionaries.find_by(name: 'Брак', object_type: DICTIONARY_TYPES.index('статус заказа'))
          status_other_region = @company.dictionaries.find_by(name: 'Другой Регион', object_type: DICTIONARY_TYPES.index('статус заказа'))
          status_recall = @company.dictionaries.find_by(name: 'Перезвонить', object_type: DICTIONARY_TYPES.index('статус заказа'))
          status_cant_call = @company.dictionaries.find_by(name: 'Недозвонился', object_type: DICTIONARY_TYPES.index('статус заказа'))


          logger.info "accepted = #{status_accepted.id}, sended = #{status_send.id}, payed = #{status_payed.id}, canceled = #{status_canceled.id}, new : #{status_new.id}"




          result_logs ||= []
          contract.orders.map { |order|
            if order.created_at > params[:date_from] and order.created_at < params[:date_to]
              if order.logs.where(new_value: status_accepted.id).count > 0
                @response[:statistic][:accepted] += 1
              else
                case order.status_id
                  when status_new.id
                    logger.info "Order ##{order.id} in process"
                    @response[:statistic][:process] += 1
                  when status_recall.id
                    logger.info "Order ##{order.id} in process"
                    @response[:statistic][:process] += 1
                  when status_cant_call.id
                    logger.info "Order ##{order.id} in process"
                    @response[:statistic][:process] += 1
                  when status_other_region.id
                    logger.info "Order ##{order.id} in canceled"
                    @response[:statistic][:canceled] += 1
                  when status_defect.id
                    logger.info "Order ##{order.id} in canceled"
                    @response[:statistic][:canceled] += 1
                  when status_canceled.id
                    logger.info "Order ##{order.id} in canceled"
                    @response[:statistic][:canceled] += 1
                  else
                    logger.info "Else Order with status #{order.status}"

                end
              end
            end


          }
          #result_logs = @company.logs.where("created_at > ? and created_at < ? and new_value = ?", params[:date_from], params[:date_to])

          result_logs.map { |log|

            logger.info "Parsing order with status_id #{log.order.status_id}"
            case log.order.status_id
              when status_new.id
              when status_recall.id
              when status_cant_call.id
                @response[:statistic][:process] += 1
              when status_accepted.id
                @response[:statistic][:accepted] += 1
              when status_other_region.id
              when status_defect.id
              when status_canceled.id
                @response[:statistic][:canceled] += 1
              else
                logger.info "Else #{log.order.status_id} with class #{log.order.status_id.class}"

            end
          }
          @response[:success] = true
        else
          @response[:success] = false
          @response[:errors] << 'Не найдена компания'
        end
      }


    else
      @response[:success] = false
      @response[:errors] << 'Не найден контракт'
    end
    render json: @response
  end

end
