class Webmaster::IndexController < WebmasterBaseController

  def serialization
    @response[:offers] = []
    Offer.all.each do |offer|
      # of =
      # of[:connected] = Contract.where(offer: offer, webmaster_id: current_user.id, accepted: true, denied: false).count > 0 ? true : false
      @response[:offers] << OfferSerializer.new(offer, root: false, scope: serialization_scope)
    end

    current_user.contracts.map {|c| @response[:contracts] << ContractSerializer.new(c, root: false)}

    # @response[:balances] ||= []
    # current_user.balances.map {|balance|
    #   @response[:balances] << BalanceSerializer.new(balance, root: false)
    # }

    @response[:user] = UserSerializer.new(current_user, root: false)

    render json: @response
  end

  def index

  end
end