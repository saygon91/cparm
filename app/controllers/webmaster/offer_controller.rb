class Webmaster::OfferController < WebmasterBaseController

  # POST /webmaster/offer/connect
  # Connect offer
  def connect
    if @offer
      logger.info "User #{current_user} wanna connect offer #{@offer.name}"

      # TODO пофиксить выставление accepted поля
      #accept = @offer.private ? true : false
      contract = current_user.contracts.where(offer: @offer, accepted: @offer.private, company_id: @offer.company_id).first_or_initialize
      if contract.valid? and contract.new_record?
        contract.save!
        @response[:contract] = ContractSerializer.new(contract, root: false)
        @response[:alerts] << 'Оффер подключен'
        @response[:alerts] << 'Ввиду того, что оффер приватный необходимо ждать подтверждения адверта! К вам на почту придет сообщение' if @offer.private
      else
        @response[:success] = false
        @response[:alerts] << 'Оффер уже подключен' if not contract.new_record?
        @response[:errors] += contract.errors.full_messages
      end

    end

    render json: @response
  end




end
