class LandingController < ApplicationController

  def new_offer
    @offers = Offer.all
    @contracts = Contract.all
    result = 'success'
    if request.post?
      @contract = Contract.find_by_id(params[:cid])

      if @contract
        logger.info "New offer from landing : #{params}"

        @company = @contract.company

        customer = @company.customers.where(phone: params[:phone]).first_or_create

        customer.first_name = params[:first_name]
        customer.last_name = params[:last_name]
        customer.phone = params[:phone]
        customer.save


        order = Order.new
        order.customer = customer
        order.landing_page = request.referer
        order.offer = @contract.offer
        order.contract_id = @contract.id
        order.company_id = @contract.company_id
        order.save

        order.order_products.create(company_id: @contract.company_id, product: @contract.offer.product, count: 1, discount: 0, price: @contract.offer.product.price)

        order.recalc_total_amount_after_create
        order.order_after_create

        # Send to baribarda
        order.send_to_baribarda
      else
        result = 'error'
      end

      render result
    end

  end
end
