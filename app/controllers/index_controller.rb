class IndexController < ActionController::Base
  before_action :authenticate_user!
  before_action {
    redirect_path ||= '/advert/'
    redirect_path = '/advert/'    if current_user.roles_name.include? 'advert' or current_user.roles_name.include? 'advert_admin'
    redirect_path = '/webmaster/' if current_user.roles_name.include? 'webmaster'
    redirect_to redirect_path
  }
  def index

  end

end