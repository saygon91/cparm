class WebmasterBaseController < ApplicationController
  layout 'webmaster'

  before_action {
    authenticate_user!
    redirect_to '/access/denied' if not current_user.is_webmaster?


    @response = build_response


    if current_user
      @offer = Offer.find_by_id(params[:offer_id]) if params.has_key? :offer_id
      @contract = current_user.contracts.find_by_id(params[:contract_id])
      @response[:checks] = current_user.checks
    end
  }

end