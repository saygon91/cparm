class AdvertBaseController < ApplicationController
  layout 'advert'

  before_action :authenticate_user!, except: [:price]
  before_action {
    @response = build_response

    @company = Company.find_by_id(params[:company_id]) if params.has_key? :company_id
    if @company

      if not current_user.related_to(@company)
        @response[:errors] << "У вас нет прав для работы в этой компании - #{params[:company_id]}"
        render json: @response
      end

      @order = @company.orders.find_by_id(params[:order_id])              if params.has_key? :order_id
      @orders = @company.orders.find(params[:order_ids])                  if params.has_key? :order_ids and not params[:order_ids].nil?
      @offer = @company.offers.find_by_id(params[:offer_id])              if params.has_key? :offer_id
      @product = @company.products.find_by_id(params[:product_id])        if params.has_key? :product_id
      @employee = @company.employees.find_by_id(params[:employee_id])     if params.has_key? :employee_id
      @warehouse = @company.warehouses.find_by_id(params[:warehouse_id])  if params.has_key? :warehouse_id
      @contract = @company.contracts.find_by_id(params[:contract_id])     if params.has_key? :contract_id
      @customer = Customer.find_by_id(params[:customer_id])               if params.has_key? :customer_id
      @webmaster = User.find_by_id(params[:webmaster_id])                 if params.has_key? :webmaster_id

      @response[:errors] << "Не найден заказ с ID #{params[:order_id]}"       if params.has_key? :order_id and @order.nil?
      @response[:errors] << "Не найден оффер с ID #{params[:offer_id]}"       if params.has_key? :offer_id and @offer.nil?
      @response[:errors] << "Не найден товар с ID #{params[:product_id]}"     if params.has_key? :product_id and @product.nil?
      @response[:errors] << "Не найден склад с ID #{params[:warehouse_id]}"   if params.has_key? :warehouse_id and @warehouse.nil?
      @response[:errors] << "Не найден контракт c ID #{params[:contract_id]}" if params.has_key? :contract_id and @contract.nil?

      @status_accepted = @company.dictionaries.find_by_name('Подтвержден')
      @status_need_send = @company.dictionaries.find_by_name('Срочно отправить')


    end

  }




  after_action {
    # logger.info "\nРезультат обработки запроса"
    # logger.info "Пользователь : #{current_user.email}" if current_user
    # logger.info "#{request.request_method} on URL : #{request.original_fullpath}"
    # logger.info "Params : #{request.request_parameters}" if request.post?
    # logger.info "Params : #{request.query_parameters}" if request.get?
    # logger.info "Response : #{@response}\n\n"

    # @response[:companies].map { |company_data|
    #   Company.where(company_data.id).map { |company|
    #     logger.info "Filter result orders for company #{company.name} as user - call operator/manager"
    #
    #     if current_user and not current_user.has_role? :advert_admin, company
    #
    #
    #       company_data['orders'].each { |order|
    #
    #         # Remove order if user is call manager/operator and order.status is Подтвержден
    #         if current_user.has_role? :call_operator, company or current_user.has_role? :call_manager, company
    #           if order.status_id == company.dictionaries.find_by_name('Подтвержден')
    #             logger.info "Order #{order.id} removed for log operator/manager from result list becouse has status_id Подтвержден"
    #             company_data[:orders].delete(order)
    #           end
    #         end
    #
    #         if @current_user.has_role? :log_operator, company or @current_user.has_role? :log_operator, company
    #           if order.status_id != company.dictionaries.find_by_name('Подтвержден')
    #             logger.info "Order #{order.id} removed for call operator/manager from result list becouse has status_id Новый"
    #             company_data[:orders].delete(order)
    #           end
    #         end
    #       }
    #
    #     end
    #
    #   }
    # }


  }


  def prepare_params(params)
    #
    # Удаление ненужные полей объекта пришедших с фронта
    result = {}
    params.each do |key, value|
      result[key.to_sym] = value if not ['id', 'created_at' ,'updated_at', '$$hashKey'].include? key
    end
    result
  end


end