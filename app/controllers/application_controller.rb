class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  #protect_from_forgery with: :exception
  serialization_scope :current_user
  before_action {
    ActiveRecord::Base.default_timezone = :local
  }

  def current_ability
    @current_user = current_user
  end

  def build_response
    @response = {
      websocket_url: Rails.configuration.web_socket,
      success: true,
      errors: [],
      alerts: [],
      messages: [],
      currency: CURRENCY,
#      statuses: [],
      contracts: [],
      dictionary_types: DICTIONARY_TYPES,
      logs: [],
      orders: []
    }


    #OrderStatus.all.map {|os| @response[:statuses] << OrderStatusSerializer.new(os, root: false)}

    return @response
  end
end
