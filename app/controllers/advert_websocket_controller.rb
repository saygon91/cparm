class AdvertWebsocketController < AdvertWebsocketBaseController
  before_action {
    @response = {
        success: false,
        errors: [],
        alerts: []
    }
  }

  def get_channel
    channel ||= Channel.new
    if current_user and current_user.company_ids.include? message[:company_id]
      channel = Channel.find_by_name(message[:channel_name], company_id: message[:company_id])
      channel = Channel.create(name: message[:channel_name]) if channel.nil?
    end
    trigger_success channel
  end

  def chat_history
    if current_user
      @response[:messages] = current_user.chat_messages(message[:company_id], message[:message_type])
      logger.info "Chat history for #{current_user.email} [#{current_user.id}]"

      @response[:success] = true
    end

    trigger_success @response
  end

  def send_message
    logger.info "Chat message (type: #{message[:message_type]}) #{message[:message]} from #{current_user.email}  [#{current_user.id}] to #{User.find_by_id(message[:dst]).email} "
    if current_user
      case message[:message_type]
        when MESSAGE_TYPE.index('channel')
          channel = Channel.find_by_id(message[:dst], company_id: message[:company_id])
          if channel
            chat_message = channel.messages.create(src: current_user.id, message_type: message[:message_type], message: message[:message], dst: message[:dst], company_id: message[:company_id])
          end
        when MESSAGE_TYPE.index('private')
          dst_user = User.find_by_id(message[:dst])
          if dst_user
            chat_message = Message.create(src: current_user.id, message_type: message[:message_type], message: message[:message], dst: message[:dst], company_id: message[:company_id])
          end
      end

      if chat_message and chat_message.valid?
        @response[:message] = MessageSerializer.new(chat_message, root: false)
      end
    end

    trigger_success @response
  end

  def message_readed
    if current_user
      Message.find_by(company_id: message[:company_id], id: message[:message_id]) do |message|
        if message.update_attibutes(read: true, readed_at: DateTime.now)
          @response[:message] = MessageSerializer.new(message, root: false)
        end
      end
    end

    trigger_success @response
  end

  def init
    logger.info "Advert websocket base controller #{current_user} -> #{message} -> #{session.as_json}"
    trigger_success "Message"
  end
end