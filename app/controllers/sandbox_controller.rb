class SandboxController < ApplicationController
  def s
    @companies = []
    if current_user

      current_user.companies.each do |company|
        @companies << company.active_model_serializer.new(company, root: false)
      end
    end
    render json: @companies
  end
end
