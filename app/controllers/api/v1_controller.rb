class Api::V1Controller < ApplicationController
  layout 'blank'
  # Integration with leadvertex

  def lead
    # first_name=%D1%82%D0%B5%D1%81%D1%82%D0%BE%D0%B2%D1%8B%D0%B9%20%D0%B7%D0%B0%D0%BA%D0%B0%D0%B7%20%D0%BA%D0%B7&&
    # phone=6540654065&
    # lead_key=1159000543473&
    # token=d196e5f6fa1&
    # offer=fitodepilation
    @response = {}
    token = Token.where(key: params[:token]).first

    logger.info "Token finded for #{token.name}"
    if token.company
      logger.info "API request for #{token.company.name}"

      company = token.company


      offer = company.offers.where(name: params[:offer]).first
      puts "#{offer.as_json} | #{offer.company.as_json} | #{offer.company.orders}"
      logger.info "Offer finded #{offer.id} - #{offer.name}"
      customer = Customer.create(order: @order,first_name: params[:first_name], phone: params[:phone])
      @order = offer.company.orders.create(lead_key: params[:lead_key], offer: offer, contract_id: -1, customer: customer, landing_page: params[:offer])


      @order.order_products.create(company_id: company.id, product: offer.product, count: 1, discount: 0, price: offer.product.price)


      offer.save
      @response[:order_id] = @order.id

      logger.info "MonsterLeads order #{params}"
    end

    render json: @response

  end


  def update_order
    @api_response = {success: false, errors: []}
    uid = '8346190439'
    key = '70ab5806c85a'


    logger.info "Update order params #{params}"

    if params[:uid].eql? uid and params[:key].eql? key


      order = Order.find_by_id(params[:order_id])
      if order
        company = order.company

        logger.info "Order #{order.as_json}"

        status = company.dictionaries.find_by(object_type: DICTIONARY_TYPES.index('статус заказа'), name: params[:status])
        if status
          logger.info "Status #{status.as_json}"
          if order.update_attributes(status_id: status.id)
            @api_response[:success] = true
            @api_response[:errors] << "Order updated with status #{params[:status]}"
          else
            logger.info "Cant update order with errors #{order.errors.full_messages}"
          end
        else
          @api_response[:success] = false
          @api_response[:errors] << "Cant find status #{params[:status]}"
        end


      else
        @api_response[:success] = false
        @api_response[:errors] << "Cant find order with id  #{params[:order_id]}"
      end


    else
      @api_response[:success] = false
      @api_response[:errors] << "Error uid or key"
    end

    render json: @api_response

  end


end
