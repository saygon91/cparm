ActiveAdmin.register Company do
  menu label: "Компании"

  index do
    column "Название", :name
    column "Владелец", :user
    column "Заказы" do |company|
      company.orders.count
    end
    column "Офферы" do |company|
      company.offers.count
    end
    actions
  end

end
