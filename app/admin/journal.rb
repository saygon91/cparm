ActiveAdmin.register Log do
  menu label: "Журнал"

  index do
    column "ID", :id
    column "Дата", :created_at
    column "Запись" do |log|
      log.as_string_without_date
    end
  end

end