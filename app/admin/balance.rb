ActiveAdmin.register Balance do
  menu label: "Баланс"

  index do
    column "ID", :id
    column "Вебмастер", :user
    column "Заказ", :order_id
    column "Сумма", :amount
    column "Дата создания", :created_at
    actions
  end

end
