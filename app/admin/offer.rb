ActiveAdmin.register Offer do
  menu label: "Офферы"

  index do
    column "Название", :name
    column "Компания", :company
    column "Продукт" do |offer|
      if offer.product
        offer.product.name
      else
        'неизвестно'
      end
    end
    actions
  end

end
