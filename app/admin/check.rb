ActiveAdmin.register Check do
  menu label: "Чеки"

  index do
    column "ID", :id
    column "Вебмастер", :webmaster
    column "Дата с", :date_from
    column "Дата по", :date_to
    column "Сумма", :amount
    column "Дата создания", :created_at
    actions
  end

end
