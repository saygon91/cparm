ActiveAdmin.register Order do
  menu label: "Заказы"

  index do
    column "Номер заказа", :id
    column "Общая цена", :total_amount
    column "Цена за доставку", :delivery_amount
    column "LandingPage", :landing_page
    column "Дата создания", :created_at
    column "Дата доставки", :delivery_date
    column "Статус заказа" do |order|
      order.status
    end
    column "Статус доставки" do |order|
      order.logistic_status
    end
    actions
  end

end
