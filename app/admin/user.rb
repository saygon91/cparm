ActiveAdmin.register User do
  menu label: "Пользователи"

  index do
    column 'Имя', :price, :sortable => :price do |user|
      div :class => "price" do
        "#{user.first_name} #{user.last_name}"
      end
    end

    column "Email", :email

    column "Компании" do |user|
      names = ""
      user.related_companies.map { |c| names += c.name }
      names
    end

    column 'Последний вход', :last_sign_in
    column 'ICQ', :icq
    column 'Skype', :skype
    column 'Баланс', :balance
    actions
  end



end
