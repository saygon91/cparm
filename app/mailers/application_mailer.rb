class ApplicationMailer < ActionMailer::Base
  default from: "cparm.notifications@yandex.ru"
  layout 'mailer'
end
