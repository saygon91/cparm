class UserMailer < ApplicationMailer

  def invite_email(user, company, password)
    @user = user
    @company = company
    @password = password
    @url  = 'http://cparm.0day.kz/'
    mail(to: @user.email, subject: "Вы приглашены в компанию #{company.name} на сайте http://cparm.0day.kz/")
  end

end
