class Employee < ActiveRecord::Base
  belongs_to :user
  belongs_to :company

  attr_accessible :company_id, :user_id, :block_reason, :user, :blocked, :blocked_at, :unblocked_at ,:unblock_reason


  def add_role(role)
    user.add_role role.to_sym, company if SYSTEM_ROLES.include? role.to_sym and not user.has_role? role.to_sym, company
  end

  def block(params)
    logger.info "Block user #{user.email} with reason #{params[:block_reason]}"
    update_attributes(blocked: true, block_reason: params[:block_reason], blocked_at: DateTime.now)
  end

  def unblock(params)
    logger.info "Unblock user #{user.email}"
    update_attributes(blocked: false, unblocked_at: DateTime.now, unblock_reason: params[:unblock_reason])
  end
end
