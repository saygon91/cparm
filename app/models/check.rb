class Check < ActiveRecord::Base
  belongs_to :user
  belongs_to :company
  belongs_to :webmaster, :class_name => 'User', :validate => true
  attr_accessible :user, :user_id,  :webmaster_id, :amount, :comment, :date_from, :date_to, :company
end
