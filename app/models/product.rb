class Product < ActiveRecord::Base
  belongs_to :company
  attr_accessible :name, :weight, :count, :company, :description, :company_id,
      :product, :product_id, :price, :lat_name, :delivery_amount, :sale_two, :sale_three

end
