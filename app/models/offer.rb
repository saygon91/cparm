class Offer < ActiveRecord::Base
  belongs_to :company
  belongs_to :product
  has_many :geos
  has_many :news
  has_one :offer_type
  has_many :landing_pages
  has_many :transit_pages

  attr_accessible :name, :description, :company, :product, :product_id, :company_id,:private, :currency, :price

  def update_offer(offer_params)
    update_attributes(offer_params)
  end
end
