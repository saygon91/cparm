class LandingPage < ActiveRecord::Base
  belongs_to :offer
  attr_accessible :url, :name, :offer_id, :offer
end
