class Warehouse < ActiveRecord::Base
  belongs_to :company
  attr_accessible :name, :user, :location
  has_many :warehouse_products
  has_many :arrivals
  has_many :outgos
end
