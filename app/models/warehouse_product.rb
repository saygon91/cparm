class WarehouseProduct < ActiveRecord::Base
  belongs_to :product
  belongs_to :warehouse

  attr_accessible :product, :warehouse, :count
end
