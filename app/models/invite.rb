class Invite < ActiveRecord::Base
  belongs_to :company
  belongs_to :user
  attr_accessible :email, :key, :user, :company, :joined

  after_destroy { |record| User.find_by_email(record.email).destroy! if record.joined == false}
end
