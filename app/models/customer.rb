class Customer < ActiveRecord::Base
  belongs_to :company
  has_many :orders
  attr_accessible :name, :first_name, :last_name, :phone, :address, :zip, :comment, :city,  :email, :country, :province, :address, :role, :company_id, :company

  def update_customer(params, user, company)
    update_attributes(params)
  end

  def log_changes(changes, user, company, order)
    changes.keys.map {|customer_field|
      logger.info "---- RAW LOG CUSTOMER #{customer_field} => #{changes[customer_field]}"

      action = LOG_ACTIONS.index('изменил')
      old = changes[customer_field][0]
      new = changes[customer_field][1]

      field_id = 0
      case customer_field
        # Фио
        when 'first_name' then field_id = LOG_ORDER_FIELDS.index('имя')
        when 'last_name'  then field_id = LOG_ORDER_FIELDS.index('фамилия')
        when 'city'       then field_id = LOG_ORDER_FIELDS.index('город')
        when 'zip'        then field_id = LOG_ORDER_FIELDS.index('индекс')
        when 'comment'    then field_id = LOG_ORDER_FIELDS.index('комментарий')
        when 'phone'      then field_id = LOG_ORDER_FIELDS.index('телефон')
        when 'province'   then field_id = LOG_ORDER_FIELDS.index('район')
        when 'address'    then field_id = LOG_ORDER_FIELDS.index('адрес')
        when 'country'    then field_id = LOG_ORDER_FIELDS.index('страна')
      end

      Log.create(order_id: order.id, user: user, field_str: customer_field,old_value: old, new_value: new, company: company,action_id: action, field_id: field_id) if not ['created_at', 'updated_at'].include? customer_field



    }
  end
  def name
    "#{first_name} #{last_name}"
  end
end
