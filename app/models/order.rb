class Order < ActiveRecord::Base
  # acts_as_xlsx
  require 'barby'
  require 'barby/barcode/code_128'
  require 'barby/outputter/png_outputter'

  #extend ::WebsocketSender

  belongs_to :offer
  belongs_to :customer
  belongs_to :company
  belongs_to :contract

  attr_accessible :offer, :customer, :offer_id, :company_id, :contract,
      :contract_id, :status_id,  :logistic_status_id, :delivery_type_id,
      :total_amount, :zip, :courier, :delivery, :delivery_amount, :landing_page,
      :barcode, :delivery_date, :recall_date, :lead_key, :total_amount_discount, :total_amount_percent

  has_many :order_products, dependent: :destroy
  has_one :order_status
  has_many :logs, dependent: :destroy
  has_many :balances, dependent: :destroy

  after_commit {
    #set_logistic_status_id_if_status_id_is_accepted
    if status_id == Dictionary.get_status_id_by_name('статус заказа', 'Подтвержден', company_id)
      send_money_to_webmaster
    end

    send_to_websocket_after_update
  }

  after_create { order_after_create }

  def send_to_baribarda
  #   http://baribarda.com/api/send_order.php – отправка данных о заявках на товар в систему Baribarda.com формат передачи JSON.
  #       Данные передаются методом POST в переменную POST[‘data’]
  #
  #   Пример отправки данных (РНР):
  #       <?php
  #   $data = array(
  #       "phone" => "77017777777", 	//номер телефона заказчика товара
  #   "price" => "6400", 			//стоимость товара
  #   "order_id" => "123321",		// id заявки внешний для синхронизации
  #   "name" => "Gena Ivanov",	// ФИО заказчика
  #   "country" => "kz",			// Гео локация
  #   "addr" => "г. Караганда",	// адрес
  #   "offer" => "pled_s_rukavamy", // наименование товара
  #   "secret" => "12344321"		// секретный ключ АПИ
  # );
  #   $uid = '11223344';  				// Ваш логин в системе
  #
  #   $data = json_encode($data);
  #   $hash_str = strlen($data) . md5($uid);
  #   $hash = hash('sha256', $hash_str);
  #   $ch = curl_init();
  #   curl_setopt($ch, CURLOPT_URL, 'http://baribarda.com:8888/api/send_order.php?uid=`.$uid.`&hash='.$hash);
  #   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  #   curl_setopt($ch, CURLOPT_POST, true);
  #   curl_setopt($ch, CURLOPT_POSTFIELDS, array('data'=>$data));
  #   curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
  #   curl_setopt($ch, CURLOPT_HTTPHEADER, array("Pragma: no-cache"));
  #   $result['EXE'] = curl_exec($ch);
  #   $result['INF'] = curl_getinfo($ch);
  #   $result['ERR'] = curl_error($ch);

    data = {  phone: customer.phone, price: offer.product.price, order_id: id,
        name: customer.name, country: 'kz', addr: '', offer: offer.name,
        secret: SECRET_KEY }
    #, dictionaries: company.dictionaries.where(object_type: DICTIONARY_TYPES.index('статус заказа'))}


    data_json = data.to_json
    logger.info "Data json class #{data_json.class} | str #{data_json.to_s}"

    sha256 = Digest::SHA256.new
    md5 = Digest::MD5.new
    clnt = HTTPClient.new('http://127.0.0.1:8080')
    #clnt = HTTPClient.new()


    md5 << UID
    logger.info "UID (#{UID}) in md5 #{md5.hexdigest}"

    hash = data_json.length.to_s + md5.hexdigest.to_s
    logger.info "Data #{data_json} with length #{data_json.length} and md5(data) ==  #{hash}"
    url = "http://baribarda.com/api/send_orderTest.php?uid=#{UID}&hash=#{Digest::SHA256.hexdigest(hash)}"

    request_data = {data: data_json.as_json}
    res = clnt.post(url, data_json)

    logger.info "Result #{res.as_json}"



    data
  end

  def barcode_output
    Base64.encode64(Barby::PngOutputter.new(Barby::Code128B.new(barcode)).to_png)
  end

  def status
    ls = Dictionary.find_by_id(status_id)
    ls.nil? ? 'неизвестно' : ls.name
  end
  def logistic_status
    ls = Dictionary.find_by_id(logistic_status_id)
    ls.nil? ? 'неизвестно' : ls.name
  end
  def order_after_create
    set_status_id_new_after_create
    recalc_total_amount_after_create


    # Send to websocket users
    send_to_websocket_after_create
  end

  def set_status_id_new_after_create
    # После создания заказа в системе устанавливает статус заказа 'Новый'
    log = logs.create(user_id: User.system_user.id, company_id: company.id, field_id: LOG_ORDER_FIELDS.index('заказ'), action_id: LOG_ACTIONS.index('создал'))

    status = Dictionary.where(object_type: DICTIONARY_TYPES.index('статус заказа'), name: 'Новый', company_id: company_id).first
    update_attributes(status_id: status.id) if status
  end

  def set_logistic_status_id_if_status_id_is_accepted
    # Если заказу присвоен статус 'Подтвержден', то статус посылки необходимо установить на 'Срочно отправить'
    set_logistic_status('Срочно отправить')
  end

  def delete_order
    order_products.destroy_all
    logs.destroy_all
    balances.destroy_all
  end

  def generate_barcode(prefix = nil, current_user)
    # Генерация штрих кода для заказа
    barcode = company.generate_next_barcode(prefix)
    if barcode
      update_attributes(barcode: "#{barcode}KZ")

      log_changes(previous_changes, current_user)
    else
      logger.debug "Error! Cant assign barcode #{barcode}"
    end
    OrderSerializer.new(self, root: false)
  end

  def send_money_to_webmaster
    if contract
      # If order maked from webmaster traffic
      contract.webmaster.balances.where(order_id: id, amount: contract.offer.price, company_id: company_id).first_or_create do |balance|
        logger.info "Send money to webmaster"
        contract.webmaster.balance += balance.amount
        contract.webmaster.save
        balance.save!
      end
    end

  end

  def send_to_websocket_after_commit

    WebsocketRails.users.map {|ws_user|
      if ws_user.user.related_companies_ids.include? company_id
        logger.info "sending order to user #{ws_user.user.email} after commit"
        ws_user.send_message('advert.update_object', {company_id: company_id, object_type: self.class.name.downcase, object: "#{self.class.name}Serializer".constantize.new(self, root: false)})
      else
        logger.info "Not sending order to user #{ws_user.user.email} after commit"
      end
    }
  end

  def send_to_websocket_after_update
    WebsocketRails.users.map {|ws_user|
      if ws_user.user.related_companies_ids.include? company_id
        ws_user.send_message('advert.update_object', {company_id: company_id, object_type: self.class.name.downcase, object: "#{self.class.name}Serializer".constantize.new(self, root: false)})
      end
    }
  end

  def send_to_websocket_after_create
    WebsocketRails.users.map {|ws_user|
      if ws_user.user.related_companies_ids.include? company_id
        company = Company.find_by_id(company_id)
        if company and ws_user.user.has_role? :call_manager, company or ws_user.user.has_role? :call_operator, company
          logger.info "Send object to #{ws_user.user.email}"
          ws_user.send_message('advert.new_object', {company_id: company_id, object_type: self.class.name.downcase, object: "#{self.class.name}Serializer".constantize.new(self, root: false)})
        end
      else
        logger.info "User #{ws_user.user.email} not related to #{company_id}"
      end
    }
  end

  def self.registry_to_xls(ids)
    orders = find(ids)
    xls_file = Axlsx::Package.new
    xls_file.workbook.add_worksheet(name: 'Реестр') do |sheet|
      sheet.add_row ["Направление	", "1"]
      sheet.add_row ["Вид РПО	", "3"]
      sheet.add_row ["Категория РПО	", "4"]
      sheet.add_row ["Отправитель	ИП ", "Кумаров Д. Б"]
      sheet.add_row ["Регион назначения	", "1"]
      sheet.add_row ["Индекс ОПС места приема	", "010000"]
      sheet.add_row ["Всего РПО	", orders.count]

      sheet.add_row ['№ п п', 'Ф.И.О. Адресата', 'Индекс ОПС места назн.', 'Адрес места назначения', 'ШПИ', 'Вес (кг.)',
          'Сумма объявленной ценности', 'Сумма наложенного платежа', 'Особые отметки']

      orders.map {|order|

        new_row = []
        new_row << order.id
        if order.customer
          new_row << order.customer.name
          new_row << order.customer.zip
          new_row << order.customer.address
        else
          new_row << 'no_data'
          new_row << 'no_data'
          new_row << 'no_data'

        end

        products_names ||= ""
        product_weight = 0
        order.order_products.map { |op|
          products_names += "#{op.product.name}\n" if not op.product.nil?
          product_weight += op.product.weight if op.product and not op.product.weight.nil?
        }

        new_row << order.barcode
        new_row << product_weight

        new_row << order.total_amount
        new_row << order.total_amount
        new_row << order.customer.comment

        # columns.keys.map {|column|
        #   #logger.info "Export #{column}"
        #   case column
        #     when 'status_id'  then new_row << Dictionary.get_status_name_by_id(order.status_id)
        #
        #     when 'last_name'
        #       if order.customer
        #         new_row << order.customer.last_name
        #       else
        #         new_row << 'неизвестно'
        #       end
        #
        #     when 'fist_name'
        #       if order.customer
        #         new_row << order.customer.first_name
        #       else
        #         new_row << 'неизвестно'
        #       end
        #
        #     when 'phone'
        #       if order.customer
        #         new_row << order.customer.phone
        #       else
        #         new_row << 'неизвестно'
        #       end
        #
        #     when 'address'
        #       if order.customer
        #         new_row << order.customer.address
        #       else
        #         new_row << 'неизвестно'
        #       end
        #
        #     when 'comment'
        #       if order.customer
        #         new_row << order.customer.comment
        #       else
        #         new_row << 'неизвестно'
        #       end
        #
        #     else
        #       if order.attribute_present? column
        #         new_row << order[column.to_sym]
        #       else
        #         new_row << order.customer[column.to_sym]
        #       end
        #     #logger.info "COLUMN #{column} not in export !"
        #   end
        #
        # }

        sheet.add_row new_row


        logger.info "Export #{order.id} | #{new_row}"




      }
    end

    export_file_name = "#{Rails.application.config.xls_export_files_path}export_xls_#{rand(1000..999999)}.xlsx"
    xls_file.serialize(export_file_name)
    export_file_name
  end

  def self.export_to_xls(ids, columns)
    orders = find(ids)
    xls_file = Axlsx::Package.new
    xls_file.workbook.add_worksheet(name: 'Заказы') do |sheet|
      sheet.add_row columns.keys
      orders.map {|order|

        new_row = []

        columns.keys.map {|column|
          #logger.info "Export #{column}"
          case column
            when 'status_id'  then new_row << Dictionary.get_status_name_by_id(order.status_id)

            when 'last_name'
              if order.customer
                new_row << order.customer.last_name
              else
                new_row << 'неизвестно'
              end

            when 'fist_name'
              if order.customer
                new_row << order.customer.first_name
              else
                new_row << 'неизвестно'
              end

            when 'phone'
              if order.customer
                new_row << order.customer.phone
              else
                new_row << 'неизвестно'
              end

            when 'address'
              if order.customer
                new_row << order.customer.address
              else
                new_row << 'неизвестно'
              end

            when 'comment'
              if order.customer
                new_row << order.customer.comment
              else
                new_row << 'неизвестно'
              end

            else
              if order.attribute_present? column
                new_row << order[column.to_sym]
              else
                new_row << order.customer[column.to_sym]
              end
              #logger.info "COLUMN #{column} not in export !"
          end

        }

        sheet.add_row new_row


        #logger.info "Export #{order.id} | #{new_row}"




      }
    end

    export_file_name = "#{Rails.application.config.xls_export_files_path}export_xls_#{rand(1000..999999)}.xlsx"
    xls_file.serialize(export_file_name)
    export_file_name
  end

  def self.as_csv
    CSV.generate do |csv|
      csv << column_names
      all.each do |item|
        csv << item.attributes.values_at(*column_names)
        logger.info "#{csv}"
      end
    end
  end

  def change_delivery_amount(amount, user)
    log_changes(previous_changes, user) if update_attributes(delivery_amount: amount)
  end

  def set_logistic_status(new_status)
    new_status = company.dictionaries.find_by_name(new_status)
    if new_status
      if update_attributes(logistic_status_id: new_status.id)
        logger.info "New logistic_status_id #{new_status.id}"
      end
    else
      logger.info "#debug : #{__FILE__} : #{__method__} : Не найден статус посылки #{new_status.id}"
    end
  end

  def set_status_by_name(status_type, object_type, status_name)
    status = company.dictionaries.find_by(object_type: object_type, name: status_name)
    logger.info "#{__method__} : #{object_type}, #{status_type}, #{status_name} -> #{status}"
    self[status_type] = status.id
    save
  end

  def change_status(status_field, status, current_user)
    status_string = nil
    case status_field
      when :status_id           then status_string = 'статус заказа'
      when :logistic_status_id  then status_string = 'статус посылки'
    end

    if company.has_status_by_id?(status_string, status) and not status_string.nil?
      self[status_field] = status
      save
      log_changes(previous_changes, current_user)
    end
  end

  def closed(user_id)
    logs.create(user_id: user_id, company_id: company_id, field_id: LOG_ORDER_FIELDS.index('заказ'), action_id: LOG_ACTIONS.index('закрыл'))
  end

  def opened(user_id)
    logs.create(user_id: user_id, company_id: company_id, field_id: LOG_ORDER_FIELDS.index('заказ'), action_id: LOG_ACTIONS.index('открыл'))
  end

  def logs_string
    logs_array = []
    logs.map { |l| logs_array << l.as_string}
    logs_array
  end

  def recalc_total_amount_after_create
    new_amount = 0
    order_products.map { |order_product|
      new_amount += order_product.full_amount
    }

    if total_amount_percent != 0
      new_amount = (total_amount_percent / 100) * new_amount - total_amount_discount
    else
      new_amount = new_amount - total_amount_discount
    end

    update_attributes(total_amount: new_amount)
  end

  def log_changes(changes, user)
    changes.keys.map { |order_field|
      #logger.info "---- RAW LOG ORDER #{order_field} | #{changes[order_field]}" if Rails.env.eql? 'development'
      ## Order
      # offer_id:
      # customer_id:
      # company_id:
      # contract_id:
      # status_id:
      # logistic_status_id: 0

      # Log
      # company_id:
      # order_id:
      # action_id:
      # field_id:
      # order_product_id:
      # old_value:
      # new_value:

      action = LOG_ACTIONS.index('изменил')
      old = changes[order_field][0]
      new = changes[order_field][1]


      case order_field
        # Скидка в сумме
        when 'total_amount_discount'
          logs.create(user: user, field_str: order_field, old_value: old, new_value: new, company: company, action_id: action, field_id: LOG_ORDER_FIELDS.index('скидка в сумме') )
        # Скидка в процентах
        when 'total_amount_percent'
          logs.create(user: user, field_str: order_field, old_value: old, new_value: new, company: company, action_id: action, field_id: LOG_ORDER_FIELDS.index('скидка в процентах') )
        # Дата доставки
        when 'delivery_date'
          logs.create(user: user, field_str: order_field, old_value: old, new_value: new, company: company, action_id: action, field_id: LOG_ORDER_FIELDS.index('дата доставки') )
        # Дата перезвона
        when 'recall_date'
          logs.create(user: user, field_str: order_field, old_value: old, new_value: new, company: company, action_id: action, field_id: LOG_ORDER_FIELDS.index('дата перезвона') )
        # Штрихкод
        when 'barcode'
          logs.create(user: user, field_str: order_field, old_value: old, new_value: new, company: company, action_id: action, field_id: LOG_ORDER_FIELDS.index('штрихкод') )
        # Индекс
        when 'zip'
          logs.create(user: user, field_str: order_field,old_value: old, new_value: new, company: company,action_id: action, field_id:LOG_ORDER_FIELDS.index('индекс')) if not new.eql? -1 or not new.eql? nil
        # Курьера
        when 'courier'
          logs.create(user: user, field_str: order_field,old_value: old, new_value: new, company: company,action_id: action, field_id:LOG_ORDER_FIELDS.index('курьер')) if not new.eql? -1 or not new.eql? nil
        # Способ доставки
        when 'delivery_type_id'
          logs.create(user: user, field_str: order_field,old_value: old, new_value: new, company: company,action_id: action, field_id:LOG_ORDER_FIELDS.index('способ доставки')) if not new.eql? -1
        # Статус заказа
        when 'status_id'
          logs.create(user: user, field_str: order_field,old_value: old, new_value: new, company: company,action_id: action,field_id: LOG_ORDER_FIELDS.index('статус заказа'))
        # Статус посылки
        when 'logistic_status_id'
          logs.create(user: user,field_str: order_field,old_value: old,new_value: new,company: company,action_id: action,field_id: LOG_ORDER_FIELDS.index('статус посылки')) if not new.eql? -1
        # сумма доставки
        when 'delivery_amount'
          logs.create(user: user,field_str: order_field,old_value: old,new_value: new,company: company,action_id: action,field_id: LOG_ORDER_FIELDS.index('сумма доставки')) if not new.eql? -1
      end
    }
  end

  def update_order(order_params, current_user = nil)
    new_amount = 0
    logger.info "#{__FILE__} : #{__method__} : update order with #{order_params} | #{current_user}"
    @status_accepted = company.dictionaries.find_by_name('Подтвержден')
    @status_need_send = company.dictionaries.find_by_name('На отправку')
    @status_canceled = company.dictionaries.find_by_name('Отменен')

    # Update order products
    order_products.destroy_all
    if order_params.has_key? :order_products
      order_params[:order_products].map { |op|
        @order_product = order_products.where(product_id: op[:product_id], order_id: op[:order_id], company_id: op[:company_id]).first_or_create
        if @order_product and @order_product.update_attributes(count: op[:count], discount: op[:discount], price: op[:price])
          update_attributes(total_amount: @order_product.full_amount )
        end
      } if not order_params[:order_products].nil?
    end



    # Устанавливаем цену за доставку от цены за доставку первого товара
    if order_products.count > 0
      op = order_products.all.first
      if op
        order_params[:delivery_amount] = op.product.delivery_amount if op and op.product
        logger.info "Delivery amount from product #{op.product.name} is #{op.product.delivery_amount}"
      else
        logger.info "Error! No order products! #{order_products.count}"
      end

    else
      logger.info "Delivery amount default 1500"
      order_params[:delivery_amount] = 1500
    end

    logger.info "New delivery_amount #{order_params[:delivery_amount]}"


    # Update customer
    if customer and order_params.has_key? :customer
      customer.update_attributes(order_params[:customer].except(:id, :created_at, :updated_at))
      customer.log_changes(customer.previous_changes, current_user, company, self)
    end


    # Status ID - Подтвержден
    # Тут выставлялся статус 'на отправку' в случае если
    # if status_id != @status_accepted.id and order_params[:status_id] == @status_accepted.id
    #   # Для лидов с монстерлида особый метод установки accepted ( отправка на API )
    #   monsters_leads_accepted if not lead_key.nil?
    #   order_params[:logistic_status_id] = @status_need_send.id
    # end


    if order_params[:status_id] == @status_accepted.id and logistic_status_id == -1
      # Для лидов с монстерлида особый метод установки accepted ( отправка на API )
      monsters_leads_accepted if not lead_key.nil?
      order_params[:logistic_status_id] = @status_need_send.id
    end




    # Status ID - Отменен
    if not lead_key.nil?
      monsters_leads_canceled if order_params[:status_id] == @status_canceled.id
    end


    # Проверка на то что дата доставки и дата перезвона не может
    # быть установлена задним числом
    if order_params.has_key? :delivery_date and not order_params[:delivery_date].nil? and order_params[:delivery_date]< DateTime.now
      errors.add(:delivery_date, 'Дата доставки не может быть установлена прошлым числом')
      order_params[:delivery_date] = nil
      logger.info "Error! date cant be in past"
    end


    if order_params.has_key? :recall_date and not order_params[:recall_date].nil? and order_params[:recall_date] < DateTime.now
      errors.add(:recall_date, 'Дата прозвона не может быть установлена прошлым числом')
      order_params[:recall_date] = nil
      logger.info "Error! date cant be in past"
    end



    if not update_attributes(order_params.except(:customer, :order_products, :logs, :id))
      logger.info "#debug : #{__FILE__} : #{__method__} -> can update order with #{order_params.except(:customer, :order_products, :logs, :id)}"
    else
      log_changes(previous_changes, current_user)
    end


    send_to_websocket_after_commit
  end



  #
  #
  # M O N S T E R S L E A D S
  #
  def monsters_leads_accepted
    monsters_leads_send_request(2)
  end

  def monsters_leads_canceled
    if customer and not customer.comment.nil?
      comment = customer.comment
    else
      comment = nil
    end
    monsters_leads_send_request(3,comment)
  end

  def monsters_leads_send_request(status_id, comment = nil)
    url = "http://api.monsterleads.pro/method/lead.edit?api_key=#{Rails.configuration.monsters_lead_api_key}&lead_key=#{lead_key}&status=#{status_id}&comments=#{comment}"
    logger.info "Send to url #{url}"
    clnt = HTTPClient.new
    res = clnt.post(url, {})
    logger.info "MONSTERSLEAD POST RESULT #{res.as_json}"
    logger.info "MONSTERSLEAD POST RESULT BODY #{res.body}"
  end

end
