class Balance < ActiveRecord::Base
  belongs_to :user
  belongs_to :company

  attr_accessible :order, :order_id, :amount, :user, :payed, :company_id
end
