class Contract < ActiveRecord::Base
  belongs_to :offer
  belongs_to :company
  belongs_to :webmaster, :class_name => 'User', :validate => true

  has_many :orders, foreign_key: 'contract_id', class_name: 'Order'


  validates :offer_id, presence: true
  validates :webmaster_id, presence: true

  attr_accessible :offer, :company_id, :company, :webmaster_id, :offer_id, :accepted, :denied,:utm_source ,:utm_medium,:utm_campaign,:utm_term,:utm_content, :old_price, :new_price


end
