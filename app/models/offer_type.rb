class OfferType < ActiveRecord::Base
  belongs_to :offer
  attr_accessible :name, :per_action, :percent_price, :offer_id, :offer, :postclick
end
