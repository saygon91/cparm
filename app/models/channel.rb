class Channel < ActiveRecord::Base
  belongs_to :company

  has_many :messages, class_name: 'Message', foreign_key: :dst
end
