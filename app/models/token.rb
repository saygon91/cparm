class Token < ActiveRecord::Base
  belongs_to :company

  attr_accessible :key, :name, :company, :company_id
end
