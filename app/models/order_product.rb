class OrderProduct < ActiveRecord::Base
  belongs_to :product
  belongs_to :order
  belongs_to :company

  attr_accessible :product, :order, :company, :count, :discount, :company_id, :order_id, :product_id, :price, :percent


  def full_amount
    if percent != 0
      return (percent / 100) * price - discount
    else
      return price - discount
    end

  end

end
