class Outgo < ActiveRecord::Base
  belongs_to :warehouse_product
  belongs_to :user

  attr_accessible :count, :warehouse_product, :comment, :user


end
