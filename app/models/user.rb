class User < ActiveRecord::Base

  rolify

  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable

  attr_accessible :email, :password, :password_confirmation, :first_name, :last_name, :skype, :icq, :balance, :company_id, :blocked, :blocked_at, :unblocked_at, :block_reason

  has_many :work_companies, class_name: Employee
  has_many :companies
  has_many :messages, class_name: 'Message', foreign_key: :src
  has_many :contracts, foreign_key: 'webmaster_id', class_name: 'Contract'
  has_many :balances
  has_many :checks, class_name: 'Check', foreign_key: :webmaster_id

  # validates :first_name, presence: true
  # validates :last_name, presence: true

  def owner(company)
    return has_role? :advert_admin, company
  end

  def create_company(company_params)
    company ||= nil
    company = companies.find_or_create_by(name: company_params[:name])
    if company
      add_role :advert_admin, company
      company.update_attributes(description: company_params[:description])
    end
    company
  end

  def chat_messages(company_id, message_type)
    @messages = []
    # from user_id to current_user
    Message.where(src: id, company_id: company_id, message_type: message_type, deleted: false).map {|message|
      message.update_attributes(read: true)
      @messages << MessageSerializer.new(message, root: false)
    }

    # from current_user to user_id
    Message.where(company_id: company_id, message_type: message_type, dst: id, deleted: false).map {|message|
      @messages << MessageSerializer.new(message, root: false)
    }

    @messages
  end

  def invite_webmaster(company, email)
    invite = nil
    if is_advert_admin? and User.find_by_email(email).nil?
      hash = SecureRandom.hex
      user = User.create(email: email, password: hash[0..8])
      invite = company.invites.where(email: email, joined: false, key: hash, user: user).first_or_create if user.valid?
    end
    invite
  end

  def self.webmasters
    users = []
    all.map {|u| users << u if u.roles_name.include? 'webmaster'}
    users
  end

  def has_access(action, element)
    # action  :edit, :delete, :update, :create, etc
    # element :product, :warehouse, etc
    return true
    logger.info "#{email} has access to #{action} a #{element} ?"
    return true if is_advert?
    case action
      when :update
        case element
          when :company
            false if not is_advert_admin?
          when :product
            false if not is_advert?
          else
            logger.info "#{email} access to #{element} return default TRUE"
        end
    end
  end

  def all_companies
    list = []
    related_companies.map {|rc| list << CompanySerializer.new(rc, root: false,  scope: self) }
    list
  end

  def self.system_user
    user = find_by_email(Rails.application.config.system_user_email)
    if user.nil?
      user = User.create(email: Rails.application.config.system_user_email, password:  SecureRandom.hex[0..10], first_name: 'Система', last_name: 'Система')
      user.add_role :system if user
    end
    user
  end

  def authorize!(arg1, arg2)
    logger.info "#{arg1} - #{arg2}"
    false
  end

  def has_block?(access, object)
    logger.info "#{access} - #{object}"
    false
  end

  def name
    if not first_name.nil? and not last_name.nil?
      "#{first_name} #{last_name} - #{email}"
    else
      "#{email}"
    end
  end


  def related_to(company)
    related_companies.map {|c| return true if c.eql? company }
    false
  end

  def related_companies_ids
    @ids = []
    related_companies.map { |e| @ids << e.id}
    @ids
  end

  def related_companies
    @comps = []
    work_companies.map  {|c| @comps << c.company if not c.blocked}
    companies.map       {|c| @comps << c}
    @comps
  end


  SYSTEM_ROLES.each do |role|
    define_method "is_#{role}?" do
      has_role? role
    end

    define_method "set_#{role}" do
      add_role role
    end

    define_method "is_#{role}_of?" do |company|
      has_role? role, company
    end

  end

end
