class Company < ActiveRecord::Base
  belongs_to :user

  attr_accessible :name, :user, :description, :barcode_prefix, :last_barcode
  #last_barcode: 0, barcode_prefix: "RD010"
  has_many :warehouses
  has_many :offers
  has_many :products
  has_many :employees
  has_many :orders
  has_many :contracts
  has_many :logs
  has_many :dictionaries
  has_many :invites
  has_many :customers
  has_many :balances
  has_many :tokens
  has_many :checks

  after_create {
    DICTIONARIES.map {|d|
      d[:words].map { |w| dictionaries.create(object_type: d[:object_type], name: w) }
    }
  }

  def webmasters
    @webmasters ||= []
    contracts.map {|c| @webmasters <<  c.webmaster if c.webmaster and not @webmasters.include? c.webmaster }
    @webmasters
  end


  def generate_next_barcode(prefix)
    temp_last_barcode = self.last_barcode
    temp_last_barcode+= 1

    if prefix.nil?
      barcode = "#{barcode_prefix}%06d" % temp_last_barcode
    else
      barcode = "#{prefix}%06d" % temp_last_barcode
    end
    update_attributes(last_barcode: temp_last_barcode)
    save
    barcode
  end

  def has_status_by_id?(type, id)
    statuses(type).map {|status|
      logger.info "#{type} #{status.id.class} != #{id.class}"
      return true if status.id.eql? id.to_i
    }
    return false
  end

  def has_status?(type, status)
    statuses(type).map {|s|
      logger.info "#{type} #{s.name} != #{status}"
      #return true if s.name.eql? status
    }
    return false
  end

  def statuses(type)
    dictionaries.where(object_type: DICTIONARY_TYPES.index(type))
  end


end
