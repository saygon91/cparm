class Dictionary < ActiveRecord::Base
  belongs_to :company


  attr_accessible :name, :object_type, :additional


  def self.seed(company)
    DICTIONARIES.map {|d| d[:words].map { |w| dict = company.dictionaries.where(object_type: d[:object_type], name: w).first_or_create } }
  end

  def self.get_zip_by_id(id)
    zip = find_by(object_type: DICTIONARY_TYPES.index('индекс'))
    return zip.name if zip
    return 'индекс неизвестен'
  end
  def self.get_status_name_by_id(status_id)
    status = find_by(id: status_id, object_type: DICTIONARY_TYPES.index('статус заказа'))
    return status.name if status
    return 'статус неизвестен'
  end
  def self.get_status_id_by_name(dict_name, status_name, company_id)
    get_dict_list(dict_name, company_id).map {|status|
      return status.id if status.name.eql? status_name
    }
    return ''
  end
  def self.get_dict_list(name, company_id)
    where(object_type: DICTIONARY_TYPES.index(name), company_id: company_id)
  end
  def self.get_dict_name(name, id, company_id)
    status = find_by(id: id, object_type: DICTIONARY_TYPES.index(name), company_id: company_id)
    ret = status.nil? ? "незивестно_#{name}" : status.name
    ret
  end

  def self.get_country(id, company_id)
    status = find_by(id: id, object_type: DICTIONARY_TYPES.index('страна'), company_id: company_id)
    ret = status.nil? ? 'незивестно' : status.name
    ret
  end

  def self.get_delivery_type(id, company_id)
    status = find_by(id: id, object_type: DICTIONARY_TYPES.index('способ доставки'), company_id: company_id)
    ret = status.nil? ? 'способ_доставки_незивестно' : status.name
    ret
  end
  def self.get_courier(id, company_id)
    status = find_by(id: id, object_type: DICTIONARY_TYPES.index('курьер'), company_id: company_id)
    ret = status.nil? ? 'курьер_незивестно' : status.name
    ret
  end
  def self.order_status(id, company_id)
    status = find_by(id: id, object_type: DICTIONARY_TYPES.index('статус заказа'), company_id: company_id)
    ret = status.nil? ? 'статус_заказа_незивестно' : status.name
    ret
  end

  def self.logistic_status(id, company_id)
    status = find_by(id: id, object_type: DICTIONARY_TYPES.index('статус посылки'), company_id: company_id)
    ret = status.nil? ? 'статус_посылки_незивестно' : status.name
    ret
  end

end
