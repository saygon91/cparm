class Geo < ActiveRecord::Base
  belongs_to :offer
  attr_accessible :name, :offer
end
