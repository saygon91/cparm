class Log < ActiveRecord::Base
  belongs_to :user
  belongs_to :company
  belongs_to :order

  attr_accessible :user, :action_id, :company_id, :user_id, :order_id, :field_id, :order_product_id, :company, :old_value, :new_value, :field_str


  def self.display_all_logs
    logs = []
    all.map { |l| logs << l.as_string }
    logs
  end
  def as_string_without_date
    field_value ||= ''

    if user_id != 0
      user = User.find_by_id(user_id)
      user_name = user.nil? ? "неизвестно" : user.email
    else
      user_name = "система"
    end

    action = LOG_ACTIONS[action_id]
    field = LOG_ORDER_FIELDS[field_id]

    # Пользователь система создал заказ с ID 13
    # Пользователь corehook@gmail.com изменил поле статус заказа с 1 на 2
    # Пользователь corehook@gmail.com изменил поле статус заказа с 2 на 3
    # Пользователь corehook@gmail.com изменил поле способ доставки с 0 на 1
    # Пользователь corehook@gmail.com изменил поле статус посылки с 0 на 22
    # Пользователь corehook@gmail.com изменил поле статус заказа с 3 на 7


    case field
      when 'статус заказа'
        field_old_value = Dictionary.order_status(old_value, company_id)
        field_new_value = Dictionary.order_status(new_value, company_id)
      when 'статус посылки'
        field_old_value = Dictionary.logistic_status(old_value, company_id)
        field_new_value = Dictionary.logistic_status(new_value, company_id)
    end

    log_string = ""
    case action_id
      when LOG_ACTIONS.index('изменил')

        case field
          when 'скидка в сумме'
            if old_value.nil?
              log_string += "Пользователь #{user_name} добавил #{field} #{new_value}"
            else
              log_string += "Пользователь #{user_name} #{action} поле #{field} с #{old_value} на #{new_value}"
            end
          when 'скидка в процентах'
            if old_value.nil?
              log_string += "Пользователь #{user_name} добавил #{field} #{new_value}"
            else
              log_string += "Пользователь #{user_name} #{action} поле #{field} с #{old_value} на #{new_value}"
            end
          when 'дата доставки'
            if old_value.nil?
              log_string += "Пользователь #{user_name} добавил #{field} #{new_value}"
            else
              log_string += "Пользователь #{user_name} #{action} поле #{field} с #{old_value} на #{new_value}"
            end
          when 'дата перезвона'
            if old_value.nil?
              log_string += "Пользователь #{user_name} добавил #{field} #{new_value}"
            else
              log_string += "Пользователь #{user_name} #{action} поле #{field} с #{old_value} на #{new_value}"
            end
          when 'штрихкод'
            if old_value.nil?
              log_string += "Пользователь #{user_name} добавил #{field} #{new_value}"
            else
              log_string += "Пользователь #{user_name} #{action} поле #{field} с #{old_value} на #{new_value}"
            end
          when 'сумма доставки'
            if old_value.nil?
              log_string += "Пользователь #{user_name} добавил #{field} #{new_value}"
            else
              log_string += "Пользователь #{user_name} #{action} поле #{field} с #{old_value} на #{new_value}"
            end
          when 'комментарий'
            if old_value.nil?
              log_string += "Пользователь #{user_name} добавил #{field} #{new_value}"
            else
              log_string += "Пользователь #{user_name} #{action} поле #{field} с #{old_value} на #{new_value}"
            end

          when 'имя'
            log_string += "Пользователь #{user_name} #{action} поле #{field} с #{old_value} на #{new_value}"
          when 'фамилия'
            log_string += "Пользователь #{user_name} #{action} поле #{field} с #{old_value} на #{new_value}"
          when 'телефон'
            log_string += "Пользователь #{user_name} #{action} поле #{field} с #{old_value} на #{new_value}"
          when 'страна'
            log_string += "Пользователь #{user_name} #{action} поле #{field} с #{Dictionary.get_dict_name('страна', old_value, company_id)} на #{Dictionary.get_dict_name('страна', new_value, company_id)}"
          when 'индекс'
            log_string += "Пользователь #{user_name} #{action} поле #{field} с #{Dictionary.get_dict_name('индекс', old_value, company_id)} на #{Dictionary.get_dict_name('индекс', new_value, company_id)}"
          when 'заказ'
            log_string += "Пользователь #{user_name} #{action} поле #{field} с #{old_value} на #{new_value}"
          when 'статус заказа'
            field_old_value = Dictionary.order_status(old_value, company_id)
            field_new_value = Dictionary.order_status(new_value, company_id)
            log_string += "Пользователь #{user_name} #{action} поле #{field} с #{field_old_value} на #{field_new_value}"
          when 'статус посылки'
            field_old_value = Dictionary.logistic_status(old_value, company_id)
            field_new_value = Dictionary.logistic_status(new_value, company_id)
            log_string += "Пользователь #{user_name} #{action} поле #{field} с #{field_old_value} на #{field_new_value}"
          when 'курьер'
            field_old_value = Dictionary.get_courier(old_value, company_id)
            field_new_value = Dictionary.get_courier(new_value, company_id)
            log_string += "Пользователь #{user_name} #{action} поле #{field} с #{field_old_value} на #{field_new_value}"
          when 'способ доставки'
            log_string += "Пользователь #{user_name} #{action} поле #{field} с #{DELIVERY_TYPES[old_value.to_i]} на #{DELIVERY_TYPES[new_value.to_i]}"
          else
            log_string += "???? #{field}"
            logger.info "#{field} ????"

        end


      when LOG_ACTIONS.index('создал')  then log_string += "#{user_name} #{action} #{field} с ID #{order_id}"
      when LOG_ACTIONS.index('открыл')  then log_string += "#{user_name} #{action} #{field} с ID #{order_id}"
      when LOG_ACTIONS.index('закрыл')  then log_string += "#{user_name} #{action} #{field} с ID #{order_id}"
    end
    logger.info "Action #{action} Field #{field} | #{self.as_json} | #{log_string}"
    log_string
  end
  def as_string
    field_value ||= ''

    if user_id != 0
      user = User.find_by_id(user_id)
      user_name = user.nil? ? "неизвестно" : user.email
    else
      user_name = "система"
    end

    action = LOG_ACTIONS[action_id]
    field = LOG_ORDER_FIELDS[field_id]

    # Пользователь система создал заказ с ID 13
    # Пользователь corehook@gmail.com изменил поле статус заказа с 1 на 2
    # Пользователь corehook@gmail.com изменил поле статус заказа с 2 на 3
    # Пользователь corehook@gmail.com изменил поле способ доставки с 0 на 1
    # Пользователь corehook@gmail.com изменил поле статус посылки с 0 на 22
    # Пользователь corehook@gmail.com изменил поле статус заказа с 3 на 7


    case field
      when 'статус заказа'
        field_old_value = Dictionary.order_status(old_value, company_id)
        field_new_value = Dictionary.order_status(new_value, company_id)
      when 'статус посылки'
        field_old_value = Dictionary.logistic_status(old_value, company_id)
        field_new_value = Dictionary.logistic_status(new_value, company_id)
    end

    log_string = "#{created_at.to_s(:long)} "
    case action_id
      when LOG_ACTIONS.index('изменил')

        case field
          when 'скидка в сумме'
            if old_value.nil?
              log_string += "Пользователь #{user_name} добавил #{field} #{new_value}"
            else
              log_string += "Пользователь #{user_name} #{action} поле #{field} с #{old_value} на #{new_value}"
            end
          when 'скидка в процентах'
            if old_value.nil?
              log_string += "Пользователь #{user_name} добавил #{field} #{new_value}"
            else
              log_string += "Пользователь #{user_name} #{action} поле #{field} с #{old_value} на #{new_value}"
            end
          when 'дата доставки'
            if old_value.nil?
              log_string += "Пользователь #{user_name} добавил #{field} #{new_value}"
            else
              log_string += "Пользователь #{user_name} #{action} поле #{field} с #{old_value} на #{new_value}"
            end
          when 'дата перезвона'
            if old_value.nil?
              log_string += "Пользователь #{user_name} добавил #{field} #{new_value}"
            else
              log_string += "Пользователь #{user_name} #{action} поле #{field} с #{old_value} на #{new_value}"
            end
          when 'штрихкод'
            if old_value.nil?
              log_string += "Пользователь #{user_name} добавил #{field} #{new_value}"
            else
              log_string += "Пользователь #{user_name} #{action} поле #{field} с #{old_value} на #{new_value}"
            end
          when 'сумма доставки'
            if old_value.nil?
              log_string += "Пользователь #{user_name} добавил #{field} #{new_value}"
            else
              log_string += "Пользователь #{user_name} #{action} поле #{field} с #{old_value} на #{new_value}"
            end
          when 'комментарий'
            if old_value.nil?
              log_string += "Пользователь #{user_name} добавил #{field} #{new_value}"
            else
              log_string += "Пользователь #{user_name} #{action} поле #{field} с #{old_value} на #{new_value}"
            end

          when 'имя'
            log_string += "Пользователь #{user_name} #{action} поле #{field} с #{old_value} на #{new_value}"
          when 'фамилия'
            log_string += "Пользователь #{user_name} #{action} поле #{field} с #{old_value} на #{new_value}"
          when 'телефон'
            log_string += "Пользователь #{user_name} #{action} поле #{field} с #{old_value} на #{new_value}"
          when 'страна'
            log_string += "Пользователь #{user_name} #{action} поле #{field} с #{Dictionary.get_dict_name('страна', old_value, company_id)} на #{Dictionary.get_dict_name('страна', new_value, company_id)}"
          when 'индекс'
            log_string += "Пользователь #{user_name} #{action} поле #{field} с #{Dictionary.get_dict_name('индекс', old_value, company_id)} на #{Dictionary.get_dict_name('индекс', new_value, company_id)}"
          when 'заказ'
            log_string += "Пользователь #{user_name} #{action} поле #{field} с #{old_value} на #{new_value}"
          when 'статус заказа'
            field_old_value = Dictionary.order_status(old_value, company_id)
            field_new_value = Dictionary.order_status(new_value, company_id)
            log_string += "Пользователь #{user_name} #{action} поле #{field} с #{field_old_value} на #{field_new_value}"
          when 'статус посылки'
            field_old_value = Dictionary.logistic_status(old_value, company_id)
            field_new_value = Dictionary.logistic_status(new_value, company_id)
            log_string += "Пользователь #{user_name} #{action} поле #{field} с #{field_old_value} на #{field_new_value}"
          when 'курьер'
            field_old_value = Dictionary.get_courier(old_value, company_id)
            field_new_value = Dictionary.get_courier(new_value, company_id)
            log_string += "Пользователь #{user_name} #{action} поле #{field} с #{field_old_value} на #{field_new_value}"
          when 'способ доставки'
            log_string += "Пользователь #{user_name} #{action} поле #{field} с #{DELIVERY_TYPES[old_value.to_i]} на #{DELIVERY_TYPES[new_value.to_i]}"
          else
            log_string += "???? #{field}"
            logger.info "#{field} ????"

        end


      when LOG_ACTIONS.index('создал')  then log_string += "#{user_name} #{action} #{field} с ID #{order_id}"
      when LOG_ACTIONS.index('открыл')  then log_string += "#{user_name} #{action} #{field} с ID #{order_id}"
      when LOG_ACTIONS.index('закрыл')  then log_string += "#{user_name} #{action} #{field} с ID #{order_id}"
    end
    logger.info "Action #{action} Field #{field} | #{self.as_json} | #{log_string}"
    log_string
  end
end
