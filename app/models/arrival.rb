class Arrival < ActiveRecord::Base
  belongs_to :warehouse
  belongs_to :product
  belongs_to :user

  attr_accessible :count, :warehouse, :comment, :user, :product


end
