class Message < ActiveRecord::Base
  attr_accessible :src, :dst, :message, :message_type, :deleted, :company_id, :read, :readed_at

  validates :message, length: {minimum: 3}, allow_blank: false

  after_create {
    send_to_websocket
  }
  after_commit {
    #send_to_websocket
  }

  def send_to_websocket
    sended_users = []
    dst_user = nil

    WebsocketRails.users.map {|ws_user|
      logger.info "-"*50
      logger.info "Message from ID #{src} to ID #{dst}"
      logger.info "WebsocketRails user #{ws_user.user.as_json}"

      if ws_user.user.id == dst
        dst_user = ws_user.user
        ws_user.send_message('advert.new_message', {company_id: company_id,object: MessageSerializer.new(self, root: false)})
        break
      end

      # if ws_user.user.related_companies_ids.include? company_id
      #   if not sended_users.include? ws_user
      #     if ws_user.send_message('advert.new_message', {company_id: company_id,object: MessageSerializer.new(self, root: false)})
      #       sended_users << ws_user
      #       break
      #     end
      #   end
      #
      #
      # else
      #   logger.info "User #{ws_user.user.id} company_ids [#{ws_user.user.related_companies_ids}] not include #{company_id}"
      # end
      #
      # logger.info "Sended Users #{sended_users}"
      # logger.info "-"*50
    }
    logger.info "Sended message #{message} to #{dst_user}"
  end
end
