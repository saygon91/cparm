@utilz = angular.module('cparm.utilz', [])

@utilz.factory 'Alerts', ['$http', ($http) ->
  class Alerts
    alerts = []

    add_alert : (msg) ->
      $(".top-right").notify(
        type: msg.type
        message:
          text: msg.message
      ).show()
      alerts.push msg

    close_alert : (index) -> alerts.splice index, 1

    get_alerts : -> alerts

    show_errors: (errors) ->
      msg = {}
      msg['message'] = errors[0]
      msg['type'] = 'error'
      @add_alert(msg)
      #@add_alert alert for alert in errors
      return true

    show_alerts: (alerts) ->
      msg = {}
      msg['message'] = alerts[0]
      msg['type'] = 'success'
      @add_alert(msg)
      return true

  new Alerts()
]

@utilz.factory 'Logging', ['$http', ($http) ->
  class Logging
    log_message = (message) ->
      if typeof message == 'object'
        console.log 'Dumping object : '
        console.log message
      else
        console.log('Log : '+message)

    debug_message = (message) ->
      console.log('Debug : '+message)

    log: log_message
    debug: debug_message

  new Logging()
]

@utilz.factory 'Urls', [ 'Logging', (Logging) ->
  class Urls
    urls =
      advert_all:       '/advert/company/all.json'

      export_to_registry:            '/advert/company/order/export_to_registry'     # company_id, orders_ids
      print_orders_form:             '/advert/company/order/print_orders_form'      # company_id, order_ids
      generate_salary:               '/advert/company/employee/salary'              # company_id, date_from, date_to
      generate_statistic:            '/advert/company/order/generate_statistic'     # date_from, date_to, status_from, status_to
      generate_statistic_wm:         '/webmaster/contract/statistic'                # date_from, date_to, contract_id
      generate_accepted_stat:        '/advert/company/order/accepted_statistic'     # company_id, date_from, date_to
      generate_barcodes:             '/advert/company/order/generate_barcodes'      #
      orders_change_delivery_amount: '/advert/company/order/change_delivery_amount' # company_id, order_ids, delivery_amount
      generate_bills:                '/advert/company/billing/calculate'            # company_id, webmaster_id, pay:boolean, date_from, date_to
      generate_bills_wm:             ''
      orders_to_xls:    '/advert/company/order/xls'                 # company_id, order_ids, columns
      search_orders:    '/advert/company/order/search_orders'       # company_id, search_text, fields : if true
      filter_logs:      '/advert/company/order/filter_logs'         # company_id, filters : [{action_id, field_id, dictionary_id}, {action_id, field_id, dictionary_id}]
      order_status_change: '/advert/company/order/status_change'    # {company_id, status_id, logistic_status_id, order_ids [1,2,3]} , return new response[:orders]
      order_logs:       '/advert/company/order/logs'                # company_id, order_id, open
      order_close:      '/advert/company/order/close'               # company_id, order_id
      new_order_status: '/advert/company/new_order_status'          # status
      get_log:          '/advert/company/log'                       # company_id, order_id
      get_logs:         '/advert/company/logs'                      # company_id, limit

      accept_contract:  '/advert/company/contract/accept'
      denied_contract:  '/advert/company/contract/denied'

      new_product:      '/advert/company/product/new'               # company_id, product
      delete_product:   '/advert/company/product/delete'            # company_id, product_id
      update_product:   '/advert/company/product/update'            # company_id, product_id, product

      new_offer:        '/advert/company/offer/new'                 # company_id, offer
      update_offer:     '/advert/company/offer/update'              # company_id, offer_id,   offer

      all_orders:       '/advert/company/order/all'                 # company_id
      update_order:     '/advert/company/order/update'              # company_id, order_id,   order
      delete_offer:     '/advert/company/offer/delete'              # company_id, offer_id

      new_employee:     '/advert/company/employee/new'              # company_id, employee
      update_employee:  '/advert/company/employee/update'           # company_id, employee_id, user_data
      all_eployees:     '/advert/company/employee/all.json'         # company_id
      block_employee:   '/advert/company/employee/block'            # company_id, employee_id, block_reason
      unblock_employee: '/advert/company/employee/unblock'          # company_id, employee_id, unblock_reason

      new_company:      '/advert/company/new'                       # company
      update_company:   '/advert/company/update'                    # company_id, company

      new_warehouse:    '/advert/company/warehouse/new'             # company_id, warehouse
      update_warehouse: '/advert/company/warehouse/update'          # warehouse_id, company_id, warehouse
      delete_warehouse: '/advert/company/warehouse/delete'          # warehouse_id

      product_arrival:  '/advert/company/warehouse/arrival/new'     # company_id, warehouse_id, product_id, count, comment
      product_outgo:    '/advert/company/warehouse/outgo/new'       # company_id, warehouse_id, product_id, count, comment

      webmaster_all:    '/webmaster/all.json'                       # nothing
      invite_webmaster: '/advert/company/invite/new'                # company_id, email

      connect_offer:    '/webmaster/offer/connect'                  # offer_id

      new_dictionarie:  '/advert/company/dictionary/new'            # company_id, word
      update_dictionarie: '/advert/company/dictionary/update'       # company_id, dictionary

      update_customer:  '/advert/company/customer/update'           # company_id, customer_id, customer

    get: (name) ->
      if urls[name]
        Logging.log 'Urls : '+name
        return urls[name]
      else
        return '/nourl/'


  new Urls()
]