@advert.factory 'Companies', ['Logging', '$http', '$rootScope', '$timeout', 'Urls', 'Alerts', (Logging, $http, $rootScope, $timeout, Urls, Alerts) ->
  class Companies
    user = []
    roles = []
    statuses = []
    dictionary_types = []
    currency = []
    companies = []
    company = undefined
    dispatcher = undefined
    init_callback = []
    websocket_connected = false

    ws_new_object = (message) ->
      switch message.object_type
        when "order"
          Logging.log message
          $rootScope.$apply ->
            for comp in companies when message.company_id is comp.id
              comp.orders.push angular.copy message.object
          msg = {}
          msg.type = "success"
          msg.message = "Получен новый заказ"
          Alerts.add_alert(msg);
        else
          alert 'UNKNOWN_OBJECT_FROM_WEBSOCKET '+message.object_type

    ws_update_object = (message) ->
      switch message.object_type
        when "order"
          Logging.log message
          $rootScope.$apply ->
            isExists = false
            for comp in companies when message.company_id is comp.id
              for order, key in comp.orders when order.id is message.object.id
                isExists = true
                comp.orders.splice(key, 1)
                comp.orders.splice(key, 0, message.object)
            comp.orders.push angular.copy message.object if not isExists
        else
          alert 'UNKNOWN_OBJECT_FROM_WEBSOCKET '+message.object_type

    ws_new_message = (message) ->
      $rootScope.$apply ->
        company["messages"] = [] if company["messages"] is `undefined`
        company['messages'].push message.object for comp in companies when comp.id is message.company_id
      msg = {}
      msg.type = "success"
      msg.message = "Получено новое сообщение"
      Alerts.add_alert(msg);

    send_chat_message_success = (message) ->
      $rootScope.$apply ->
        company["messages"] = [] if company["messages"] is `undefined`
        company['messages'].push message.message
      Logging.log "send_chat_message_success"

    send_chat_message: (message, dst, message_type) ->
      packet =
        company_id: company.id
        message: message
        message_type: message_type
        dst: dst
      dispatcher.trigger('advert.send_message', packet, send_chat_message_success)

    message_readed_success = (message) ->
      Logging.log "message_readed_success"
      Logging.log message

    get_chat_history_success = (message) ->
      Logging.log "get_chat_history_success"
      Logging.log message
      $rootScope.$apply ->
        company["messages"] = [] if company["messages"] is `undefined`
        if message.messages
          for message in message.messages
            if message.dst is user.id and !message.read
              packet =
                message_id: message.id
                company_id: company.id
              dispatcher.trigger('advert.message_readed', packet, message_readed_success)
              message.read = true
            company['messages'].push message

    get_chat_history_failure = (message) ->
      Logging.log message

    get_chat_history: (message_type, dst) ->
      packet =
        company_id: company.id
        message_type: message_type
        dst: dst
      dispatcher.trigger('advert.chat_history', packet, get_chat_history_success, get_chat_history_failure)

    init_websocket = (websocket_url) ->
      Logging.log "init websockets"
      if !websocket_connected
        dispatcher = new WebSocketRails(websocket_url)
        websocket_connected = true
        dispatcher.bind('advert.new_object', ws_new_object)
        dispatcher.bind('advert.update_object', ws_update_object)
        dispatcher.bind('advert.new_message', ws_new_message)
        dispatcher.bind 'connection_closed', ->
          websocket_connected = false
          Logging.log "connection_closed, trying to reconnect..."
          init_websocket(websocket_url)

    init: (done_callback) ->
      $http.get(Urls.get('advert_all'), {}
      ).then((response, status, headers, config) ->
        if response.data.success
          user = response.data.user
          roles = response.data.roles
          statuses = response.data.statuses
          dictionary_types = response.data.dictionary
          currency = response.data.currency
          if response.data.companies.length > 0
            companies = response.data.companies
            company = companies[0]

          init_websocket(response.data.websocket_url)

          init_callback = done_callback
          done_callback()
          Logging.log 'json fetched from post request'
        else
          Logging.log 'произошла ошибка при подгурзке данных'
          msg = {}
          msg.type = "error"
          msg.message = "Произошла ошибка при подгурзке данных"
          Alerts.add_alert(msg);
      )

    get_all_logs: () ->
      $http.post(Urls.get('get_logs'), {
          company_id: company.id,
          limit: 100
        }
      ).then((response, status, headers, config) ->
        if response.data.success
          company.logs = response.data.logs
          Logging.log 'json fetched logs request'
        else
          Logging.log 'произошла ошибка при подгурзке данных'
      )

    get_order_logs: (order, open) ->
      $http.post(Urls.get('order_logs'), {
          company_id: company.id,
          order_id: order.id,
          open: open
        }
      ).then((response, status, headers, config) ->
        if response.data.success
          order["logs"] = response.data.logs
          Logging.log order
          Logging.log 'json fetched logs request'
        else
          Logging.log 'произошла ошибка при подгурзке данных'
      )

    order_close: (order) ->
      $http.post(Urls.get('order_close'), {
          company_id: company.id,
          order_id: order.id
        }
      ).then((response, status, headers, config) ->
        if response.data.success
          Logging.log 'modal closed event post success'
        else
          Logging.log 'произошла ошибка при подгурзке данных'
      )

    update_orders: (orders) ->
      for item in orders
        for temp in company.orders when temp.id is item.id
          temp = angular.copy item

    get_user: () ->
      Logging.log "get_user"
      user

    get_roles: () ->
      Logging.log "get_roles"
      roles

    get_statuses: () ->
      Logging.log "get_statuses"
      statuses

    get_dictionary_types: () ->
      Logging.log "get_dictionary_types"
      dictionary_types

    get_currency: () ->
      Logging.log "get_currency"
      currency

    get_all_companies: () ->
      Logging.log "get_all_companies"
      companies

    get_all_products: () ->
      Logging.log "get_all_products"
      company.products

    get_all_warehouses: () ->
      Logging.log "get_all_warehouses"
      company.warehouses

    get_by_id: (array_name, id) ->
      if array_name of company
        result = undefined
        result = item for item in company[array_name] when item.id is id
        result

    get_dictionary: (object_type, field, value) ->
      items = []
      items.push item for item in company.dictionaries when item.object_type is object_type and item[field] is value
      items

    delete_by_id: (array_name, item) ->
      status = false
      if array_name of company
        array_name_singular = array_name.substring 0, array_name.length - 1
        params = {}
        params["company_id"] = company.id
        params[array_name_singular + "_id"] = item.id
        $http.post(Urls.get('delete_'+array_name_singular), params).success((data, status, headers, config) ->

          if data.success
            company[array_name].splice(company[array_name].indexOf(item), 1)
            Logging.log 'delete_by_id from ' + array_name + " element id: " + item.id
            msg = {}
            msg.type = "success"
            msg.message = "Успешно удалено"
            Alerts.add_alert(msg);
            status = true
          else
            Alerts.show_errors(data.errors)
        ).error (data, status, headers, config) ->
          Logging.log data
          Alerts.show_errors(data.errors)
          status = false

      return status

    add_new_item: (array_name, params, modal_name) ->
      if array_name of company
        array_name_singular = array_name.substring 0, array_name.length - 1
        $http.post(Urls.get('new_'+array_name_singular), params).success((data, status, headers, config) ->
          if data.success && array_name_singular of data
            temp[array_name].push data[array_name_singular] for temp in companies when temp.id is company.id
            Logging.log "add_new_" + array_name_singular
            msg = {}
            msg.type = "success"
            msg.message = "Успешно добавлено"
            Alerts.add_alert(msg);
          else
            Alerts.show_errors(data.errors)
          $('#'+modal_name).modal('hide');
        ).error (data, status, headers, config) ->
          Logging.log data
          Alerts.show_errors(data.errors)
        true

    edit_item: (array_name, params, modal_name) ->
      if array_name of company
        array_name_singular = array_name.substring 0, array_name.length - 1
        $http.post(Urls.get('update_'+array_name_singular), params).success((data, status, headers, config) ->
          if data.success && array_name_singular of data
            for temp_company in companies when temp_company.id is company.id
              temp = data[array_name_singular] for temp in temp_company[array_name] when temp.id is data[array_name_singular].id
            Logging.log "edit_" + array_name_singular
            msg = {}
            msg.type = "success"
            msg.message = "Успешно изменено"
            Alerts.add_alert(msg);
          else
            Alerts.show_errors(data.errors)
          $('#'+modal_name).modal('hide');
        ).error (data, status, headers, config) ->
          Logging.log data
          Alerts.show_errors(data.errors)
        true

    get_current_company: () ->
      Logging.log "get_current_company : "+company.name if company
      company

    change_current_company: (comp) ->
      company = comp
      init_callback()

    get_company_id: (id) ->
      result = company for company in companies when company.id is id
      result

    add_new_company: (new_company) ->
      $http.post(Urls.get('new_company'),
        company:  new_company
      ).success((data, status, headers, config) ->
        Logging.log "add_new_company"
        if data.success && data.company
          companies.push data.company
          company = companies[companies.indexOf(data.company)]
          console.log 'New company : '+company.name
          msg = {}
          msg.type = "success"
          msg.message = "Успешно добавлено"
          Alerts.add_alert(msg);
        else
          Alerts.show_errors(data.errors)
        $('#company-add-modal').modal('hide');
      ).error (data, status, headers, config) ->
        Logging.log data
        Alerts.show_errors(data.errors)
      true

    edit_company: (company) ->
      $http.post(Urls.get('update_company'),
        company_id: company.id
        company: company
      ).success((data, status, headers, config) ->
        Logging.log "edit_company"
        if data.success && data.company
          temp_company = data.company for temp_company in companies when temp_company.id is data.company.id
          msg = {}
          msg.type = "success"
          msg.message = "Успешно изменено"
          Alerts.add_alert(msg);
        else
          Alerts.show_errors(data.errors)
        $('#company-add-modal').modal('hide');
      ).error (data, status, headers, config) ->
        Logging.log data
        Alerts.show_errors(data.errors)
      true

    block_employee: (employee) ->
      $http.post(Urls.get('block_employee'),
        company_id: company.id
        employee_id: employee.id,
        block_reason: ""
      ).success((data, status, headers, config) ->
        Logging.log "block_employee"
        if data.success
          employee.blocked = true
          Logging.log true
        else
          Alerts.show_errors(data.errors)
      ).error (data, status, headers, config) ->
        Logging.log data
      true

    unblock_employee: (employee) ->
      $http.post(Urls.get('unblock_employee'),
        company_id: company.id
        employee_id: employee.id,
        unblock_reason: ""
      ).success((data, status, headers, config) ->
        Logging.log "unblock_employee"
        if data.success
          employee.blocked = false
          Logging.log true
        else
          Alerts.show_errors(data.errors)
      ).error (data, status, headers, config) ->
        Logging.log data
      true

    product_arrival: (params, modal_name) ->
      $http.post(Urls.get('product_arrival'), params).success((data, status, headers, config) ->
        if data.success
          for temp in company.warehouses when temp.id is params.warehouse_id
            temp.arrivals.push data.arrival
            existed = false
            for warehouse_product, i in temp.warehouse_products when warehouse_product.id is data.warehouse_product.id
              temp.warehouse_products[i] = data.warehouse_product
              existed = true
            temp.warehouse_products.push data.warehouse_product if not existed
          Logging.log "product_arrival"
          msg = {}
          msg.type = "success"
          msg.message = "Успешно добавлено"
          Alerts.add_alert(msg);
        else
          Alerts.show_errors(data.errors)
        $('#'+modal_name).modal('hide');
      ).error (data, status, headers, config) ->
        Logging.log data
        Alerts.show_errors(data.errors)
      true

    product_outgo: (params, modal_name) ->
      $http.post(Urls.get('product_outgo'), params).success((data, status, headers, config) ->
        if data.success
          for temp in company.warehouses when temp.id is params.warehouse_id
            temp["outgos"].push data.outgo
            existed = false
            for warehouse_product, i in temp.warehouse_products when warehouse_product.id is data.warehouse_product.id
              if data.warehouse_product.count is 0
                temp.warehouse_products.splice(i, 1)
              else
                temp.warehouse_products[i] = data.warehouse_product
              existed = true
            temp.warehouse_products.push data.warehouse_product if not existed
          Logging.log "product_outgo"
          msg = {}
          msg.type = "success"
          msg.message = "Успешно добавлено"
          Alerts.add_alert(msg);
        else
          Alerts.show_errors(data.errors)
        $('#'+modal_name).modal('hide');
      ).error (data, status, headers, config) ->
        Logging.log data
        Alerts.show_errors(data.errors)
      true

    accept_contract: (contract) ->
      $http.post(Urls.get('accept_contract'),
        company_id: company.id,
        contract_id: contract.id
      ).success((data, status, headers, config) ->
        Logging.log "accept_contract"
        if data.success
          for item in company.contracts when item.id is contract.id
            angular.copy(data.contract, item)
          Alerts.show_alerts(data.alerts)
        else
          Alerts.show_errors(data.errors)
      ).error (data, status, headers, config) ->
        Logging.log data
      true

    decline_contract: (contract) ->
      $http.post(Urls.get('denied_contract'),
        company_id: company.id,
        contract_id: contract.id
      ).success((data, status, headers, config) ->
        Logging.log "decline_contract"
        if data.success
          for item in company.contracts when item.id is contract.id
            angular.copy(data.contract, item)
          Alerts.show_alerts(data.alerts)
        else
          Alerts.show_errors(data.errors)
      ).error (data, status, headers, config) ->
        Logging.log data

    #not used for now
    new_order_status: (new_status_name, modal_name) ->
      $http.post(Urls.get('new_order_status'),
        status: new_status_name
      ).success((data, status, headers, config) ->
        Logging.log "new_order_status"
        if data.success
          statuses.push data.status
          Alerts.show_alerts(data.alerts)
        else
          Alerts.show_errors(data.errors)
        $('#'+modal_name).modal('hide')
      ).error (data, status, headers, config) ->
        Logging.log data
      true

    invite_webmaster: (webmaster, modal_name) ->
      $http.post(Urls.get('invite_webmaster'),
        company_id: company.id,
        email: webmaster.email
      ).success((data, status, headers, config) ->
        Logging.log "invite_webmaster"
        if data.success
          Alerts.show_alerts(data.alerts)
        else
          Alerts.show_errors(data.errors)
        $('#'+modal_name).modal('hide')
      ).error (data, status, headers, config) ->
        Logging.log data
      true

  new Companies()
]