@offers = angular.module('OffersApp', [])
@orders = angular.module('OrdersApp', [])
@products = angular.module('ProductsApp', [])
@warehouses = angular.module('WarehousesApp', [])
@employees = angular.module('EmployeesApp', [])
@statistic = angular.module('StatisticApp', [])
@companies = angular.module('CompaniesApp', [])
@contracts = angular.module('ContractsApp', [])
@customers = angular.module('CustomersApp', [])
@logs = angular.module('LogsApp', [])
@animations = angular.module('AnimationsApp', ['ngAnimate'])
@messages = angular.module('MessagesApp', [])
@settings = angular.module('SettingsApp', [])
@billing = angular.module('BillingApp', [])

@advert = angular.module('advert', ['localytics.directives', 'ui.bootstrap', 'angular-loading-bar', 'cparm.utilz', 'ngRoute', 'AnimationsApp', 'OffersApp', 'ProductsApp', 'WarehousesApp', 'EmployeesApp', 'CustomersApp', 'StatisticApp', 'CompaniesApp', 'ContractsApp', 'LogsApp', 'MessagesApp', 'SettingsApp', 'BillingApp'])

@advert.config ($httpProvider) ->          $httpProvider.defaults.withCredentials = true
@advert.config (cfpLoadingBarProvider) ->  cfpLoadingBarProvider.includeSpinner = true

@advert.controller 'AdvertController', ['cfpLoadingBar', '$http', 'Alerts', 'Urls', '$scope', 'Logging', '$route', '$routeParams', '$location', 'Companies', (cfpLoadingBar, $http, Alerts, Urls, $scope, Logging, $route, $routeParams, $location,Companies) ->
  $scope.isActive = (route) -> route is $location.path()
  $scope.alerts = Alerts.get_alerts()

  init_done = ->
    $scope.user = Companies.get_user()
    $scope.roles = Companies.get_roles()
    $scope.statuses = Companies.get_statuses()
    $scope.dictionary_types = Companies.get_dictionary_types()
    $scope.currency = Companies.get_currency()
    $scope.company = Companies.get_current_company()
    $scope.all_orders = $scope.company.orders
    $scope.companies = Companies.get_all_companies()
    $scope.warehouse = $scope.company.warehouses[0] if $scope.company and $scope.company.warehouses.length > 0
    $scope.debug = false
    window._scope = $scope
    window._company = $scope.company
    window._companies = $scope.companies

    $scope.access =
      OffersCtrl: 'advert_admin'
      ContractsCtrl: 'advert_admin'
      EmployeesCtrl: 'advert_admin'
      OrdersCtrl: 'advert_admin, log_manager, log_operator, call_operator, call_manager'
      CustomersCtrl: 'advert_admin, log_manager, log_operator, call_operator, call_manager'
      ProductsCtrl: 'advert_admin, log_manager, log_operator'
      WarehousesCtrl: 'advert_admin, log_manager, storekeeper'
      WarehouseCtrl: 'advert_admin, log_manager, storekeeper'
      BillingCtrl: 'advert_admin'
      StatisticCtrl: 'advert_admin'
      LogsCtrl: 'advert_admin'
      MessagesCtrl: null
      DictionaryCtrl: 'advert_admin'
      SettingsCtrl: 'advert_admin'

    $scope.redirectIfNoAccess()

  Companies.init(init_done)

  $scope.hasAccess = (roles_list_string) ->
    has_access = false
    if $scope.company?
      if roles_list_string?
        has_roles = roles_list_string.split /,\s*/
        for role in has_roles
          for temp in $scope.company.roles when role is temp.name
            has_access = true
      else
        has_access = true
    has_access

  $scope.test = (param) ->
    Logging.log param
    Logging.log "it works!"

  $scope.redirectIfNoAccess = () ->
    $location.path('advert/no_access') if ($scope.access and !$scope.hasAccess($scope.access[$route.current.controller])) or !$scope.company
]