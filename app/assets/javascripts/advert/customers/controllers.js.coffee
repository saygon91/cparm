@customers.controller 'CustomersCtrl', ['$scope', '$http', 'Logging', 'Urls', 'Companies', ($scope, $http, Logging, Urls, Companies) ->
  $scope.redirectIfNoAccess()
  $scope.array_name = "customers"
  $scope.current_customer = {}

  $scope.open_customer = (customer) ->
    $scope.current_customer = customer
    $("#customer-edit-modal").modal("show")
    true

  $scope.save_customer = (customer) ->
    if customer.id
      modal_name = "customer-edit-modal"
      params = {}
      params["company_id"] = $scope.company.id
      params["customer_id"] = customer.id
      params["customer"] = customer
      Companies.edit_item($scope.array_name, params, modal_name)
    return
]