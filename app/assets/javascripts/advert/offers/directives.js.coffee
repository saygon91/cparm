@offers.directive 'OffersDirective', ['$http', ($http) ->
  promise = null
  getOffers= ->
    if promise
      promise
    else
      promise = $http.get("advert/offers.json")
      promise
]