@offers.controller 'OffersCtrl', ['$scope', 'Logging', 'Urls', 'Companies', ($scope, Logging, Urls, Companies) ->
  $scope.redirectIfNoAccess()
  $scope.offer = {}
  $scope.array_name = "offers"
  $scope.private_types = [
    { value: true, name: 'Закрыт' }
    { value: false, name: 'Открыт' }
  ]

  $scope.add_to_list = (key, value) ->
    $scope.offer[key] = [] if $scope.offer[key] is `undefined`
    $scope.offer[key].push value  if $scope.offer[key].indexOf(value) is -1
    Logging.log "adding value to list: "+key+" value: " + value
    return

  $scope.delete_from_list = (key, index) ->
    console.log $scope.offer[key]
    $scope.offer[key].splice(index, 1)
    window.offer = $scope.offer
    console.log $scope.offer
    Logging.log "removing value from list: "+key+" value: " + index
    return

  $scope.add_offer = () ->
    console.log $scope.offer
    if $scope.offer.id
      modal_name = "offers-add-modal"
      params = {}
      params["company_id"] = $scope.company.id
      params["offer_id"] = $scope.offer.id
      params["offer"] = $scope.offer
      Companies.edit_item($scope.array_name, params, modal_name)
    else
      modal_name = "offers-add-modal"
      params = {}
      params["company_id"] = $scope.company.id
      params["offer"] = $scope.offer
      Companies.add_new_item($scope.array_name, params, modal_name)
    return

  $scope.delete_offer = (id) ->
    Companies.delete_by_id $scope.array_name, Companies.get_by_id($scope.array_name, id)
    return

  $scope.edit_offer = (id) ->
    $scope.offer = {}
    $scope.offer = Companies.get_by_id($scope.array_name, id)
    if $scope.offer
      $('#offers-add-modal').modal('show');
    else
#      error alert
      alert("Error epta");
    return
]