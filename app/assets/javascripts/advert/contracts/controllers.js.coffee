@contracts.controller 'ContractsCtrl', ['$scope', 'Logging', 'Urls', 'Companies', ($scope, Logging, Urls, Companies) ->
  $scope.redirectIfNoAccess()
  $scope.webmaster = {}

  $scope.accept_contract = (contract) ->
    Companies.accept_contract(contract)

  $scope.decline_contract = (contract) ->
    Companies.decline_contract(contract)

  $scope.invite_webmaster = (webmaster) ->
    Companies.invite_webmaster(webmaster, "invite-webmaster")
]