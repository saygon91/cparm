@messages.controller 'MessagesCtrl', ['$scope', '$timeout', 'Logging', 'Companies', ($scope, $timeout, Logging, Companies) ->
  $scope.redirectIfNoAccess()
  $scope.current_chat = {}

  $scope.send_message = (event) ->
    Companies.send_chat_message($scope.message, $scope.current_chat.user_id, $scope.current_chat.message_type)
    $scope.message = ''
    event.preventDefault();
    event.stopPropagation();
    false

  $scope.open_dialog = (user) ->
    if !user.dialog_opened?
      Companies.get_chat_history(1, user.user_id)
    $scope.current_chat =
      user_id: user.user_id
      message_type: 1
    user['dialog_opened'] = true
    $timeout (->
      $('#text-message').focus()
      return
    ), 100
    true

  $(document).ready ->
    n = 1
    $('.contacts .list').on 'mousewheel', (event) ->
      offset = $('.contacts .list .contact:last-of-type').offset()
      height = $('.contacts .list').height()
      amount = $('.contacts .list .contact').length
      if event.deltaY < 0
        if amount - n + 1 > Math.floor(height / 65)
          $('.contacts .list .contact:nth-of-type(' + n + ')').addClass 'folded'
          n = n + 1
      else if n > 1
        n = n - 1
        $('.contacts .list .contact:nth-of-type(' + n + ')').removeClass 'folded'
      return
    return
]