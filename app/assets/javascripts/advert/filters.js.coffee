@advert.filter 'get_by_id', ['Companies', (Companies) ->
  (id, array_name, key) ->
    item = {}
    if array_name
      item = Companies.get_by_id(array_name, id)
    if item and key of item
      item[key]
    else
      item
]

@advert.filter 'last_status_change', ['Companies', (Companies) ->
  (status, order, last) ->
    result = ""
    if "logs" of order
      for log in order.logs
        if log.indexOf("на #{status.name}") > -1
          log_words = log.split ' '
          result = "#{status.name}: #{log_words[0]} #{log_words[1]}"
          result += ", " if !last
    result
]

@advert.filter 'tenge_currency', [
  '$filter'
  '$locale'
  ($filter, $locale) ->
    currency = $filter('currency')
    formats = $locale.NUMBER_FORMATS
    (amount, symbol) ->
      if amount?
        value = currency(amount, '₸ ', 0)
        tenge = value.split ' '
        if tenge[0] is '₸'
          tenge[1] + ' ' + tenge[0]
        else
          value
]