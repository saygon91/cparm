@offers.directive 'optionsDisabled', ($parse) ->

  disableOptions = (scope, attr, element, data, fnDisableIfTrue) ->
    # refresh the disabled options in the select element.
    angular.forEach element.find('option'), (value, index) ->
      elem = angular.element(value)
      if elem.val() != ''
        locals = {}
        locals[attr] = data[index]
        elem.attr 'disabled', fnDisableIfTrue(scope, locals)
        console.log elem
      return
    return

  {
  priority: 0
  require: 'ngModel'
  link: (scope, iElement, iAttrs, ctrl) ->
    # parse expression and build array of disabled options
    expElements = iAttrs.optionsDisabled.match(/^\s*(.+)\s+for\s+(.+)\s+in\s+(.+)?\s*/)
    attrToWatch = expElements[3]
    fnDisableIfTrue = $parse(expElements[1])
    scope.$watch attrToWatch, ((newValue, oldValue) ->
      if newValue
        console.log attrToWatch
        disableOptions scope, expElements[2], iElement, newValue, fnDisableIfTrue
      return
    ), true
    # handle model updates properly
    scope.$watch iAttrs.ngModel, (newValue, oldValue) ->
      disOptions = $parse(attrToWatch)(scope)
      if newValue
        disableOptions scope, expElements[2], iElement, disOptions, fnDisableIfTrue
      return
    return

  }