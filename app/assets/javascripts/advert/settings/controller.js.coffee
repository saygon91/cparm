@settings.controller 'SettingsCtrl', ['$scope', '$http', '$location', 'Urls', 'Logging', 'Companies', ($scope, $http, $location, Urls, Logging, Companies) ->
  $scope.redirectIfNoAccess()
]

@settings.controller 'DictionaryCtrl', ['$scope', '$route', '$routeParams', '$http', '$location', 'Urls', 'Logging', 'Companies', ($scope, $route, $routeParams, $http, $location, Urls, Logging, Companies) ->
  $scope.redirectIfNoAccess()

  $scope.array_name = "dictionaries"
  $scope.current_dictionary = ''
  $scope.current_dictionary = $routeParams.id if $routeParams.id
  $scope.new_dictionary = {}

  $scope.add_dictionary = () ->
    Logging.log 'add_dictionary'
    if $scope.new_dictionary.id
      modal_name = "add-new-dictionary"
      params = {}
      params["company_id"] = $scope.company.id
      params["dictionary"] = $scope.new_dictionary
      Companies.edit_item($scope.array_name, params, modal_name)
    else
      modal_name = "add-new-dictionary"
      params = {}
      params["company_id"] = $scope.company.id
      word =
        name: $scope.new_dictionary.name
        additional: $scope.new_dictionary.additional
        object_type: $scope.current_dictionary
      params["word"] = word
      Logging.log $scope.new_dictionary
      Companies.add_new_item($scope.array_name, params, modal_name)
    return

  $scope.edit_dictionary = (id) ->
    $scope.new_dictionary = {}
    $scope.new_dictionary = Companies.get_by_id($scope.array_name, id)
    if $scope.new_dictionary
      $('#add-new-dictionary').modal('show');
    else
#      error alert
      alert("Error epta");
    return
]