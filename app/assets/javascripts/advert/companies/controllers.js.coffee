@companies.controller 'CompaniesCtrl', ['$scope', '$location', 'Logging', 'Urls', 'Companies', ($scope, $location, Logging, Urls, Companies) ->
  $scope.new_company = {}

  $scope.add_company = () ->
    console.log $scope.new_company
    if $scope.new_company.id
      Companies.edit_company $scope.new_company
    else
      Companies.add_new_company($scope.new_company)
    return

  $scope.change_current_company = (comp) ->
    $scope.company = comp
    Companies.change_current_company(comp)
    $location.path( "/advert/orders" );
    return true

  $scope.edit_company = (id) ->
    $scope.new_company = {}
    $scope.new_company = Companies.get_company_id(id)
    if $scope.new_company
      $('#company-add-modal').modal('show');
    else
#      error alert
      alert("Error epta");
    return
]