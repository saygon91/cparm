@warehouses.controller 'WarehousesCtrl', ['$scope', 'Logging', 'Companies', ($scope, Logging, Companies) ->
  $scope.redirectIfNoAccess()
  $scope.new_warehouse = {}
  $scope.array_name = "warehouses"

  $scope.add_warehouse = () ->
    if $scope.new_warehouse.id
      modal_name = "add-new-warehouse"
      params = {}
      params["company_id"] = $scope.company.id
      params["warehouse_id"] = $scope.new_warehouse.id
      params["warehouse"] = $scope.new_warehouse
      Companies.edit_item($scope.array_name, params, modal_name)
    else
      modal_name = "add-new-warehouse"
      params = {}
      params["company_id"] = $scope.company.id
      params["warehouse"] = $scope.new_warehouse
      Companies.add_new_item($scope.array_name, params, modal_name)
    return

  $scope.delete_warehouse = (id) ->
    Companies.delete_by_id $scope.array_name, Companies.get_by_id($scope.array_name, id)
    return

  $scope.edit_warehouse = (id) ->
    $scope.new_warehouse = {}
    $scope.new_warehouse = Companies.get_by_id($scope.array_name, id)
    if $scope.new_warehouse
      $('#add-new-warehouse').modal('show');
    else
#      error alert
      alert("Error epta");
    return
]

@warehouses.controller 'WarehouseCtrl', ['$scope', '$routeParams', 'Logging', 'Companies', ($scope, $routeParams, Logging, Companies) ->
  $scope.redirectIfNoAccess()
  $scope.product_arrival = {}
  $scope.product_outgo = {}
  id = $routeParams.id
  if id and $scope.company
    $scope.warehouse = warehouse for warehouse in $scope.company.warehouses when warehouse.id == +id

  $scope.clearForms = () ->
    $scope.product_arrival = {}
    $scope.product_outgo = {}

  $scope.product_arrivals = () ->
    modal_name = "product-arrival"
    params = {}
    params["company_id"] = $scope.company.id
    params["warehouse_id"] = +id
    params["product_id"] = $scope.product_arrival.product_id
    params["count"] = $scope.product_arrival.count
    Companies.product_arrival(params, modal_name)
    return

  $scope.product_outgos = () ->
    modal_name = "product-outgo"
    params = {}
    params["company_id"] = $scope.company.id
    params["warehouse_id"] = +id
    params["product_id"] = $scope.product_outgo.product_id
    params["count"] = $scope.product_outgo.count
    Companies.product_outgo(params, modal_name)
    return

]