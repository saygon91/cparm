@products.controller 'ProductsCtrl', ['$scope', 'Logging', 'Companies', ($scope, Logging, Companies) ->
  $scope.redirectIfNoAccess()
  $scope.product = {}
  $scope.array_name = "products"

  $scope.add_product = () ->
    console.log $scope.product
    if $scope.product.id
      modal_name = "new_product"
      params = {}
      params["company_id"] = $scope.company.id
      params["product_id"] = $scope.product.id
      params["product"] = $scope.product
      Companies.edit_item($scope.array_name, params, modal_name)
    else
      modal_name = "new_product"
      params = {}
      params["company_id"] = $scope.company.id
      params["product"] = $scope.product
      Companies.add_new_item($scope.array_name, params, modal_name)
    return

  $scope.delete_product = (id) ->
    Companies.delete_by_id $scope.array_name, Companies.get_by_id($scope.array_name, id)
    return

  $scope.edit_product = (id) ->
    $scope.product = {}
    $scope.product = Companies.get_by_id($scope.array_name, id)
    if $scope.product
      $('#new_product').modal('show');
    else
#      error alert
      alert("Error epta");
    return
]