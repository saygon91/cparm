@products.controller 'BillingCtrl', ['$scope', '$http', 'Urls', 'Logging', 'Companies', ($scope, $http, Urls, Logging, Companies) ->
  $scope.redirectIfNoAccess()
  date = new Date
  $scope.date_from = new Date(1945,1,1,0,0)
  $scope.date_to = new Date(date.getFullYear(),date.getMonth(),date.getDate(),23,59)
  $scope.date_from_2 = new Date(1945,1,1,0,0)
  $scope.date_to_2 = new Date(date.getFullYear(),date.getMonth(),date.getDate(),23,59)
  $scope.salaries = undefined
  $scope.bills = undefined

  $scope.generate_salary = () ->
    $http.post(Urls.get('generate_salary'), {
        company_id: $scope.company.id,
        date_from: $scope.date_from
        date_to: $scope.date_to
        status_id: $scope.status_id
      }
    ).then((response, status, headers, config) ->
      if response.data.success
        Logging.log response
        $scope.salaries = response.data.salary
      else
        Logging.log 'произошла ошибка при подгрурзке данных'
    )

  $scope.generate_bills = () ->
    $http.post(Urls.get('generate_bills'), {
        company_id: $scope.company.id,
        date_from: $scope.date_from_2
        date_to: $scope.date_to_2
        webmaster_id: $scope.webmaster
        comment: $scope.comment
        pay: false
      }
    ).then((response, status, headers, config) ->
      if response.data.success
        Logging.log response
        $scope.checks = response.data.checks
        $scope.total_billing = 0
        $scope.total_billing+=ckeck.amount for ckeck in $scope.checks
      else
        Logging.log 'произошла ошибка при подгрурзке данных'
    )

  $scope.pay_bills = (ckeck) ->
    $http.post(Urls.get('generate_bills'), {
        company_id: $scope.company.id,
        date_from: $scope.date_from_2
        date_to: $scope.date_to_2
        webmaster_id: ckeck.webmaster_id
        comment: ckeck.comment
        pay: true
      }
    ).then((response, status, headers, config) ->
      if response.data.success
        Logging.log response
        $scope.checks = ''
        alert("Счет оплачен");
      else
        Logging.log 'произошла ошибка при подгрурзке данных'
    )
]