@advert.config ($routeProvider, $locationProvider) ->
  $routeProvider.when '/advert/',
    redirectTo: (current, path, search) ->
      if search.goto
        return "/advert/" + search.goto
      else
        return "/"

  .when '/advert/offers',
    templateUrl: 'offers.html'
    module: 'OffersApp'
    controller: 'OffersCtrl'

  .when '/advert/contracts',
    templateUrl: 'contracts.html'
    module: 'ContractsApp',
    controller: 'ContractsCtrl'

  .when '/advert/products',
    templateUrl: 'products.html'
    module: 'ProductsApp',
    controller: 'ProductsCtrl'

  .when '/advert/warehouses',
    templateUrl: 'warehouses.html'
    module: 'WarehousesApp',
    controller: 'WarehousesCtrl'

  .when '/advert/warehouse/:id',
    templateUrl: 'warehouse.html'
    module: 'WarehousesApp',
    controller: 'WarehouseCtrl'

  .when '/advert/orders',
    templateUrl: 'orders.html'
    module: 'OrdersApp',
    controller: 'OrdersCtrl'

  .when '/advert/employees',
    templateUrl: 'employees.html'
    module: 'EmployeesApp',
    controller: 'EmployeesCtrl'

  .when '/advert/customers',
    templateUrl: 'customers.html',
    module: 'CustomersApp',
    controller: 'CustomersCtrl'

  .when '/advert/notifications',
    templateUrl: 'notifications.html'

  .when '/advert/settings',
    templateUrl: 'settings.html'
    module: 'SettingsApp',
    controller: 'SettingsCtrl'

  .when '/advert/dictionary/:id',
    templateUrl: 'dictionary.html'
    module: 'SettingsApp',
    controller: 'DictionaryCtrl'

  .when '/advert/billing',
    templateUrl: 'billing.html'
    module: 'BillingApp',
    controller: 'BillingCtrl'

  .when '/advert/statistic',
    templateUrl: 'statistic.html'
    module: 'StatisticApp',
    controller: 'StatisticCtrl'

  .when '/advert/logs',
    templateUrl: 'logs.html'
    module: 'LogsApp',
    controller: 'LogsCtrl'

  .when '/advert/messages',
    templateUrl: 'messages.html'
    module: 'MessagesApp',
    controller: 'MessagesCtrl'

  .when '/advert/no_access',
    templateUrl: 'no_access.html'

  .otherwise
    templateUrl: 'orders.html'
    module: 'OrdersApp',
    controller: 'OrdersCtrl'

  $locationProvider.html5Mode({ enabled: true, requireBase: false});
  return