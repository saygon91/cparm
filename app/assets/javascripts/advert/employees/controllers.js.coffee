@employees.controller 'EmployeesCtrl', ['$scope', 'Logging', 'Urls', 'Companies', ($scope, Logging, Urls, Companies) ->
  $scope.redirectIfNoAccess()
  $scope.employee = {}
  $scope.array_name = "employees"

  $scope.add_employee = () ->
    console.log $scope.employee
    if $scope.employee.id
      modal_name = "employees-list-employee-modal"
      params = {}
      params["company_id"] = $scope.company.id
      params["employee_id"] = $scope.employee.id
      params["employee"] = $scope.employee
      Companies.edit_item($scope.array_name, params, modal_name)
    else
      modal_name = "employees-add-modal"
      params = {}
      params["company_id"] = $scope.company.id
      params["employee"] = $scope.employee
      Companies.add_new_item($scope.array_name, params, modal_name)
    return

  $scope.block_employee = (id) ->
    Companies.block_employee Companies.get_by_id($scope.array_name, id)
    return

  $scope.unblock_employee = (id) ->
    Companies.unblock_employee Companies.get_by_id($scope.array_name, id)
    return

  $scope.edit_employee = (id) ->
    $scope.employee = {}
    $scope.employee = Companies.get_by_id($scope.array_name, id)
    if $scope.employee
      $('#employees-list-employee-modal').modal('show');
    else
#      error alert
      alert("Error epta");
    return
]