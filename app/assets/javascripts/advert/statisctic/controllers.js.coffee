@statistic.controller 'StatisticCtrl', ['$scope', '$http', 'Urls', 'Logging', 'Companies', ($scope, $http, Urls, Logging, Companies) ->
  $scope.redirectIfNoAccess()
  date = new Date
  $scope.date_from = new Date(1945,1,1,0,0)
  $scope.date_to = new Date(date.getFullYear(),date.getMonth(),date.getDate(),23,59)
  $scope.date_from_2 = new Date(1945,1,1,0,0)
  $scope.date_to_2 = new Date(date.getFullYear(),date.getMonth(),date.getDate(),23,59)
  if $scope.company
    for word in $scope.company.dictionaries
      $scope.status_from = word.id if word.name is "Новый"
      $scope.status_to = word.id if word.name is "Подтвержден"
  $scope.statistics = undefined
  $scope.accepted_statistics = undefined

  $scope.generate_statistic = () ->
    $http.post(Urls.get('generate_statistic'), {
        company_id: $scope.company.id,
        date_from: $scope.date_from
        date_to: $scope.date_to
        status_from: $scope.status_from
        status_to: $scope.status_to
      }
    ).then((response, status, headers, config) ->
      if response.data.success
        Logging.log response
        $scope.statistics = response.data.statistic
      else
        Logging.log 'произошла ошибка при подгрурзке данных'
    )

  $scope.generate_accepted_statistic = () ->
    $http.post(Urls.get('generate_accepted_stat'), {
        company_id: $scope.company.id
        date_from: $scope.date_from_2
        date_to: $scope.date_to_2
      }
    ).then((response, status, headers, config) ->
      if response.data.success
        Logging.log response
        $scope.accepted_statistics = response.data.statistic
      else
        Logging.log 'произошла ошибка при подгрурзке данных'
    )
]