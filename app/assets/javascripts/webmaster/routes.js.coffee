@webmaster.config ($routeProvider, $locationProvider) ->
  $routeProvider.when '/webmaster/',
    redirectTo: (current, path, search) ->
      if search.goto
        return "/webmaster/" + search.goto
      else
        return "/"

  .when '/webmaster/offers',
    templateUrl: 'offers.html'
    module: 'OffersApp'
    controller: 'OffersCtrl'

  .when '/webmaster/contracts',
    templateUrl: 'contracts.html'
    module: 'ContractsApp'
    controller: 'ContractsCtrl'

  .when '/webmaster/getlink',
    templateUrl: 'getlink.html'
    module: 'GetlinkApp'
    controller: 'GetlinkCtrl'

  .when '/webmaster/billing',
    templateUrl: 'billing.html'
    module: 'BillingApp'
    controller: 'BillingCtrl'

  .when '/webmaster/statistic',
    templateUrl: 'statistic.html'
    module: 'StatisticApp'
    controller: 'StatisticCtrl'

  .when '/webmaster/notifications',
    templateUrl: 'notifications.html'

  .otherwise
      templateUrl: 'offers.html'
      module: 'OffersApp'
      controller: 'OffersCtrl'

  $locationProvider.html5Mode({ enabled: true, requireBase: false});
  return