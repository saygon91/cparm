@offers.controller 'OffersCtrl', ['$scope', 'Logging', 'Urls', 'WmData', ($scope, Logging, Urls, WmData) ->
  Logging.log "OffersCtrl"

  $scope.connect_offer = (offer) ->
    WmData.connect_offer(offer)
    console.log offer

  $scope.is_connected_offer = (offer) ->
    WmData.is_connected_offer(offer)
]