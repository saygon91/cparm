@statistic.controller 'StatisticCtrl', ['$scope', '$http', 'Urls', 'Logging', ($scope, $http, Urls, Logging) ->
  date = new Date
  $scope.date_from = new Date(1945,1,1,0,0)
  $scope.date_to = new Date(date.getFullYear(),date.getMonth(),date.getDate(),23,59)
  $scope.statistics = undefined

  $scope.generate_statistic = () ->
    $http.post(Urls.get('generate_statistic_wm'), {
        date_from: $scope.date_from
        date_to: $scope.date_to
        contract_id: $scope.contract_id
      }
    ).then((response, status, headers, config) ->
      if response.data.success
        Logging.log response
        $scope.statistics = response.data.statistic
      else
        Logging.log 'произошла ошибка при подгрурзке данных'
    )
]