@webmaster.factory 'WmData', ['Logging', '$http', 'Urls', 'Alerts', (Logging, $http, Urls, Alerts) ->
  class WmData
    roles = []
    offers = []
    contracts = []
    billing = []

    init: (done_callback) ->
      $http.get(Urls.get('webmaster_all'), {}
      ).then((response, status, headers, config) ->
        if response.data.success
          roles = response.data.roles
          billing = response.data.user.balances
          if response.data.offers.length > 0
            offers = response.data.offers
          contracts = response.data.contracts
          done_callback()
          Logging.log 'json fetched from post request'

      )

    get_roles: () ->
      Logging.log "get_roles"
      roles

    get_all_offers: () ->
      Logging.log "get_all_offers"
      offers

    get_all_contracts: () ->
      Logging.log "get_all_contracts"
      contracts

    get_billing: () ->
      Logging.log "get_billing"
      billing

    connect_offer: (offer) ->
      $http.post(Urls.get('connect_offer'),
        offer_id: offer.id
      ).success((data, status, headers, config) ->
        Logging.log "connect_offer"
        if data.success
          item.connected = true for item in offers when offer.id is item.id
          contracts.push data.contract
          Logging.log data
          Alerts.show_alerts(data.alerts)
        else
          Alerts.show_errors(data.errors)
      ).error (data, status, headers, config) ->
        Logging.log data
      true

    is_connected_offer: (offer) ->
      return true for contract in contracts when offer.id is contract.offer_id
      false

    get_offer_by_id: (id) ->
      result = undefined
      result = offer for offer in offers when offer.id is id
      result

    get_by_id: (array_name, id) ->
      if array_name of company
        Logging.log "get_by_id from " + array_name + " element id: " + id
        result = undefined
        result = item for item in company[array_name] when item.id is id
        result

    delete_by_id: (array_name, item) ->
      status = false
      if array_name of company
        array_name_singular = array_name.substring 0, array_name.length - 1
        params = {}
        params["company_id"] = company.id
        params[array_name_singular + "_id"] = item.id
        $http.post(Urls.get('delete_'+array_name_singular), params).success((data, status, headers, config) ->

          if data.success
            company[array_name].splice(company[array_name].indexOf(item), 1)
            Logging.log 'delete_by_id from ' + array_name + " element id: " + item.id
            msg = {}
            msg.type = "success"
            msg.message = "Успешно удалено"
            Alerts.add_alert(msg);
            status = true
          else
            Alerts.show_errors(data.errors)
        ).error (data, status, headers, config) ->
          Logging.log data
          Alerts.show_errors(data.errors)
          status = false

      return status

    add_new_item: (array_name, params, modal_name) ->
      if array_name of company
        array_name_singular = array_name.substring 0, array_name.length - 1
        $http.post(Urls.get('new_'+array_name_singular), params).success((data, status, headers, config) ->
          if data.success && array_name_singular of data
            temp[array_name].push data[array_name_singular] for temp in companies when temp.id is company.id
            Logging.log "add_new_" + array_name_singular
            msg = {}
            msg.type = "success"
            msg.message = "Успешно добавлено"
            Alerts.add_alert(msg);
          else
            Alerts.show_errors(data.errors)
          $('#'+modal_name).modal('hide');
        ).error (data, status, headers, config) ->
          Logging.log data
          Alerts.show_errors(data.errors)
        true

    edit_item: (array_name, params, modal_name) ->
      if array_name of company
        array_name_singular = array_name.substring 0, array_name.length - 1
        $http.post(Urls.get('update_'+array_name_singular), params).success((data, status, headers, config) ->
          if data.success && array_name_singular of data
            for temp_company in companies when temp_company.id is company.id
              temp = data[array_name_singular] for temp in temp_company[array_name] when temp.id is data[array_name_singular].id
            Logging.log "edit_" + array_name_singular
            msg = {}
            msg.type = "success"
            msg.message = "Успешно изменено"
            Alerts.add_alert(msg);
          else
            Alerts.show_errors(data.errors)
          $('#'+modal_name).modal('hide');
        ).error (data, status, headers, config) ->
          Logging.log data
          Alerts.show_errors(data.errors)
        true

  new WmData()
]