@billing.controller 'BillingCtrl', ['$scope', '$http', 'Logging', 'Urls', 'WmData', ($scope, $http, Logging, Urls, WmData) ->
  Logging.log "BillingCtrl"

  date = new Date
  $scope.date_from = new Date(1945,1,1,0,0)
  $scope.date_to = new Date(date.getFullYear(),date.getMonth(),date.getDate(),23,59)
  $scope.bills = undefined

  $scope.generate_bills = () ->
    $http.post(Urls.get('generate_bills_wm'), {
        date_from: $scope.date_from
        date_to: $scope.date_to
        contract_id: $scope.contract_id
      }
    ).then((response, status, headers, config) ->
      if response.data.success
        Logging.log response
        $scope.bills = response.data.check
      else
        Logging.log 'произошла ошибка при подгрурзке данных'
    )

]