@offers = angular.module('OffersApp', [])
@contracts = angular.module('ContractsApp', [])
@getlink = angular.module('GetlinkApp', [])
@billing = angular.module('BillingApp', [])
@statistic = angular.module('StatisticApp', [])

@webmaster = angular.module('webmaster', ['ui.bootstrap', 'angular-loading-bar', 'cparm.utilz', 'ngRoute', 'OffersApp', 'ContractsApp', 'GetlinkApp', 'BillingApp', 'StatisticApp'])

@webmaster.config ($httpProvider) ->          $httpProvider.defaults.withCredentials = true
@webmaster.config (cfpLoadingBarProvider) ->  cfpLoadingBarProvider.includeSpinner = true

@webmaster.controller 'WebmasterController', ['cfpLoadingBar', '$http', 'Alerts', 'Urls', '$scope', 'Logging', '$route', '$routeParams', '$location', 'WmData', (cfpLoadingBar, $http, Alerts, Urls, $scope, Logging, $route, $routeParams, $location, WmData) ->
  $scope.isActive = (route) -> route is $location.path()
  $scope.alerts = Alerts.get_alerts()

  init_done = ->
    $scope.roles = WmData.get_roles()
    $scope.offers = WmData.get_all_offers()
    $scope.contracts = WmData.get_all_contracts()
    $scope.billing = WmData.get_billing()
    window._scope = $scope

  WmData.init(init_done)
]