// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery-ui
//= require bootstrap
// require angular-file-upload-shim.min.js
//= require angular/angular
//= require angular-ui-sortable
//= require angular-route
//= require angular-bootstrap
// require angular-file-upload.min.js
//= require angular-animate
//= require angular-loading-bar
//= require 'utilz'
//= require chosen/chosen.jquery
//= require angular-chosen-localytics
//= require ./advert/advert
//= require_tree ./advert
//= require notifications
//= require websocket_rails/main






