(($) ->
  Notification = (element, options) ->

    # Element collection
    @$element = $(element)
    @$note = $("<div class=\"alert\"></div>")
    @options = $.extend(true, {}, $.fn.notify.defaults, options)

    # Setup from options
    if @options.transition
      if @options.transition is "fade"
        @$note.addClass("in").addClass @options.transition
      else
        @$note.addClass @options.transition
    else
      @$note.addClass("fade").addClass "in"
    if @options.type
      @$note.addClass "alert-" + @options.type
    else
      @$note.addClass "alert-success"
    if not @options.message and @$element.data("message") isnt "" # dom text
      @$note.html @$element.data("message")
    else if typeof @options.message is "object"
      if @options.message.html
        @$note.html @options.message.html
      else @$note.text @options.message.text  if @options.message.text
    else
      @$note.html @options.message
    this

  onClose = ->
    @options.onClose()
    $(@$note).remove()
    @options.onClosed()
    false

  Notification::show = ->
    @$note.delay(@options.fadeOut.delay or 3000).fadeOut "slow", $.proxy(onClose, this)  if @options.fadeOut.enabled
    @$element.append @$note
    @$note.alert()
    return

  Notification::hide = ->
    if @options.fadeOut.enabled
      @$note.delay(@options.fadeOut.delay or 3000).fadeOut "slow", $.proxy(onClose, this)
    else
      onClose.call this
    return

  $.fn.notify = (options) ->
    new Notification(this, options)

  $.fn.notify.defaults =
    type: "success"
    closable: true
    transition: "fade"
    fadeOut:
      enabled: true
      delay: 3000

    message: null
    onClose: ->

    onClosed: ->

  return
) window.jQuery